package notification

import (
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	util2 "bitbucket.org/fraktalcyber/cspm-core/src/core/util"
	"crypto/tls"
	"fmt"
	"log"
	"net/smtp"
	"strings"
	"time"
)

func SendEmailAlertActived(finding *model2.SecurityFinding) error {
	return sendAlertEmail(finding, "+")
}

func SendEmailAlertDeactivated(finding *model2.SecurityFinding) error {
	return sendAlertEmail(finding, "-")
}

func sendAlertEmail(finding *model2.SecurityFinding, operation string) error {
	if util2.GetConfig("smtp_enabled", "true") != "true" {
		return nil
	}

	toEmailAddresses := getToEmailAddresses()

	firstObserved := finding.FirstObserved.Format(time.RFC3339)
	lastObserved := finding.LastObserved.Format(time.RFC3339)
	priority := finding.Priority
	state := finding.State
	source := finding.SourceType
	category := finding.Category
	findingType := finding.Type
	key := finding.Key
	team := finding.Team
	env := finding.EnvironmentType
	account := finding.AccountName
	region := finding.Region
	resource := ""
	if finding.ResourceId != nil {
		resource = *finding.ResourceId
	}
	title := finding.Title
	description := finding.Description

	priorityString := getAlertPriority(priority)

	subject := operation + " " + priorityString + " ALERT: " + account + " " + region + " " + title
	body := "" +
		"first_observed=" + firstObserved + "\n" +
		"last_observed=" + lastObserved + "\n" +
		"priority=" + priorityString + "\n" +
		"entity=" + account + " " + region + " " + fmt.Sprintf("%v", resource) + "\n" +
		"state=" + fmt.Sprintf("%v", state) + "\n" +
		"source=" + source + "\n" +
		"category=" + fmt.Sprintf("%v", category) + "\n" +
		"type=" + findingType + "\n" +
		"key=" + key + "\n" +
		"team=" + team + "\n" +
		"env=" + env + "\n" +
		"account=" + account + "\n" +
		"region=" + region + "\n" +
		"resource=" + fmt.Sprintf("%v", resource) + "\n" +
		"title=" + title + "\n" +
		"description=" + description + "\n"

	return SendEmail(toEmailAddresses, subject, body)
}

func getAlertPriority(priority int) string {
	switch priority {
	case 5:
		return "P1"
	case 4:
		return "P2"
	case 3:
		return "P3"
	case 2:
		return "P4"
	case 1:
		return "P5"
	default:
		return "?"
	}
}

func SendEmail(toEmailAddresses []string, subject string, body string) error {
	smtpHost := util2.GetConfig("smtp_host", "")
	smtpPort := util2.GetConfig("smtp_port", "")
	smtpAddress := smtpHost + ":" + smtpPort
	smtpUsername := util2.GetConfig("smtp_username", "")
	smtpPassword, err := util2.GetSecret("smtp_password")
	if err != nil {
		log.Printf("Error reading smtp_password %v.", err.Error())
		return err
	}
	smtpSecure := util2.GetConfig("smtp_secure", "") != "false"
	smtpFrom := util2.GetConfig("smtp_from", "")

	toEmailAddressesString := strings.Join(toEmailAddresses, ", ")
	auth := smtp.PlainAuth("", smtpUsername, smtpPassword, smtpHost)

	message := []byte("To: " + toEmailAddressesString + "\r\n" +
		"Subject: " + subject + "\r\n" +
		"\r\n" +
		body + "\r\n")

	if smtpSecure {
		conn, err := tls.Dial("tcp", smtpAddress, &tls.Config{
			InsecureSkipVerify: false,
			ServerName:         smtpHost,
		})
		if err != nil {
			log.Printf("Error sending email %v.", err.Error())
			return err
		}

		c, err := smtp.NewClient(conn, smtpHost)
		if err != nil {
			log.Printf("Error sending email %v.", err.Error())
			return err
		}

		// Auth
		if err = c.Auth(auth); err != nil {
			log.Printf("Error sending email %v.", err.Error())
			return err
		}

		// To && From
		if err = c.Mail(smtpFrom); err != nil {
			log.Printf("Error sending email %v.", err.Error())
			return err
		}

		for _, toEmailAddress := range toEmailAddresses {
			if err = c.Rcpt(toEmailAddress); err != nil {
				log.Printf("Error sending email %v.", err.Error())
				return err
			}
		}

		// Data
		w, err := c.Data()
		if err != nil {
			log.Printf("Error sending email %v.", err.Error())
			return err
		}

		_, err = w.Write(message)
		if err != nil {
			log.Printf("Error sending email %v.", err.Error())
			return err
		}

		err = w.Close()
		if err != nil {
			log.Printf("Error sending email %v.", err.Error())
			return err
		}
	} else {
		err = smtp.SendMail(smtpAddress, auth, smtpFrom, toEmailAddresses, message)
		if err != nil {
			log.Printf("Error sending email %v.", err.Error())
			return err
		}
	}

	return nil
}

func getToEmailAddresses() []string {
	smtpAlertTo := util2.GetConfig("smtp_alert_to", "")
	toEmailAddresses := strings.Split(smtpAlertTo, ",")
	for i := range toEmailAddresses {
		toEmailAddresses[i] = strings.TrimSpace(toEmailAddresses[i])
	}
	return toEmailAddresses
}
