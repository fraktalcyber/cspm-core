package util

import (
	"encoding/json"
	"github.com/mattn/go-jsonpointer"
)

func GetStringWithJsonPointer(configuration map[string]interface{}, jsonPointer string) *string {
	value, err := jsonpointer.Get(configuration, jsonPointer)
	if err != nil {
		return nil
	}
	stringValue := ValueToString(value)
	return &stringValue
}

func GetInt64WithJsonPointer(configuration map[string]interface{}, jsonPointer string) *int64 {
	value, err := jsonpointer.Get(configuration, jsonPointer)
	if err != nil {
		return nil
	}
	stringValue := ValueToInt64(value)
	return &stringValue
}

func GetFloat64WithJsonPointer(configuration map[string]interface{}, jsonPointer string) *float64 {
	value, err := jsonpointer.Get(configuration, jsonPointer)
	if err != nil {
		return nil
	}
	stringValue := ValueToFloat64(value)
	return &stringValue
}

func ToPrettyJson(value interface{}) (string, error) {
	bytes, err := json.MarshalIndent(value, "", "\t")
	return string(bytes), err
}

func ToJson(value interface{}) (string, error) {
	bytes, err := json.Marshal(value)
	return string(bytes), err
}
