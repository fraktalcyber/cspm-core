package util

import (
	"encoding/json"
	"fmt"
	"log"
)

func FormatStringPointer(value *string) string {
	if value == nil {
		return "nil"
	} else {
		return fmt.Sprintf("%v", *value)
	}
}

func FormatFloatPointer(value *float64) string {
	if value == nil {
		return "nil"
	} else {
		return fmt.Sprintf("%v", *value)
	}
}

func ValueToStringPointer(value interface{}) *string {
	if value == nil {
		return nil
	} else {
		val := value.(string)
		return &val
	}
}

func ValueToString(value interface{}) string {
	if value == nil {
		return ""
	} else {
		val := value.(string)
		return val
	}
}

func ValueToInt64(value interface{}) int64 {
	if value == nil {
		return -1
	} else {
		val := value.(int64)
		return val
	}
}

func ValueToFloat64(value interface{}) float64 {
	if value == nil {
		return -1
	} else {
		val := value.(float64)
		return val
	}
}

func ValueToFloatPointer(value interface{}) *float64 {
	if value == nil {
		return nil
	} else {
		val := value.(float64)
		return &val
	}
}

func FormatInterface(value interface{}) string {
	bytes, err := json.Marshal(value)
	if err != nil {
		log.Println("Error formatting interface.", err)
		return "ERROR"
	} else {
		return string(bytes)
	}
}
