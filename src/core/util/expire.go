package util

import (
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	"time"
)

var HourlyExpiringSecurityFindingTypes = []string{
	model2.SecurityFindingTypeUnauthorizedAPICalls,
	"CIS AWS Benchmark/Console Signin",
	"CIS AWS Benchmark/Console Signin No MFA",
	"CIS AWS Benchmark/Unauthorized API Calls",
}

func IsFindingExpired(finding *model2.SecurityFinding) bool {
	now := time.Now()
	normalExpiration := now.Add(-time.Hour * time.Duration(15*24))
	hourlyExpiration := now.Add(-time.Hour * time.Duration(1))
	if finding.Category == model2.CategoryNotification ||
		StringSliceContains(HourlyExpiringSecurityFindingTypes, finding.Type) {
		if finding.LastObserved.Before(hourlyExpiration) {
			return true
		}
	} else {
		if finding.LastObserved.Before(normalExpiration) {
			return true
		}
	}
	return false
}
