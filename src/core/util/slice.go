package util

import "reflect"

func Reverse(s interface{}) {
	n := reflect.ValueOf(s).Len()
	swap := reflect.Swapper(s)
	for i, j := 0, n-1; i < j; i, j = i+1, j-1 {
		swap(i, j)
	}
}

func StringSliceIndexOf(strings []string, str string) int {
	for k, v := range strings {
		if str == v {
			return k
		}
	}
	return -1
}

func StringSliceContains(strings []string, str string) bool {
	return StringSliceIndexOf(strings, str) != -1
}
