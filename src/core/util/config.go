package util

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"gopkg.in/ini.v1"
)

var iniConfig *ini.File

var debugConfig = false

func LoadConfig() {
	initFlags()
	var err error
	env := os.Getenv("ENV")
	if env == "" {
		env = "default"
	}
	fmt.Printf("Enviroment: %v\n", env)
	iniConfig, err = ini.Load("config/" + env + ".ini")
	if err != nil {
		// continue without .ini -- everything needs to be passed in as env variables
		// overwrite iniConfig to make sure ini.Load didn't leave junk there
		iniConfig = nil
	}
	debugConfig = GetConfig("debug_config", "false") == "true"
}

func GetConfig(key string, defaultValue string) string {
	envKey := strings.Replace(strings.ToUpper(key), "-", "_", -1)
	value := os.Getenv(envKey)

	if value != "" {
		if debugConfig {
			log.Printf("%40s: %s (env)", envKey, value)
		}
		return value
	}

	if iniConfig == nil && defaultValue != "" {
		return defaultValue
	}

	if iniConfig == nil {
		panic(fmt.Sprintf("environment variable %s not found and no .ini file loaded\n", envKey))
	}

	if iniConfig.Section("").HasKey(key) && strings.Trim(iniConfig.Section("").Key(key).String(), " ") != "" {
		value = iniConfig.Section("").Key(key).String()
		if debugConfig {
			log.Printf("%40s: %s (ini)", envKey, value)
		}
		return value
	}

	if defaultValue == "" {
		if debugConfig {
			panic(fmt.Sprintf("No config value: '%v' and no default provided.\n", key))
		}
	}

	if debugConfig {
		log.Printf("%40s: %s (default)", envKey, defaultValue)
	}

	return defaultValue
}

func GetSecret(key string) (string, error) {
	envKey := strings.Replace(strings.ToUpper(key), "-", "_", -1)
	envValue := os.Getenv(envKey)

	if envValue != "" {
		if debugConfig {
			log.Printf("%40s: ***********", envKey)
		}
		return envValue, nil
	}

	secretPathKey := key + "_path"
	secretPath := GetConfig(secretPathKey, "secrets/"+key)

	value, err := ioutil.ReadFile(secretPath)
	if err != nil {
		return "", err
	}

	if debugConfig {
		log.Printf("%40s: ***********", secretPath)
	}
	return string(value), nil
}
