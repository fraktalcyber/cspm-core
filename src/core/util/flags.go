package util

import (
	"flag"
	"log"
	"os"
	"strings"
)

type FlagArray []string

func (a *FlagArray) String() string {
	return strings.Join(*a, ",")
}

func (a *FlagArray) Set(value string) error {
	*a = append(*a, value)
	return nil
}

var environmentVariableFlags FlagArray

func initFlags() {
	flag.Var(&environmentVariableFlags, "env", "Environment variable.")
	flag.Parse()

	for _, environmentVariableFlag := range environmentVariableFlags {
		environmentVariableFlagParts := strings.Split(environmentVariableFlag, "=")
		if len(environmentVariableFlagParts) != 2 {
			continue
		}
		environmentVariableKey := environmentVariableFlagParts[0]
		environmentVariableValue := environmentVariableFlagParts[1]
		if err := os.Setenv(environmentVariableKey, environmentVariableValue); err != nil {
			panic(err)
		}
		log.Printf("Set environment variable based on --env flag: '%v'='%v'", environmentVariableKey, environmentVariableValue)
	}
}
