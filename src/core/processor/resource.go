package processor

import (
	db2 "bitbucket.org/fraktalcyber/cspm-core/src/core/storage/db"
	"log"
	"sync"
)

type ResourceProcessor struct {
	processingContext *ProcessingContext
	runWaitGroup      sync.WaitGroup
	close             chan bool
}

func NewResourceProcessor(processingContext *ProcessingContext) *ResourceProcessor {
	return &ResourceProcessor{
		processingContext: processingContext,
		close:             make(chan bool),
	}
}

func (h *ResourceProcessor) Start() error {
	log.Print("Resource Processor started.")
	h.runWaitGroup.Add(1)
	go h.run()
	return nil
}

func (h *ResourceProcessor) run() {
	for exitLoop := false; !exitLoop; {
		select {
		case <-h.close:
			exitLoop = true
		case securityResource := <-h.processingContext.Resource:
			err := db2.SaveResource(h.processingContext.Database, securityResource)
			if err != nil {
				log.Println(err)
			}
		}
	}
	log.Println("Resource Processor exited.")
	h.runWaitGroup.Done()
}

func (h *ResourceProcessor) Stop() error {
	log.Print("Resource Processor stopping...")
	h.close <- true
	h.runWaitGroup.Wait()
	log.Print("Resource Processor stopped.")
	return nil
}
