package processor

import (
	db2 "bitbucket.org/fraktalcyber/cspm-core/src/core/storage/db"
	"log"
	"sync"
)

type FindingProcessor struct {
	processingContext *ProcessingContext
	runWaitGroup      sync.WaitGroup
	close             chan bool
}

func NewSecurityFindingProcessor(processingContext *ProcessingContext) *FindingProcessor {
	return &FindingProcessor{
		processingContext: processingContext,
		close:             make(chan bool),
	}
}

func (h *FindingProcessor) Start() error {
	log.Print("Finding Processor started.")
	h.runWaitGroup.Add(1)
	go h.run()
	return nil
}

func (h *FindingProcessor) run() {
	for exitLoop := false; !exitLoop; {
		select {
		case <-h.close:
			exitLoop = true
		case securityFinding := <-h.processingContext.SecurityFinding:
			err := db2.SaveFinding(h.processingContext.Database, securityFinding)
			if err != nil {
				log.Println(err)
			}
		}
	}
	log.Println("Finding Processor exited.")
	h.runWaitGroup.Done()
}

func (h *FindingProcessor) Stop() error {
	log.Print("Finding Processor stopping...")
	h.close <- true
	h.runWaitGroup.Wait()
	log.Print("Finding Processor stopped.")
	return nil
}
