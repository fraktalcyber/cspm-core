package common

import (
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	"bitbucket.org/fraktalcyber/cspm-core/src/core/processor"
	"log"
	"sync"
	"time"
)

type GenericProcessor struct {
	name               string
	accountContext     *model2.AccountContext
	processingContext  *processor.ProcessingContext
	period             time.Duration
	runWaitGroup       sync.WaitGroup
	close              bool
	startDelayDuration time.Duration
	run                func(accountContext *model2.AccountContext, processingContext *processor.ProcessingContext, close *bool)
}

func NewGenericProcessor(name string, accountContext *model2.AccountContext, processingContext *processor.ProcessingContext, period time.Duration, accountIndex int, numberOfContexts int, run func(accountContext *model2.AccountContext, processingContext *processor.ProcessingContext, close *bool)) *GenericProcessor {
	var startDelayMs = 0
	if accountContext != nil {
		var startDelayWindow = period
		if startDelayWindow > 1*time.Hour {
			startDelayWindow = 1 * time.Hour
		}
		startDelayMs = (int(startDelayWindow/time.Millisecond) / numberOfContexts) * accountIndex
	}

	return &GenericProcessor{
		name:               name,
		accountContext:     accountContext,
		processingContext:  processingContext,
		period:             period,
		close:              false,
		startDelayDuration: time.Duration(startDelayMs) * time.Millisecond,
		run:                run,
	}
}

func (h *GenericProcessor) Start() error {
	h.runWaitGroup.Add(1)
	go h.loop()
	return nil
}

func (h *GenericProcessor) loop() {
	for i := 0; i < int(h.startDelayDuration/time.Second); i++ {
		time.Sleep(time.Second)
		if h.close {
			break
		}
	}
	if !h.close {
		if h.accountContext != nil {
			log.Printf("%v / %v %v (%v) started. Start delay: %+v.\n", h.name, h.accountContext.Name, h.accountContext.Region, h.accountContext.Id, h.startDelayDuration)
		} else {
			log.Printf("%v started. Start delay: %+v.\n", h.name, h.startDelayDuration)
		}
	}
	for !h.close {
		startTime := time.Now()
		h.run(h.accountContext, h.processingContext, &h.close)
		endTime := time.Now()
		spentDuration := endTime.Sub(startTime)
		waitDuration := h.period - spentDuration

		if h.accountContext != nil {
			log.Printf("%v / %v %v (%v) sleeping for %v.\n", h.name, h.accountContext.Name, h.accountContext.Region, h.accountContext.Id, waitDuration)
		} else {
			log.Printf("%v sleeping for %v.\n", h.name, waitDuration)
		}
		for i := 0; i < int(waitDuration/time.Second); i++ {
			time.Sleep(time.Second)
			if h.close {
				break
			}
		}
	}
	if h.accountContext != nil {
		log.Printf("%v / %v %v (%v) exited.\n", h.name, h.accountContext.Name, h.accountContext.Region, h.accountContext.Id)
	} else {
		log.Printf("%v exited.\n", h.name)
	}
	h.runWaitGroup.Done()
}

func (h *GenericProcessor) RequestStop() {
	if h.accountContext != nil {
		log.Printf("%v / %v %v (%v) stopping...\n", h.name, h.accountContext.Name, h.accountContext.Region, h.accountContext.Id)
	} else {
		log.Printf("%v stopping...\n", h.name)
	}
	h.close = true
}

func (h *GenericProcessor) WaitStop() {
	h.runWaitGroup.Wait()
	if h.accountContext != nil {
		log.Printf("%v / %v %v (%v) stopped.\n", h.name, h.accountContext.Name, h.accountContext.Region, h.accountContext.Id)
	} else {
		log.Printf("%v stopped.\n", h.name)
	}
}
