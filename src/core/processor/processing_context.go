package processor

import (
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	db2 "bitbucket.org/fraktalcyber/cspm-core/src/core/storage/db"
)

type ProcessingContext struct {
	Database        *db2.Database
	SecurityFinding chan *model2.SecurityFinding
	Resource        chan *model2.Resource
	Vulnerability   chan *model2.Vulnerability
}
