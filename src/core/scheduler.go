package core

import (
	db2 "bitbucket.org/fraktalcyber/cspm-core/src/core/storage/db"
	"log"
	"sync"
	"time"
)

type Scheduler struct {
	runWaitGroup sync.WaitGroup
	extensions   []Extension
	close        bool
}

func NewScheduler(extensions []Extension) *Scheduler {
	return &Scheduler{
		extensions: extensions,
		close:      false,
	}
}

func (h *Scheduler) Start() error {
	db2.InitDatabase()
	log.Print("Scheduler started.")
	h.runWaitGroup.Add(1)
	go h.run()
	return nil
}

func (h *Scheduler) run() {
	for !h.close {
		var err error

		mainProcessor := NewMainProcessor(h.extensions)
		err = mainProcessor.Start()
		if err != nil {
			log.Printf("Error starting main processor: %v\n", err.Error())
			time.Sleep(5 * time.Second)
			continue
		}
		for i := 0; i < 10800; i++ {
			time.Sleep(1 * time.Second)
			if h.close {
				break
			}
		}

		err = mainProcessor.Stop()
		if err != nil {
			log.Printf("Error stopping main processor: %v\n", err.Error())
			continue
		}

	}
	log.Println("Scheduler exited.")
	h.runWaitGroup.Done()
}

func (h *Scheduler) Stop() error {
	log.Print("Scheduler stopping...")
	h.close = true
	h.runWaitGroup.Wait()
	log.Print("Scheduler stopped.")
	return nil
}
