package core

import (
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	"bitbucket.org/fraktalcyber/cspm-core/src/core/processor"
	"bitbucket.org/fraktalcyber/cspm-core/src/core/processor/common"
)

type Extension interface {
	Name() string
	Start() error
	AddGenericProcessors(processingContext *processor.ProcessingContext, genericProcessors []*common.GenericProcessor, accountContext *model2.AccountContext, accountIndex int, organizationContext *model2.OrganizationContext, err error) (error, []*common.GenericProcessor)
	Stop() error
}
