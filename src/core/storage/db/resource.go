package db

import (
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	"bytes"
	"encoding/json"
	uuid "github.com/satori/go.uuid"
	"log"
	"sort"
	"time"
)

func CountExpiredResources(database *Database, time time.Time) (int, error) {
	var resources []*model2.Resource
	count, err := database.Db.Model(&resources).Where("state != ? AND last_observed < ?", model2.ResourceStateArchived, time).Count()
	if err != nil {
		log.Printf("Error selecting expired resources: %v\n", err.Error())
		return -1, err
	}
	return count, nil
}

func LoadExpiredResources(database *Database, time time.Time, offset int, limit int) ([]*model2.Resource, error) {
	var resources []*model2.Resource
	err := database.Db.Model(&resources).Where("state != ? AND last_observed < ?", model2.ResourceStateArchived, time).Offset(offset).Limit(limit).Select()
	if err != nil {
		log.Printf("Error selecting expired resources: %v\n", err.Error())
		return nil, err
	}
	return resources, nil
}

func CountActiveResources(database *Database, category model2.ResourceCategory, resourceType string) (int, error) {
	var resources []*model2.Resource
	count, err := database.Db.Model(&resources).Where("state != ? AND category = ? AND type = ?", model2.ResourceStateArchived, category, resourceType).Count()
	if err != nil {
		log.Printf("Error selecting active resources: %v\n", err.Error())
		return -1, err
	}
	return count, nil
}

func LoadActiveResources(database *Database, category model2.ResourceCategory, resourceType string, offset int, limit int) ([]*model2.Resource, error) {
	var resources []*model2.Resource
	err := database.Db.Model(&resources).Where("state != ? AND  category = ? AND type = ?", model2.ResourceStateArchived, category, resourceType).Offset(offset).Limit(limit).Select()
	if err != nil {
		log.Printf("Error selecting active resources: %v\n", err)
		return nil, err
	}
	return resources, nil
}

func GetResource(database *Database, key string) (*model2.Resource, error) {
	var resources []*model2.Resource
	err := database.Db.Model(&resources).Where("key = ?", key).Select()
	if err != nil {
		log.Printf("Error selecting resource: %v\n", err)
		return nil, err
	}
	if len(resources) > 0 {
		return resources[0], nil
	} else {
		return nil, nil
	}
}

func SaveResource(database *Database, resource *model2.Resource) error {
	var err error

	if resource.ParentId == nil && resource.ParentKey != nil {
		parentResource, err := GetResource(database, *resource.ParentKey)
		if err != nil {
			log.Printf("Error loading resource parent by key: %v\n", err)
		} else {
			if parentResource != nil {
				resource.ParentId = &parentResource.Id
			}
		}
	}

	var resources []*model2.Resource
	err = database.Db.Model(&resources).Where("key = ?", resource.Key).Select()
	if err != nil {
		log.Printf("Error selecting existig resources: %v\n", err)
		return err
	}

	if len(resources) > 0 {
		return updateResource(database, resource, resources[0], "updated from source", "source")
	} else {
		return insertResource(database, resource)
	}
}

func insertResource(database *Database, resource *model2.Resource) error {
	resourceId := uuid.NewV4().String()
	resource.Id = resourceId
	var err = database.Db.Insert(resource)

	if err != nil {
		bytes, _ := json.Marshal(resource)
		log.Printf("Error inserting resource: %v\n%v\n", err.Error(), string(bytes))
		return err
	}

	return nil
}

func updateResource(database *Database, resource *model2.Resource, existingResource *model2.Resource, comment string, author string) error {
	//log.Println("Updating resource: " + *existingResource.AccountId + " / " + *existingResource.Region + " / " + *existingResource.Key)

	originalBytes, _ := json.Marshal(existingResource)

	existingResource.State = resource.State
	existingResource.AccountName = resource.AccountName

	// Copy new keys to existing metadata
	for k, v := range *resource.Metadata {
		(*existingResource.Metadata)[k] = v
	}

	// Ensure that metadata key count does not grow larger than 100.
	if len(*existingResource.Metadata) > 100 {
		keys := make([]string, 0, len(*existingResource.Metadata))
		for k := range *existingResource.Metadata {
			keys = append(keys, k)
		}
		sort.Strings(keys)
		removeCount := len(*existingResource.Metadata) - 100
		for i := 0; i < removeCount; i++ {
			delete(*existingResource.Metadata, keys[i])
		}
	}

	if existingResource.FirstObserved.After(resource.FirstObserved) {
		existingResource.FirstObserved = resource.FirstObserved
	}
	if existingResource.LastObserved.Before(resource.LastObserved) {
		existingResource.LastObserved = resource.LastObserved
	}

	existingResource.Region = resource.Region
	existingResource.Category = resource.Category
	existingResource.Type = resource.Type
	existingResource.ParentId = resource.ParentId
	existingResource.VulnerabilityKeys = resource.VulnerabilityKeys
	existingResource.Team = resource.Team
	existingResource.EnvironmentType = resource.EnvironmentType

	modifiedBytes, _ := json.Marshal(existingResource)

	existingResource.Modified = time.Now()

	if bytes.Compare(originalBytes, modifiedBytes) != 0 { // Update to database only if there are changes.
		err := database.Db.Update(existingResource)
		if err != nil {
			log.Printf("Error updating resource: %v\n%v\n", err.Error(), string(modifiedBytes))
			return err
		}
	}

	return nil
}

func ArchiveResource(database *Database, resource *model2.Resource) error {
	resource.Modified = time.Now()
	resource.State = model2.ResourceStateArchived

	var err = database.Db.Update(resource)
	if err != nil {
		resourceBytes, _ := json.Marshal(resource)

		log.Printf("Error updating resource: %v\n%v\n", err.Error(), string(resourceBytes))
		return err
	}

	return nil
}
