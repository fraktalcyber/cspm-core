package db

import (
	"context"
	"github.com/go-pg/pg/v9"
	"log"
)

type Logger struct{}

func (d Logger) BeforeQuery(c context.Context, q *pg.QueryEvent) (context.Context, error) {
	return c, nil
}

func (d Logger) AfterQuery(c context.Context, q *pg.QueryEvent) error {
	log.Println(q.FormattedQuery())
	return nil
}
