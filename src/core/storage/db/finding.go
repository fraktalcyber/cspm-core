package db

import (
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	util2 "bitbucket.org/fraktalcyber/cspm-core/src/core/util"
	"bytes"
	"encoding/json"
	uuid "github.com/satori/go.uuid"
	"log"
	"sort"
	"time"
)

func CountNonArchivedFindings(database *Database, spaceId string) (int, error) {
	var findings []*model2.SecurityFinding
	count, err := database.Db.Model(&findings).Where("(state != ? OR alert_state=?) AND space_id=?", "archived", model2.AlertStateActive, spaceId).Count()
	if err != nil {
		log.Printf("Error selecting existig findings: %v\n", err.Error())
		return -1, err
	}
	return count, nil
}

func LoadNonArchivedFindings(database *Database, spaceId string, limit int, offset int) ([]*model2.SecurityFinding, error) {
	var findings []*model2.SecurityFinding
	err := database.Db.Model(&findings).Where("(state != ? OR alert_state=?) AND space_id=?", "archived", model2.AlertStateActive, spaceId).Offset(offset).Limit(limit).Select()
	if err != nil {
		log.Printf("Error selecting existig findings: %v\n", err.Error())
		return nil, err
	}
	return findings, nil
}

func SaveFinding(database *Database, finding *model2.SecurityFinding) error {
	var err error

	if len(finding.Description) > 1000 {
		finding.Description = finding.Description[0:1000]
	}

	var findings []*model2.SecurityFinding
	err = database.Db.Model(&findings).Where("key = ?", finding.Key).Select()
	if err != nil {
		log.Printf("Error selecting existig findings: %v\n", err.Error())
		return err
	}

	if len(findings) > 0 {
		return updateFinding(database, finding, findings[0], "updated from source", "source")
	} else {
		return insertFinding(database, finding)
	}
}

func SaveFindingChange(database *Database, change *model2.RuleApplyResult) error {
	//log.Printf("Saving security finding %v change by %v:  %v", change.SecurityFinding.Key, change.Author, change.Comment)

	//findingBytes, _ := json.Marshal(change.SecurityFinding)
	//log.Printf(string(findingBytes))
	stateChangeId := uuid.NewV4().String()
	var stateChange = &model2.StateChange{
		Id:                stateChangeId,
		SpaceId:           change.SecurityFinding.SpaceId,
		SecurityFindingId: change.SecurityFinding.Id,
		Metadata:          change.SecurityFinding.Metadata,
		State:             change.SecurityFinding.State,
		Priority:          change.SecurityFinding.Priority,
		Comment:           change.Comment,
		Author:            change.Author,
		Created:           time.Now(),
		Modified:          time.Now(),
	}

	var err = database.Db.Insert(stateChange)
	if err != nil {
		log.Printf("Error saving security finding state change: %v\n", err.Error())
	}

	change.SecurityFinding.Modified = time.Now()
	change.SecurityFinding.LastComment = change.Comment
	change.SecurityFinding.LastAuthor = change.Author

	return database.Db.Update(change.SecurityFinding)
}

func insertFinding(database *Database, finding *model2.SecurityFinding) error {
	var comment = "inserted from source"
	var author = "source"
	//log.Println(finding.Key)
	//log.Printf("'Inserting finding': %v / %v / %v", finding.AccountId, finding.Region, finding.Key)
	findingId := uuid.NewV4().String()
	finding.Id = findingId
	finding.LastComment = comment
	finding.LastAuthor = author
	var err = database.Db.Insert(finding)

	if err != nil {
		bs, _ := json.Marshal(finding)
		log.Printf("Error inserting security finding: %v\n%v\n", err.Error(), string(bs))
		return err
	}

	stateChangeId := uuid.NewV4().String()
	stateChange := &model2.StateChange{
		Id:                stateChangeId,
		SpaceId:           finding.SpaceId,
		SecurityFindingId: findingId,
		Metadata:          finding.Metadata,
		State:             finding.State,
		Priority:          finding.Priority,
		Comment:           comment,
		Author:            author,
		Created:           time.Now(),
		Modified:          time.Now(),
	}
	err = database.Db.Insert(stateChange)
	if err != nil {
		log.Printf("Error saving security finding state change: %v\n", err.Error())
	}
	return nil
}

func updateFinding(database *Database, finding *model2.SecurityFinding, existingFinding *model2.SecurityFinding, comment string, author string) error {
	//log.Println("Updating finding: " + *existingFinding.AccountId + " / " + *existingFinding.Region + " / " + *existingFinding.Key)

	originalBytes, _ := json.Marshal(existingFinding)

	if existingFinding.State != finding.State || existingFinding.SourcePriority != finding.SourcePriority {
		stateChangeId := uuid.NewV4().String()
		var stateChange = &model2.StateChange{
			Id:                stateChangeId,
			SpaceId:           existingFinding.SpaceId,
			SecurityFindingId: existingFinding.Id,
			Metadata:          finding.Metadata,
			State:             finding.State,
			Priority:          finding.Priority,
			Comment:           comment,
			Author:            author,
			Created:           time.Now(),
			Modified:          time.Now(),
		}
		var err = database.Db.Insert(stateChange)
		if err != nil {
			log.Printf("Error saving security finding state change: %v\n", err.Error())
		}
	}

	existingFinding.State = finding.State
	existingFinding.AccountName = finding.AccountName

	// Replaced with map merge: existingFinding.Metadata = finding.Metadata

	// Copy new keys to existing metadata
	for k, v := range *finding.Metadata {
		(*existingFinding.Metadata)[k] = v
	}

	// Ensure that metadata key count does not grow larger than 30.
	if len(*existingFinding.Metadata) > 30 {
		keys := make([]string, 0, len(*existingFinding.Metadata))
		for k := range *existingFinding.Metadata {
			keys = append(keys, k)
		}
		sort.Strings(keys)
		removeCount := len(*existingFinding.Metadata) - 30
		for i := 0; i < removeCount; i++ {
			delete(*existingFinding.Metadata, keys[i])
		}
	}

	existingFinding.SourcePriority = finding.SourcePriority
	existingFinding.Priority = CalculateCompositePriority(existingFinding)

	if existingFinding.FirstObserved.After(finding.FirstObserved) {
		existingFinding.FirstObserved = finding.FirstObserved
	}
	if existingFinding.LastObserved.Before(finding.LastObserved) {
		existingFinding.LastObserved = finding.LastObserved
	}

	existingFinding.LastComment = comment
	existingFinding.LastAuthor = author
	existingFinding.Category = finding.Category
	existingFinding.SourceType = finding.SourceType
	existingFinding.SourceId = finding.SourceId
	existingFinding.SourceFindingUrl = finding.SourceFindingUrl
	existingFinding.ResourceType = finding.ResourceType
	existingFinding.ResourceId = finding.ResourceId
	existingFinding.Title = finding.Title
	existingFinding.Type = finding.Type
	existingFinding.TypeLabel = finding.TypeLabel
	existingFinding.Description = finding.Description
	existingFinding.Remediation = finding.Remediation
	existingFinding.RemediationUrl = finding.RemediationUrl
	existingFinding.AccountName = finding.AccountName
	existingFinding.Team = finding.Team
	existingFinding.EnvironmentType = finding.EnvironmentType

	if util2.IsFindingExpired(existingFinding) {
		existingFinding.State = model2.StateArchived
	}

	modifiedBytes, _ := json.Marshal(existingFinding)

	existingFinding.Modified = time.Now()

	if bytes.Compare(originalBytes, modifiedBytes) != 0 { // Update to database only if there are changes.
		err := database.Db.Update(existingFinding)
		if err != nil {
			log.Printf("Error updating security finding: %v\n%v\n", err.Error(), string(modifiedBytes))
			return err
		}
	}

	return nil
}

func CalculateCompositePriority(finding *model2.SecurityFinding) int {
	if finding.ManualPriority != nil {
		return *finding.ManualPriority
	} else if finding.RulePriority != nil {
		return *finding.RulePriority
	} else {
		return finding.SourcePriority
	}
}
