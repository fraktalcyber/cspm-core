package db

import (
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	"bitbucket.org/fraktalcyber/cspm-core/src/core/util"
	"crypto/tls"
	"errors"
	"fmt"
	"github.com/go-pg/pg/v9"
	"github.com/go-pg/pg/v9/types"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
	"time"
)

type Database struct {
	host          string
	port          int
	database      string
	user          string
	password      string
	ssl           bool
	log           bool
	schemaInstall bool
	Db            *pg.DB
}

var dbInitialized = false

func InitDatabase() {
	if !dbInitialized {
		log.Println("Initializing database.")
		types.RegisterAppender(map[string]interface{}{}, JsonMapAppender)
		types.RegisterScanner(map[string]interface{}{}, JsonMapScanner)
		dbInitialized = true
	}
}

func NewDatabase(host string, port int, database string, user string, password string, ssl bool, log bool, schemaInstall bool) *Database {
	InitDatabase()
	return &Database{
		host:          host,
		port:          port,
		database:      database,
		user:          user,
		password:      password,
		ssl:           ssl,
		log:           log,
		schemaInstall: schemaInstall,
	}
}

func NewApplicationDatabase() *Database {
	var err error

	databaseAddress := util.GetConfig("database_host", "")
	databasePort, err := strconv.Atoi(util.GetConfig("database_port", "5432"))
	if err != nil {
		panic(fmt.Sprintf("Error getting database port config:%+v", err))
	}

	databaseName := util.GetConfig("database_name", "")
	databaseUser := util.GetConfig("database_user", "")
	databaseSsl := "true" == util.GetConfig("database_ssl", "true")
	databaseLog := "true" == util.GetConfig("database_log", "false")
	schemaInstall := "true" == util.GetConfig("database_schema_install_enabled", "false")

	databasePassword, err := util.GetSecret("database_password")
	if err != nil {
		panic(fmt.Sprintf("Error getting database password secret:%+v", err))
	}

	return NewDatabase(databaseAddress, databasePort, databaseName, databaseUser, databasePassword, databaseSsl, databaseLog, schemaInstall)
}

func (t *Database) Start() error {
	var address = t.host + ":" + strconv.Itoa(t.port)
	var err error
	options := pg.Options{
		Addr:     address,
		User:     t.user,
		Password: t.password,
		Database: t.database,
	}
	if t.ssl {
		options.TLSConfig = &tls.Config{InsecureSkipVerify: true}
	}
	t.Db = pg.Connect(&options)
	if t.log {
		t.Db.AddQueryHook(Logger{})
	}
	_, err = t.Db.Exec("SELECT 1")
	if err != nil {
		log.Printf("database start failed: %+v", err)
		return errors.New("database connect failed")
	}

	log.Print("database connected.")

	return t.UpdateSchema()
}

func (t *Database) UpdateSchema() error {
	if t.schemaInstall {
		log.Printf("database schema install executing...\n")
		files, err := ioutil.ReadDir("schema")
		if err != nil {
			return err
		}

		_, err = t.Db.Exec(`SELECT COUNT(*) FROM version`)
		if err != nil {
			_, err = t.Db.Exec(`CREATE TABLE public.version (version integer NOT NULL,created timestamp with time zone NOT NULL,type character varying(30) COLLATE pg_catalog."default" NOT NULL,CONSTRAINT version_pkey PRIMARY KEY (type, version))`)
			if err != nil {
				return err
			}
			log.Printf("database schema version table created.\n")
		}

		for _, f := range files {
			fileName := f.Name()
			filePrefix := fileName[:strings.IndexByte(fileName, '.')]
			schemaType := "cspm"

			var versions []*model2.Version

			count, err := t.Db.Model(&versions).Where("type = ? AND version = ?", schemaType, filePrefix).Count()
			if err != nil {
				return err
			}

			if count == 0 {
				log.Printf("database schema update %v.\n", filePrefix)

				sqlFileContent, err := ioutil.ReadFile("schema/" + f.Name())
				if err != nil {
					return err
				}

				sql := string(sqlFileContent)

				_, err = t.Db.Exec(sql)
				if err != nil {
					return err
				}

				version := &model2.Version{
					Type:    schemaType,
					Version: filePrefix,
					Created: time.Now(),
				}
				err = t.Db.Insert(version)

				if err != nil {
					return err
				}

			}
		}
		log.Printf("database schema install executed.\n")
	}

	return nil
}

func (t *Database) Stop() error {
	log.Print("database stopping...")
	err := t.Db.Close()
	log.Print("database stopped.")
	return err
}
