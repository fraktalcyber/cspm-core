package db

import (
	"encoding/json"
	"fmt"
	"github.com/go-pg/pg/v9/types"
	"reflect"
)

func JsonMapAppender(in []byte, v reflect.Value, flags int) []byte {
	m := v.Interface().(map[string]interface{})
	bytes, err := json.Marshal(m)
	if err != nil {
		panic(err)
	}
	return types.AppendJSONB(in, bytes, flags)
}

func JsonMapScanner(v reflect.Value, rd types.Reader, n int) error {
	if !v.CanSet() {
		return fmt.Errorf("pg: Scan(nonsettable %s)", v.Type())
	}

	bytes, err := rd.ReadFull()
	if n == 6 && string(bytes) == "[null]" {
		return nil
	}
	if err != nil {
		return err
	}
	var i map[string]interface{}
	err = json.Unmarshal(bytes, &i)
	if err != nil {
		return err
	}
	v.Set(reflect.ValueOf(i))
	return nil
}
