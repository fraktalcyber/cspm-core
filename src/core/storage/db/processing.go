package db

import (
	"bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	"encoding/json"
	uuid "github.com/satori/go.uuid"
	"log"
	"time"
)

func InsertProcessingRun(database *Database, spaceId string, key string, periodStart time.Time, periodEnd time.Time) error {

	row := &model.ProcessingRun{
		Id:          uuid.NewV4().String(),
		SpaceId:     spaceId,
		Key:         key,
		PeriodStart: periodStart,
		PeriodEnd:   periodEnd,
		Created:     time.Now(),
		Modified:    time.Now(),
	}

	var err = database.Db.Insert(row)

	if err != nil {
		bytes, _ := json.Marshal(row)
		log.Printf("Error inserting processing run: %v\n%v\n", err.Error(), string(bytes))
		return err
	}

	return nil
}

func GetLatestProcessingRun(database *Database, spaceId string, key string) (*model.ProcessingRun, error) {
	var resources []*model.ProcessingRun
	err := database.Db.Model(&resources).Where("space_id = ? AND key = ?", spaceId, key).Limit(1).OrderExpr("created DESC").Select()
	if err != nil {
		log.Printf("Error selecting latest processing run: %v\n", err)
		return nil, err
	}
	if len(resources) > 0 {
		return resources[0], nil
	} else {
		return nil, nil
	}
}
