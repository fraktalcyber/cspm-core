package db

func DeleteExpiresRows(database *Database) error {
	_, err := database.Db.Exec(`DELETE FROM security_finding WHERE DATE_PART('day', now() - last_observed) > 182`)
	if err != nil {
		return err
	}

	_, err = database.Db.Exec(`DELETE FROM resource WHERE DATE_PART('day', now() - last_observed) > 182`)
	if err != nil {
		return err
	}

	_, err = database.Db.Exec(`DELETE FROM security_finding_report_row WHERE DATE_PART('day', now() - row_date) > 730`)
	if err != nil {
		return err
	}

	return nil
}
