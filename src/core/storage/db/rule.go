package db

import (
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	"log"
)

func LoadEnabledRules(database *Database, spaceId string) ([]*model2.SecurityFindingRule, error) {
	var findings []*model2.SecurityFindingRule
	err := database.Db.Model(&findings).Where("enabled = true AND space_id=?", spaceId).Select()
	if err != nil {
		log.Printf("Error selecting enabled rules from database: %v\n", err.Error())
		return nil, err
	}
	return findings, nil
}
