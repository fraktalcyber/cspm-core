package db

import (
	model "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	"encoding/json"
	uuid "github.com/satori/go.uuid"
	"log"
	"time"
)

func CountSecurityFindings(database *Database, spaceId string, accountId string, region string) (int, error) {
	var rows []*model.SecurityFinding
	count, err := database.Db.Model(&rows).Where("space_id = ? AND account_id = ? AND region = ?", spaceId, accountId, region).Count()
	if err != nil {
		log.Printf("Error counting security findings: %v\n", err.Error())
		return -1, err
	}
	return count, nil
}

func GetSecurityFindings(database *Database, spaceId string, accountId string, region string, offset int, limit int) ([]*model.SecurityFinding, error) {
	var rows []*model.SecurityFinding
	err := database.Db.Model(&rows).Where("space_id = ? AND account_id = ? AND region = ?", spaceId, accountId, region).Offset(offset).Limit(limit).Select()
	if err != nil {
		log.Printf("Error selecting security findings: %v\n", err.Error())
		return nil, err
	}
	return rows, nil
}

func GetSecurityFindingState(database *Database, securityFindingId string, time time.Time) (*model.StateChange, error) {
	var resources []*model.StateChange
	err := database.Db.Model(&resources).Where("security_finding_id = ? AND created < ?", securityFindingId, time).Limit(1).OrderExpr("created DESC").Select()
	if err != nil {
		log.Printf("Error selecting resource: %v\n", err)
		return nil, err
	}
	if len(resources) > 0 {
		return resources[0], nil
	} else {
		return nil, nil
	}
}

func SaveSecurityFindingReportRow(database *Database, row *model.SecurityFindingReportRow) error {
	var err error

	var rows []*model.SecurityFindingReportRow
	err = database.Db.Model(&rows).Where("key = ?", row.Key).Select()
	if err != nil {
		log.Printf("Error selecting existing security finding report rows: %v\n", err.Error())
		return err
	}

	if len(rows) > 0 {
		return updateSecurityFindingReportRow(database, row, rows[0])
	} else {
		return insertSecurityFindingReportRow(database, row)
	}
}

func insertSecurityFindingReportRow(database *Database, row *model.SecurityFindingReportRow) error {
	rowId := uuid.NewV4().String()
	row.Id = rowId
	var err = database.Db.Insert(row)

	if err != nil {
		bytes, _ := json.Marshal(row)
		log.Printf("Error inserting security finding report row: %v\n%v\n", err.Error(), string(bytes))
		return err
	}

	return nil
}

func updateSecurityFindingReportRow(database *Database, row *model.SecurityFindingReportRow, existingSecurityFindingReportRow *model.SecurityFindingReportRow) error {
	row.Id = existingSecurityFindingReportRow.Id

	err := database.Db.Update(existingSecurityFindingReportRow)
	if err != nil {
		bytes, _ := json.Marshal(row)
		log.Printf("Error updating security finding report row: %v\n%v\n", err.Error(), string(bytes))
		return err
	}

	return nil
}
