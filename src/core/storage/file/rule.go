package file

import (
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	"encoding/json"
	"io/ioutil"
	"strings"
)

func LoadRules(rulePath string) ([]*model2.Rule, error) {
	files, err := ioutil.ReadDir(rulePath)
	if err != nil {
		return nil, err
	}
	var rules []*model2.Rule
	for _, f := range files {
		//log.Println(f.Key())
		categoryRules, err := LoadRuleCategory(rulePath, f.Name())
		if err != nil {
			return nil, err
		}
		for _, r := range categoryRules {
			rules = append(rules, r)
		}
	}
	return rules, nil
}

func LoadRuleCategory(rulePath string, category string) ([]*model2.Rule, error) {
	//log.Println("Loading rule category: " + category)
	files, err := ioutil.ReadDir(rulePath + "/" + category)
	if err != nil {
		return nil, err
	}
	var rules []*model2.Rule
	for _, f := range files {
		//log.Println(f.Key())
		rule, err := LoadRule(rulePath, category, f.Name())
		if err != nil {
			return nil, err
		}
		rules = append(rules, rule)
	}
	return rules, nil
}

func LoadRule(rulePath string, category string, ruleName string) (*model2.Rule, error) {
	//log.Println("  - rule: " + category + "/" + ruleName)
	bytes, err := ioutil.ReadFile(rulePath + "/" + category + "/" + ruleName)
	if err != nil {
		return nil, err
	}
	var rule model2.Rule
	err = json.Unmarshal(bytes, &rule)
	if err != nil {
		return nil, err
	}
	rule.Key = strings.Split(ruleName, ".")[0]
	rule.Category = category
	return &rule, nil
}
