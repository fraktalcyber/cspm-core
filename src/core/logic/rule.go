package logic

import (
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	notification2 "bitbucket.org/fraktalcyber/cspm-core/src/core/notification"
	db2 "bitbucket.org/fraktalcyber/cspm-core/src/core/storage/db"
	file2 "bitbucket.org/fraktalcyber/cspm-core/src/core/storage/file"
	util2 "bitbucket.org/fraktalcyber/cspm-core/src/core/util"
	"encoding/json"
	"fmt"
	"github.com/mattn/go-jsonpointer"
	"log"
	"regexp"
	"sort"
	"strings"
	"time"
)

func ProcessRules(database *db2.Database, close *bool) {
	var spaceId = util2.GetConfig("space_id", "")

	rules, err := file2.LoadRules(util2.GetConfig("rule_path", "./data/rules"))
	if err != nil {
		log.Printf("Error loading rules: %v\n", err.Error())
		return
	}

	securityFindingRules, err := db2.LoadEnabledRules(database, spaceId)
	if err != nil {
		log.Printf("Error loading database rules: %v\n", err.Error())
	} else {
		log.Printf("Loaded rules from database: %v\n", len(securityFindingRules))
		for _, securityFindingRule := range securityFindingRules {
			rule := model2.Rule{
				Id:       securityFindingRule.Id,
				Category: "database",
				Key:      securityFindingRule.Key,
				Priority: securityFindingRule.Priority,
				Enabled:  securityFindingRule.Enabled,
				Criteria: []*model2.CriteriaPart{&securityFindingRule.Criteria},
				Action: model2.Action{
					Prioritize: securityFindingRule.Prioritize,
					Alert:      securityFindingRule.Alert,
					Archive:    securityFindingRule.Archive,
				},
			}
			//json, _ := util2.ToJson(&rule)
			//log.Printf("Loaded rule from database: %v\n", json)
			rules = append(rules, &rule)
		}
	}

	sort.SliceStable(rules, func(i, j int) bool {
		return rules[i].Priority < rules[j].Priority
	})

	batch := 1000
	count, err := db2.CountNonArchivedFindings(database, spaceId)
	if err != nil {
		log.Printf("Error counting findings: %v\n", err.Error())
		return
	}
	for i := 0; i < count; i += batch {
		findings, err := db2.LoadNonArchivedFindings(database, spaceId, batch, i)
		if err != nil {
			log.Printf("Error loading finding batch: %v, %v\n", i, err.Error())
			return
		}
		changes := ApplyRulesToFindings(rules, findings)
		for _, change := range changes {
			if change.Changed {
				err = db2.SaveFindingChange(database, change)
				if err != nil {
					log.Printf("Error saving change: %v.", err.Error())
				}
			}
			if change.AlertActivated {
				if err := notification2.SendEmailAlertActived(change.SecurityFinding); err != nil {
					log.Printf("error sending alert activation email: %v", err)
				}
			}
			if change.AlertDeactivated {
				if err := notification2.SendEmailAlertDeactivated(change.SecurityFinding); err != nil {
					log.Printf("error sending alert deactivation email: %v", err)
				}
			}
		}
		ArchiveExpiredFindings(database, findings)

		if *close {
			break
		}

		time.Sleep(300 * time.Millisecond)
	}
}

func ArchiveExpiredFindings(database *db2.Database, findings []*model2.SecurityFinding) {
	for _, finding := range findings {
		if util2.IsFindingExpired(finding) {
			archiveExpiredFinding(database, finding)
		}
	}

}

func archiveExpiredFinding(database *db2.Database, finding *model2.SecurityFinding) {
	finding.State = model2.StateArchived

	change := model2.RuleApplyResult{
		SecurityFinding:  finding,
		Author:           "rule processor",
		Comment:          "archived expired finding",
		Changed:          true,
		AlertDeactivated: true,
	}

	err := db2.SaveFindingChange(database, &change)
	if err != nil {
		log.Printf("Error saving archive change: %v.", err.Error())
	}
}

func ApplyRulesToFindings(rules []*model2.Rule, findings []*model2.SecurityFinding) []*model2.RuleApplyResult {
	var allChanges []*model2.RuleApplyResult
	for _, finding := range findings {
		changes := ApplyRulesToFinding(rules, finding)
		for _, change := range changes {
			allChanges = append(allChanges, change)
		}
	}
	return allChanges
}

func ApplyRulesToFinding(rules []*model2.Rule, finding *model2.SecurityFinding) []*model2.RuleApplyResult {
	var changes []*model2.RuleApplyResult

	// Conver to JSON Map so JSON path can be used in rule matching.
	var findingBytes, err = json.Marshal(finding)
	if err != nil {
		log.Printf("Error JSON marshalling finding %v: %v", finding.Key, err)
		return changes
	}
	var findingJsonMap map[string]interface{}
	err = json.Unmarshal(findingBytes, &findingJsonMap)
	if err != nil {
		log.Printf("Error JSON unmarshalling finding %v: %v", finding.Key, err)
		return changes
	}

	prioritizedBefore := finding.RulePriority != nil
	alertedBefore := finding.AlertState == model2.AlertStateActive
	prioritizedNow := false
	alertedNow := false

	for _, rule := range rules {

		result := ApplyRuleToFinding(rule, finding, &findingJsonMap)

		if result == nil {
			continue
		}

		if result.Changed {
			changes = append(changes, result)

			// Finding is changed then convert to JSON map so that next rules will be aware of the change.
			findingBytes, err = json.Marshal(finding)
			if err != nil {
				log.Printf("Error JSON marshalling finding %v: %v", finding.Key, err)
				return changes
			}
			err = json.Unmarshal(findingBytes, &findingJsonMap)
			if err != nil {
				log.Printf("Error JSON unmarshalling finding %v: %v", finding.Key, err)
				return changes
			}
		}

		if result.Prioritized {
			prioritizedNow = true
		}
		if result.Alerted {
			alertedNow = true
		}

	}

	if prioritizedBefore && !prioritizedNow {
		comment := "rule priority cleared\n"
		finding.RulePriority = nil
		finding.Priority = db2.CalculateCompositePriority(finding)
		changes = append(changes, &model2.RuleApplyResult{
			SecurityFinding: finding,
			Author:          "rule processor",
			Comment:         comment,
			Changed:         true,
		})
	}

	if alertedBefore && !alertedNow {
		comment := "alert deactivated\n"
		finding.AlertState = model2.AlertStateInactive
		changes = append(changes, &model2.RuleApplyResult{
			SecurityFinding:  finding,
			Author:           "rule processor",
			Comment:          comment,
			Changed:          true,
			AlertDeactivated: true,
		})
	}

	if !alertedBefore && alertedNow {
		comment := "alert activated\n"
		changes = append(changes, &model2.RuleApplyResult{
			SecurityFinding: finding,
			Author:          "rule processor",
			Comment:         comment,
			AlertActivated:  true,
		})
	}

	return changes
}

func MatchCriteria(criteria []*model2.CriteriaPart, item *map[string]interface{}) bool {
	combiningLogicalOperator := model2.OperatorUndefined

	var criteriaResult bool
	var criteriaResultSet bool
	for _, part := range criteria {
		// Set combining logical operator
		if part.Operator != nil && operatorIndexOf(*part.Operator, model2.CombiningLogicalOperators) != -1 {
			combiningLogicalOperator = *part.Operator
			//log.Printf("Set combining logical operator: %v\n", combiningLogicalOperator)
			continue
		}

		var partResult bool
		if part.Criteria != nil {
			partResult = MatchCriteria(part.Criteria, item)
			//log.Printf("Sub criteria result: %v\n", partResult)
		} else if part.Path != nil && part.Operator != nil && part.Value != nil {
			partResult = matchPath(*part.Path, *part.Operator, *part.Value, item)
			//log.Printf("Path match result %v, %v = %v\n", *part.Path, *part.Value, partResult)
		}

		if !criteriaResultSet {
			criteriaResult = partResult
			criteriaResultSet = true
		} else {
			switch combiningLogicalOperator {
			case model2.OperatorAnd:
				criteriaResult = criteriaResult && partResult
				//log.Printf("And with criteria result: %v\n", criteriaResult)
				continue
			case model2.OperatorOr:
				criteriaResult = criteriaResult || partResult
				//log.Printf("Or with criteria result: %v\n", criteriaResult)
				continue
			default:
				panic("Unsupported logical operator: " + combiningLogicalOperator)
			}
		}
	}

	return criteriaResult
}

func matchPath(path string, operator model2.Operator, regexpString string, item *map[string]interface{}) bool {
	result, err := jsonpointer.Get(*item, path)
	if err != nil {
		if strings.Index(err.Error(), "on zero Value") == -1 {
			log.Printf("Error matching path %v: %v.\n", path, err.Error())
		}
		return false
	}

	if result == nil {
		return false
	}

	stringValue, err := util2.ToJson(result)
	matched, err := regexp.MatchString(regexpString, stringValue)
	if err != nil {
		log.Printf("Error matching filter regexp %v with string %v: %v\n", regexpString, stringValue, err)
		return false
	}

	switch operator {
	case model2.OperatorEquals:
		return matched
	case model2.OperatorNotEquals:
		return !matched
	default:
		panic("Unsupported comparison operator: " + operator)
	}

	return matched
}

func operatorIndexOf(element model2.Operator, data []model2.Operator) int {
	for k, v := range data {
		if element == v {
			return k
		}
	}
	return -1
}

func ApplyRuleToFinding(rule *model2.Rule, finding *model2.SecurityFinding, findingJsonMap *map[string]interface{}) *model2.RuleApplyResult {
	if !rule.Enabled {
		return nil
	}

	if !MatchCriteria(rule.Criteria, findingJsonMap) {
		return nil
	}

	comment := ""

	if rule.Action.Prioritize != -1 && (finding.RulePriority == nil || *finding.RulePriority != rule.Action.Prioritize) {
		var message string
		if finding.RulePriority != nil {
			message = "rule priority set from " + fmt.Sprint(*finding.RulePriority) + " to " + fmt.Sprint(rule.Action.Prioritize)
		} else {
			message = "rule priority set from <nil> to " + fmt.Sprint(rule.Action.Prioritize)
		}
		comment = comment + message + "\n"
		finding.RulePriority = &rule.Action.Prioritize
	}
	if rule.Action.Archive && finding.State != "archived" {
		message := "state set from " + fmt.Sprint(finding.State) + " to " + fmt.Sprint(model2.StateArchived)
		comment = comment + message + "\n"
		finding.State = model2.StateArchived
	}
	if rule.Action.Alert && finding.AlertState != model2.AlertStateActive {
		message := "alert set from " + fmt.Sprint(finding.AlertState) + " to " + fmt.Sprint(model2.AlertStateActive)
		comment = comment + message + "\n"
		finding.AlertState = model2.AlertStateActive
	}
	finding.Priority = db2.CalculateCompositePriority(finding)
	return &model2.RuleApplyResult{
		SecurityFinding: finding,
		Author:          rule.Category + "/" + rule.Key,
		Comment:         comment,
		Prioritized:     rule.Action.Prioritize != -1,
		Changed:         comment != "",
		Alerted:         rule.Action.Alert,
	}
}
