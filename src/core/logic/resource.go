package logic

import (
	db2 "bitbucket.org/fraktalcyber/cspm-core/src/core/storage/db"
	"log"
	"time"
)

func ArchiveExpiredResources(database *db2.Database, close *bool) {
	now := time.Now()

	expiration := now.Add(-time.Hour * time.Duration(12))

	batch := 50
	count, err := db2.CountExpiredResources(database, expiration)
	if err != nil {
		log.Printf("Error counting expired resources: %v\n", err.Error())
		return
	}
	for i := 0; i < count; i += batch {
		resources, err := db2.LoadExpiredResources(database, expiration, i, batch)
		if err != nil {
			log.Printf("Error loading expired resources batch: %v, %v\n", i, err.Error())
			return
		}

		for _, resource := range resources {
			if err := db2.ArchiveResource(database, resource); err != nil {
				log.Printf("error archiving resource: %v", err)
			}
		}

		if *close {
			break
		}
	}
}
