package logic

import (
	"bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	"bitbucket.org/fraktalcyber/cspm-core/src/core/storage/db"
	"fmt"
	"log"
	"strconv"
	"time"
)

func UpdateSecurityFindingReportRowHistory(database *db.Database, currentYear int, currentMonth int, currentDay int, historyLengthInDays int, dayBatchSize int,
	spaceId string, accountId string, region string, close *bool) error {
	localLocation := time.Now().Location()

	currentDate := time.Date(currentYear, time.Month(currentMonth), currentDay, 0, 0, 0, 0, localLocation)

	latestRow, err := db.GetLatestProcessingRun(database, spaceId, GetSecuringFindingReportProcessingRunKey(accountId, region))
	if err != nil {
		return err
	}

	var startDate time.Time
	if latestRow != nil {
		startDate = latestRow.PeriodEnd.In(localLocation).Add(time.Duration(24*1) * time.Hour)
	} else {
		startDate = currentDate.Add(-time.Duration(24*historyLengthInDays) * time.Hour)
	}

	if startDate.UnixNano() >= currentDate.UnixNano() {
		return nil
	}

	days := int(currentDate.Sub(startDate) / (time.Duration(24) * time.Hour))

	if days > dayBatchSize {
		days = dayBatchSize
	}

	err = UpdateSecurityFindingReportRows(database, startDate.Year(), int(startDate.Month()), startDate.Day(), days, spaceId, accountId, region, close)
	if err != nil {
		return err
	}

	return nil
}

func UpdateSecurityFindingReportRows(database *db.Database, startYear int, startMonth int, startDay int, days int,
	spaceId string, accountId string, region string, close *bool) error {

	log.Printf("Updating security finding report rows %v/%v starting %04d-%02d-%02d with %v days window", accountId, region, startYear, startMonth, startDay, days)

	reportRows, err := calculateSecurityFindingReportRows(database, startYear, startMonth, startDay, days, spaceId, accountId, region, close)
	if err != nil {
		return err
	}
	if *close {
		return err
	}

	localLocation := time.Now().Location()
	startDate := time.Date(startYear, time.Month(startMonth), startDay, 0, 0, 0, 0, localLocation)

	err = saveSecurityFindingReportRows(database, spaceId, accountId, region, startDate, startDate.Add(time.Duration(24*(days-1))*time.Hour), reportRows)
	if err != nil {
		return err
	}

	return err
}

func calculateSecurityFindingReportRows(database *db.Database, startYear int, startMonth int, startDay int, days int, spaceId string, accountId string, region string, close *bool) (map[string]*model.SecurityFindingReportRow, error) {
	reportRows := make(map[string]*model.SecurityFindingReportRow)

	localLocation := time.Now().Location()
	startDate := time.Date(startYear, time.Month(startMonth), startDay, 0, 0, 0, 0, localLocation)

	// Load all findings in region
	batch := 300
	count, err := db.CountSecurityFindings(database, spaceId, accountId, region)
	if err != nil {
		log.Printf("Error counting findings: %v\n", err.Error())
		return nil, err
	}

	for i := 0; i < count; i += batch {
		findingRows, err := db.GetSecurityFindings(database, spaceId, accountId, region, i, batch)
		if err != nil {
			log.Printf("Error loading finding batch: %v, %v\n", i, err.Error())
			return nil, err
		}

		for _, finding := range findingRows {

			for d := 0; d < days; d++ {
				startOfDay := startDate.Add(time.Duration(d*24) * time.Hour)
				endOfDay := startDate.Add(time.Duration((d+1)*24) * time.Hour)

				if finding.Created.UnixNano() > endOfDay.UnixNano() || finding.LastObserved.Add(time.Duration(24*30)*time.Hour).UnixNano() < endOfDay.UnixNano() {
					continue
				}

				state, err := db.GetSecurityFindingState(database, finding.Id, endOfDay)
				if err != nil {
					log.Printf("Error loading security finding state findings: %v\n", err.Error())
					return nil, err
				}
				if state == nil {
					log.Printf("Warning, no state for security finding: %v\n", finding.Id)
					continue
				}

				year := startOfDay.Year()
				month := int(startOfDay.Month())
				day := startOfDay.Day()
				reportRow := getSecurityFindingReportRow(year, month, day, spaceId, finding, state.Priority, reportRows)
				if state.State == model.StateActive {
					reportRow.FindingCount++
				}
			}

		}

		if *close {
			break
		}

		time.Sleep(100 * time.Millisecond)
	}

	return reportRows, nil
}

func saveSecurityFindingReportRows(database *db.Database, spaceId string, accountId string, region string, periodStart time.Time, periodEnd time.Time, reportRows map[string]*model.SecurityFindingReportRow) error {
	tx, err := database.Db.Begin()
	if err != nil {
		return err
	}
	for _, reportRow := range reportRows {
		err := db.SaveSecurityFindingReportRow(database, reportRow)
		if err != nil {
			err2 := tx.Rollback()
			log.Printf("Error rolling back transaction: %v.", err2.Error())
			return err
		}
	}

	err = db.InsertProcessingRun(database, spaceId, GetSecuringFindingReportProcessingRunKey(accountId, region), periodStart, periodEnd)
	if err != nil {
		err2 := tx.Rollback()
		log.Printf("Error rolling back transaction: %v.", err2.Error())
		return err
	}

	err = tx.Commit()
	if err != nil {
		return err
	}

	return nil
}

func GetSecuringFindingReportProcessingRunKey(accountId string, region string) string {
	return accountId + "/" + region + "/security-finding-report"
}

func getSecurityFindingReportRow(year int, month int, day int,
	spaceId string,
	finding *model.SecurityFinding,
	priority int,
	reportRows map[string]*model.SecurityFindingReportRow) *model.SecurityFindingReportRow {

	key := getSecurityFindingReportRowKey(year, month, day, finding, priority)

	var reportRow *model.SecurityFindingReportRow
	if val, ok := reportRows[key]; ok {
		reportRow = val
	} else {
		reportRow = &model.SecurityFindingReportRow{
			Id:              "",
			Key:             key,
			SpaceId:         spaceId,
			AccountId:       finding.AccountId,
			AccountName:     finding.AccountName,
			Region:          finding.Region,
			Team:            finding.Team,
			EnvironmentType: finding.EnvironmentType,
			SourceType:      finding.SourceType,
			Category:        finding.Category,
			Type:            finding.Type,
			Priority:        priority,
			FindingCount:    0,
			RowDate:         time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Now().Location()),
			Created:         time.Now(),
			Modified:        time.Now(),
		}
		reportRows[key] = reportRow
	}

	return reportRow
}

func getSecurityFindingReportRowKey(year int, month int, day int,
	finding *model.SecurityFinding,
	priority int) string {

	return fmt.Sprintf("%04d-%02d-%02d", year, month, day) + "/" +
		finding.AccountId + "/" +
		finding.Region + "/" +
		finding.SourceType + "/" +
		string(finding.Category) + "/" +
		finding.Type + "/" +
		strconv.Itoa(priority)
}
