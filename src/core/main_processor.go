package core

import (
	aws2 "bitbucket.org/fraktalcyber/cspm-core/src/core/integration/aws"
	"bitbucket.org/fraktalcyber/cspm-core/src/core/integration/shodan"
	"bitbucket.org/fraktalcyber/cspm-core/src/core/logic"
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	"bitbucket.org/fraktalcyber/cspm-core/src/core/processor"
	"bitbucket.org/fraktalcyber/cspm-core/src/core/processor/common"
	db2 "bitbucket.org/fraktalcyber/cspm-core/src/core/storage/db"
	util2 "bitbucket.org/fraktalcyber/cspm-core/src/core/util"
	"log"
	"strings"
	"time"
)

type MainProcessor struct {
	processingContext      *processor.ProcessingContext
	findingProcessor       *processor.FindingProcessor
	resourceProcessor      *processor.ResourceProcessor
	vulnerabilityProcessor *processor.VulnerabilityProcessor
	genericProcessors      []*common.GenericProcessor
	longRunningProcessors  []*common.GenericProcessor
	extensions             []Extension
}

func NewMainProcessor(extensions []Extension) *MainProcessor {
	return &MainProcessor{
		extensions: extensions,
	}
}

func (h *MainProcessor) Start() error {
	log.Print("Main Processor started.")

	var err error

	// Load scope
	organizationContext, err := aws2.GetOrganizationContext()
	if err != nil {
		return err
	}

	// Construct processing context and components
	h.processingContext = &processor.ProcessingContext{
		Database:        db2.NewApplicationDatabase(),
		SecurityFinding: make(chan *model2.SecurityFinding),
		Resource:        make(chan *model2.Resource),
		Vulnerability:   make(chan *model2.Vulnerability),
	}

	h.findingProcessor = processor.NewSecurityFindingProcessor(h.processingContext)
	h.resourceProcessor = processor.NewResourceProcessor(h.processingContext)
	h.vulnerabilityProcessor = processor.NewVulnerabilityProcessor(h.processingContext)

	h.genericProcessors = append(h.genericProcessors, common.NewGenericProcessor("Rule Processor", nil, h.processingContext, 1*time.Minute, 0, len(organizationContext.AccountContexts), func(accountContext *model2.AccountContext, processingContext *processor.ProcessingContext, close *bool) {
		database := processingContext.Database
		logic.ProcessRules(database, close)
	}))

	h.genericProcessors = append(h.genericProcessors, common.NewGenericProcessor("Resource Archive Processor", nil, h.processingContext, 1*time.Hour, 0, len(organizationContext.AccountContexts), func(accountContext *model2.AccountContext, processingContext *processor.ProcessingContext, close *bool) {
		database := processingContext.Database
		logic.ArchiveExpiredResources(database, close)
	}))

	h.genericProcessors = append(h.genericProcessors, common.NewGenericProcessor("Cleanup Processor", nil, h.processingContext, 3*time.Hour, 0, len(organizationContext.AccountContexts), func(accountContext *model2.AccountContext, processingContext *processor.ProcessingContext, close *bool) {
		database := processingContext.Database
		err := db2.DeleteExpiresRows(database)
		if err != nil {
			log.Printf("Error deleting expired rows: %v\n", err)
		}
	}))

	if util2.GetConfig("osint_enabled", "false") == "true" {
		h.genericProcessors = append(h.genericProcessors, common.NewGenericProcessor("Open Source Intelligence Processor", nil, h.processingContext, 6*time.Hour, 0, len(organizationContext.AccountContexts), func(accountContext *model2.AccountContext, processingContext *processor.ProcessingContext, close *bool) {
			err := shodan.GetResourceScans(accountContext, processingContext.Database, processingContext.Resource, processingContext.Vulnerability, close)
			if err != nil {
				log.Printf("Error getting resource scans: %v\n", err)
			}
		}))
	}

	for accountIndex, accountContext := range organizationContext.AccountContexts {
		if util2.GetConfig("report_enabled", "false") == "true" {
			h.genericProcessors = append(h.genericProcessors, common.NewGenericProcessor("Report Processor", accountContext, h.processingContext, 60*time.Minute, accountIndex, len(organizationContext.AccountContexts), func(accountContext *model2.AccountContext, processingContext *processor.ProcessingContext, close *bool) {
				now := time.Now()
				err := logic.UpdateSecurityFindingReportRowHistory(processingContext.Database, now.Year(), int(now.Month()), now.Day(), 182, 10, accountContext.SpaceId, accountContext.Id, accountContext.Region, close)
				if err != nil {
					log.Printf("Error updating security finding report rows: %v\n", err)
				}
			}))
		}

		if util2.GetConfig("loki_enabled", "false") == "true" {
			h.genericProcessors = append(h.genericProcessors, common.NewGenericProcessor("Loki Processor", accountContext, h.processingContext, 15*time.Minute, accountIndex, len(organizationContext.AccountContexts), func(accountContext *model2.AccountContext, processingContext *processor.ProcessingContext, close *bool) {
				err := aws2.GetLokiFindings(organizationContext.LogAccountContext, accountContext, processingContext.SecurityFinding)
				if err != nil {
					log.Printf("Error getting Loki findings: %v\n", err)
				}
			}))
		}

		h.genericProcessors = append(h.genericProcessors, common.NewGenericProcessor("AWS CSPM Processor", accountContext, h.processingContext, 1*time.Hour, accountIndex, len(organizationContext.AccountContexts), func(accountContext *model2.AccountContext, processingContext *processor.ProcessingContext, close *bool) {
			err := aws2.GetCsmpFindings(accountContext, &processingContext.SecurityFinding)
			if err != nil {
				log.Printf("Error getting CSPM findings: %v\n", err)
			}
		}))

		h.genericProcessors = append(h.genericProcessors, common.NewGenericProcessor("AWS SecurityHub Processor "+accountContext.Name+" ("+accountContext.Id+")", accountContext, h.processingContext, 1*time.Hour, accountIndex, len(organizationContext.AccountContexts), func(accountContext *model2.AccountContext, processingContext *processor.ProcessingContext, close *bool) {
			err := aws2.GetSecurityHubFindings(accountContext, &processingContext.SecurityFinding)
			if err != nil {
				log.Printf("Error getting SecurityHub findings: %v\n", err)
			}
		}))

		h.genericProcessors = append(h.genericProcessors, common.NewGenericProcessor("AWS Resource Processor", accountContext, h.processingContext, 1*time.Hour, accountIndex, len(organizationContext.AccountContexts), func(accountContext *model2.AccountContext, processingContext *processor.ProcessingContext, close *bool) {
			err := aws2.GetResources(accountContext, processingContext.Resource)
			if err != nil {
				log.Printf("Error getting resources: %v\n", err)
			}
		}))

		if util2.GetConfig("trusted_advisor_enabled", "false") == "true" {
			h.genericProcessors = append(h.genericProcessors, common.NewGenericProcessor("AWS Trusted Advisor Processor", accountContext, h.processingContext, 1*time.Hour, accountIndex, len(organizationContext.AccountContexts), func(accountContext *model2.AccountContext, processingContext *processor.ProcessingContext, close *bool) {
				if accountContext.Region != "us-east-1" {
					return // Skip other regions than us-east-1
				}
				err := aws2.GetTrustedAdvisorCheckResults(accountContext, processingContext.SecurityFinding)
				if err != nil {
					if strings.Contains(err.Error(), "AWS Premium Support") && (accountContext.EnvironmentType == "development" || accountContext.EnvironmentType == "test") {
						log.Printf("AWS business or premium support not enabled. Skipping Trusted Advisor checks for account: " + accountContext.Name)
					} else {
						log.Printf("Error getting resources: %v\n", err)
					}
				}
			}))
		}

		for _, extension := range h.extensions {
			err, h.genericProcessors = extension.AddGenericProcessors(h.processingContext, h.genericProcessors, accountContext, accountIndex, organizationContext, err)
			if err != nil {
				log.Printf("Error adding generic processors from extension %v: %v\n", extension.Name(), err.Error())
			}
		}
	}

	// Start components
	err = h.processingContext.Database.Start()
	if err != nil {
		log.Printf("Error starting database: %v\n", err.Error())
		return err
	}

	for _, extension := range h.extensions {
		err = extension.Start()
		if err != nil {
			log.Printf("Error starting extension %v: %v\n", extension.Name(), err.Error())
			return err
		}
	}

	err = h.findingProcessor.Start()
	if err != nil {
		log.Printf("Error starting finding processor: %v\n", err.Error())
		return err
	}

	err = h.resourceProcessor.Start()
	if err != nil {
		log.Printf("Error starting resource processor: %v\n", err.Error())
		return err
	}

	err = h.vulnerabilityProcessor.Start()
	if err != nil {
		log.Printf("Error starting resource processor: %v\n", err.Error())
		return err
	}

	for _, processor := range h.genericProcessors {
		err := processor.Start()
		if err != nil {
			log.Printf("Error starting generic processor: %v\n", err.Error())
			return err
		}
	}

	for _, processor := range h.longRunningProcessors {
		err := processor.Start()
		if err != nil {
			log.Printf("Error starting log running processor: %v\n", err.Error())
			return err
		}
	}

	return nil
}

func (h *MainProcessor) Stop() error {
	log.Print("Main Processor stopping...")

	var err error
	util2.Reverse(h.genericProcessors)

	for _, processor := range h.longRunningProcessors {
		processor.RequestStop()
	}
	for _, processor := range h.longRunningProcessors {
		processor.WaitStop()
	}

	for _, processor := range h.genericProcessors {
		processor.RequestStop()
	}
	for _, processor := range h.genericProcessors {
		processor.WaitStop()
	}

	err = h.vulnerabilityProcessor.Stop()
	if err != nil {
		log.Printf("Error stopping resource processor: %v\n", err.Error())
	}

	err = h.resourceProcessor.Stop()
	if err != nil {
		log.Printf("Error stopping resource processor: %v\n", err.Error())
	}

	err = h.findingProcessor.Stop()
	if err != nil {
		log.Printf("Error stopping finding processor: %v\n", err.Error())
	}

	for _, extension := range h.extensions {
		err = extension.Stop()
		if err != nil {
			log.Printf("Error stopping extension main processor: %v, %v\n", extension.Name(), err.Error())
		}
	}

	err = h.processingContext.Database.Stop()
	if err != nil {
		log.Printf("Error stopping database: %v\n", err.Error())
	}

	log.Print("Main Processor stopped.")
	return nil
}
