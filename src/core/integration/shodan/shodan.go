package shodan

import (
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	db2 "bitbucket.org/fraktalcyber/cspm-core/src/core/storage/db"
	util2 "bitbucket.org/fraktalcyber/cspm-core/src/core/util"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	time "time"
)

func GetResourceScans(accountContext *model2.AccountContext, database *db2.Database, resourceChannel chan *model2.Resource, vulnerabilityChannel chan *model2.Vulnerability, close *bool) error {
	batch := 50
	count, err := db2.CountActiveResources(database, model2.ResourceCategoryIp, "public-ip")
	if err != nil {
		log.Printf("Error counting active public IP resources: %v\n", err.Error())
		return err
	}
	for i := 0; i < count; i += batch {
		resources, err := db2.LoadActiveResources(database, model2.ResourceCategoryIp, "public-ip", i, batch)
		if err != nil {
			log.Printf("Error loading active public IP resources batch: %v, %v\n", i, err.Error())
			return err
		}

		for _, resource := range resources {

			err = GetShodanScanForIpResource(resource, resourceChannel, vulnerabilityChannel)
			if err != nil {
				log.Printf("Error getting shodan scan for IP %+v: %+v", resource.Key, err)
				return err
			}

			if *close {
				break
			}

			time.Sleep(3 * time.Second)
		}

		if *close {
			break
		}
	}

	return nil
}

func GetShodanScanForIpResource(ipResource *model2.Resource, resourceChannel chan *model2.Resource, vulnerabilityChannel chan *model2.Vulnerability) error {
	shodanKey, err := util2.GetSecret("shodan_key")
	if err != nil {
		log.Printf("Error getting shodan key: %+v", err)
		return err
	}

	url := "https://api.shodan.io/shodan/host/" + ipResource.Key + "?key=" + shodanKey

	//log.Printf("Shodan IP (%+v) scan report request started. ", ipResource.Key)
	ctx := context.Background()
	req, _ := http.NewRequest(http.MethodGet, url, nil)
	req = req.WithContext(ctx)

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Printf("Error getting scan from shodan API: %+v", err)
		return err
	}
	defer res.Body.Close()

	if res.StatusCode == 404 {
		//log.Printf("Shodan IP (%+v) scan report response, not found (404)", ipResource.Key)
		return nil
	}

	if res.StatusCode != 200 {
		log.Printf("Error getting scan report from shodan API for IP (%+v): %+v", ipResource.Key, res.StatusCode)
		return err
	}

	bodyBytes, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Printf("Error reading scan from shodan API response body: %+v", err)
		return err
	}

	err = ProcessShodanScanResult(ipResource, bodyBytes, resourceChannel, vulnerabilityChannel)

	//log.Println(string(bodyBytes))

	if err != nil {
		log.Printf("Error processing shodan API scan response body: %+v", err)
		return err
	}

	return nil
}

func ProcessShodanScanResult(ipResource *model2.Resource, bytes []byte, resourceChannel chan *model2.Resource, vulnerabilityChannel chan *model2.Vulnerability) error {
	now := time.Now()
	var scanResult map[string]interface{}
	err := json.Unmarshal(bytes, &scanResult)
	if err != nil {
		log.Printf("Error parsing shodan API scan respone: %+v", err)
		return err
	}

	//log.Println(" hostnames:")
	/*if hs, ok := scanResult["hostnames"]; ok {
		var hostnames = hs.([]interface{})
		for _, h := range hostnames {
			var hostname = h.(string)
			log.Println("  " + hostname)
		}
	}*/

	//log.Println(" ports:")
	if data, ok := scanResult["data"]; ok {
		var portScans = data.([]interface{})

		for _, p := range portScans {
			var portScan = p.(map[string]interface{})
			var transport = portScan["transport"].(string)
			var port = portScan["port"].(float64)
			var product = util2.ValueToStringPointer(portScan["product"])
			var version = util2.ValueToStringPointer(portScan["version"])
			var timestampString = portScan["timestamp"].(string)
			var timestamp, err = time.Parse("2006-01-02T15:04:05.000000", timestampString)

			if err != nil {
				log.Printf("Error parsing shodan API port scan timestamp: %+v.\n", err)
				return err
			}
			//log.Printf("Port scan (%+v,%+v) result for IP resource %v (%+v): ", timestamp, timestampString, ipResource.Key, ipResource.FirstObserved)
			if ipResource.FirstObserved.After(timestamp) {
				//log.Printf("Ignored old port scan (%+v) result for new IP resource %v (%+v): ", timestamp, ipResource.Key, ipResource.FirstObserved)
				continue
			}
			var portVulnerabilityKeys []string
			//log.Printf("  %v/%v %v %v", port, transport, util.FormatStringPointer(product), util.FormatStringPointer(version))

			var resourceType = transport + "-port"
			var resourceId = ipResource.Key + "/" + fmt.Sprintf("%.0f", port)

			var labels = map[string]interface{}{}
			labels["aws_account_id"] = ipResource.AccountId
			labels["aws_region"] = ipResource.Region
			labels["product"] = product
			labels["version"] = version

			if vs, ok := portScan["vulns"]; ok {
				var vulnerabilities = vs.(map[string]interface{})
				for vulnId, v := range vulnerabilities {
					var vulnerability = v.(map[string]interface{})

					var cvss float64 = -1
					switch v := vulnerability["cvss"].(type) {
					case int:
						cvss = float64(v)
					case float64:
						cvss = v
					case string:
						cvss, err = strconv.ParseFloat(v, 64)
						if err != nil {
							log.Printf("Error parsing cvss (%+v): %+v\n", v, err)
							cvss = -1
						}
					default:
						cvss = -1
					}

					var summary = vulnerability["summary"].(string)
					if len(summary) > 400 {
						summary = summary[0:400]
					}
					//log.Printf("   %v %v - %v", vulnId, cvssString, summary)

					var referenceArray []string = []string{}
					if rs, ok := vulnerability["references"]; ok {
						var references = rs.([]interface{})
						for _, r := range references {
							var reference = r.(string)
							//log.Printf("    %v", reference)
							referenceArray = append(referenceArray, reference)
						}
					}
					if err != nil {
						log.Printf("Error parsing shodan API vulnerability cvss: %+v.\n", err)
						return err
					}

					//log.Printf("Found resource %+v vulnerability: %+v.\n", ipResource.Id, vulnId)

					vulnerabilityMetadata := v.(map[string]interface{})

					vulnerabilityChannel <- &model2.Vulnerability{
						Id:         "",
						Key:        vulnId,
						Source:     "shodan",
						Summary:    summary,
						Severity:   CvssToSeverity(cvss),
						Score:      cvss,
						Metadata:   &vulnerabilityMetadata,
						References: referenceArray,
						Created:    now,
						Modified:   now,
					}

					portVulnerabilityKeys = append(portVulnerabilityKeys, vulnId)
				}
			}

			//log.Printf("Saving resource %+v with %+v vulnerabilities", ipResource.Id, len(portVulnerabilityKeys))
			resourceMetadata := p.(map[string]interface{})
			resourceChannel <- &model2.Resource{
				Id:                "",
				ParentId:          &ipResource.Id,
				SpaceId:           ipResource.SpaceId,
				AccountId:         ipResource.AccountId,
				AccountName:       ipResource.AccountName,
				Region:            ipResource.Region,
				Team:              ipResource.Team,
				EnvironmentType:   ipResource.EnvironmentType,
				Category:          model2.ResourceCategoryPort,
				Type:              resourceType,
				Key:               resourceId,
				Labels:            &labels,
				Metadata:          &resourceMetadata,
				State:             model2.ResourceStateActive,
				Created:           now,
				Modified:          now,
				FirstObserved:     timestamp,
				LastObserved:      timestamp,
				VulnerabilityKeys: portVulnerabilityKeys,
			}
		}
	}

	return nil
}

func CvssToSeverity(cvss float64) int {
	if cvss == 0 {
		return 1
	}
	if cvss < 4 {
		return 2
	}
	if cvss < 7 {
		return 3
	}
	if cvss < 9 {
		return 4
	}
	return 5
}
