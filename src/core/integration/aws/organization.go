package aws

import (
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	util2 "bitbucket.org/fraktalcyber/cspm-core/src/core/util"
	"errors"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/organizations"
	"log"
	"strings"
)

func GetOrganizationContext() (*model2.OrganizationContext, error) {
	var masterAccountId = util2.GetConfig("aws_organization_master_account_id", "")
	var logAccountId = util2.GetConfig("aws_log_account_id", "")
	var iamRole = util2.GetConfig("aws_iam_role", "")
	var regions = strings.Split(util2.GetConfig("aws_regions", ""), ",")
	var spaceId = util2.GetConfig("space_id", "")

	var accountIdsString = util2.GetConfig("aws_account_ids", "*")
	var accountNamesString = util2.GetConfig("aws_account_names", "*")

	if accountNamesString == "*" {
		accountNamesString = accountIdsString
	}

	var err error
	var organizationContext *model2.OrganizationContext

	if accountIdsString != "*" {
		organizationContext, err = populateOrganizationContext(accountIdsString, accountNamesString, regions, iamRole, spaceId, masterAccountId, logAccountId)
	} else {
		organizationContext, err = populateOrganizationContextFromAws(masterAccountId, logAccountId, iamRole, regions, spaceId)
	}
	if err != nil {
		return nil, err
	}

	// Collect account id name map to each account context.
	for _, accountContext := range organizationContext.AccountContexts {
		for _, ac := range organizationContext.AccountContexts {
			accountContext.AccountNames[ac.Id] = ac.Name
			accountContext.AccountContexts[ac.Id] = ac
		}
	}

	return organizationContext, nil
}

func populateOrganizationContext(accountIdsString string, accountNamesString string, regions []string, iamRole string, spaceId string, masterAccountId string, logAccountId string) (*model2.OrganizationContext, error) {
	log.Println("Populating organization context.")
	organizationContext := &model2.OrganizationContext{}
	var accountIds = strings.Split(accountIdsString, ",")
	var accountNames = strings.Split(accountNamesString, ",")
	var awsPrimaryRegion = util2.GetConfig("aws_primary_region", "eu-west-1")
	if len(accountIds) != len(accountNames) {
		return nil, fmt.Errorf("config aws_account_ids and aws_accound_names have different length")
	}
	for i := 0; i < len(accountIds); i++ {
		accountId := accountIds[i]
		accountName := accountNames[i]
		for _, region := range regions {
			accountContext := newAccountContext(accountId, accountName, region, iamRole, spaceId, "default", "production")
			organizationContext.AccountContexts = append(organizationContext.AccountContexts, accountContext)
			if accountId == masterAccountId && region == awsPrimaryRegion {
				organizationContext.MasterAccountContext = accountContext
			}
			if accountId == logAccountId && region == awsPrimaryRegion {
				organizationContext.LogAccountContext = accountContext
			}
		}
	}
	if organizationContext.LogAccountContext == nil {
		organizationContext.LogAccountContext = newAccountContext(logAccountId, util2.GetConfig("AWS_LOG_ACCOUNT_NAME", ""), awsPrimaryRegion, iamRole, spaceId, "default", "production")
	}
	return organizationContext, nil
}

func populateOrganizationContextFromAws(masterAccountId string, logAccountId string, iamRole string, regions []string, spaceId string) (*model2.OrganizationContext, error) {
	log.Println("Populating organization context from AWS organization.")

	tagKeyTeam := util2.GetConfig("aws_account_tag_key_team", "team")
	tagKeyEnvironmentType := util2.GetConfig("aws_account_tag_key_environment_type", "environment-type")
	var awsPrimaryRegion = util2.GetConfig("aws_primary_region", "eu-west-1")
	log.Printf("Tag key team: '%+v'", tagKeyTeam)
	log.Printf("Tag key environment type: '%+v'", tagKeyEnvironmentType)

	var organizationContext = &model2.OrganizationContext{}
	var masterRoleArn = "arn:aws:iam::" + masterAccountId + ":role/" + iamRole
	credentials := stscreds.NewCredentials(session.Must(session.NewSession()), masterRoleArn)
	sess := session.Must(session.NewSession(&aws.Config{Credentials: credentials}))
	svc := organizations.New(sess)

	var nextToken *string
	for {
		input := &organizations.ListAccountsInput{}
		input.NextToken = nextToken
		result, err := svc.ListAccounts(input)
		if err != nil {
			return nil, errors.New("Error listing accounts: " + err.Error())
		}

		for _, account := range result.Accounts {
			if *account.Status == "SUSPENDED" {
				log.Printf("Skipped suspended account: %+v\n", *account.Name)
				continue
			} else {
				log.Printf("Account status: %+v = %+v\n", *account.Name, *account.Status)
			}
			for _, region := range regions {

				accountId := *account.Id
				accountName := *account.Name

				team, environmentKey, err := getAccountTags(svc, accountId, tagKeyTeam, tagKeyEnvironmentType)
				if err != nil {
					log.Println("Error reading account tags.", err)
				}

				accountContext := newAccountContext(accountId, accountName, region, iamRole, spaceId, team, environmentKey)
				organizationContext.AccountContexts = append(organizationContext.AccountContexts, accountContext)
				if *account.Id == masterAccountId && region == awsPrimaryRegion {
					organizationContext.MasterAccountContext = accountContext
				}
				if *account.Id == logAccountId && region == awsPrimaryRegion {
					organizationContext.LogAccountContext = accountContext
				}

			}
		}

		log.Printf("Loaded page of %v accounts.", len(result.Accounts))

		nextToken = result.NextToken
		if nextToken == nil {
			break
		}
	}

	log.Printf("Populated total of %v accounts from AWS organization.", len(organizationContext.AccountContexts))

	return organizationContext, nil
}

func getAccountTags(svc *organizations.Organizations, accountId string, tagKeyTeam string, tagKeyEnvironmentType string) (string, string, error) {
	var team = ""
	var environmentKey = ""
	err := svc.ListTagsForResourcePages(&organizations.ListTagsForResourceInput{
		ResourceId: &accountId,
	}, func(output *organizations.ListTagsForResourceOutput, lastPage bool) bool {
		for _, tag := range output.Tags {
			if *tag.Key == tagKeyTeam {
				team = *tag.Value
			}
			if *tag.Key == tagKeyEnvironmentType {
				environmentKey = *tag.Value
			}
		}
		return !!lastPage
	})
	return team, environmentKey, err
}

func newAccountContext(accountId string, accountName string, region string, iamRole string, spaceId string, team string, environmentType string) *model2.AccountContext {
	var roleArn = "arn:aws:iam::" + accountId + ":role/" + iamRole
	var credentials = stscreds.NewCredentials(session.Must(session.NewSession()), roleArn)
	var regionName = region
	var sess = session.Must(session.NewSession(&aws.Config{Credentials: credentials, Region: &regionName}))

	accountContext := &model2.AccountContext{
		Id:              accountId,
		Name:            accountName,
		Region:          regionName,
		SpaceId:         spaceId,
		Team:            team,
		EnvironmentType: environmentType,
		Session:         sess,
		AccountNames:    make(map[string]string),
		AccountContexts: make(map[string]*model2.AccountContext),
	}

	return accountContext
}
