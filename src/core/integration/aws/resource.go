package aws

import (
	resource2 "bitbucket.org/fraktalcyber/cspm-core/src/core/integration/aws/resource"
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	"log"
)

func GetResources(accountContext *model2.AccountContext, resourceChannel chan *model2.Resource) error {

	err := resource2.GetAwsAccountAndRegion(accountContext, resourceChannel)
	if err != nil {
		log.Println("Error getting aws account and region resources: "+accountContext.Name+"/"+accountContext.Region, err)
		return err
	}

	err = resource2.GetRoute53Resources(accountContext, resourceChannel)
	if err != nil {
		log.Println("Error getting Route53 resources: "+accountContext.Name+"/"+accountContext.Region, err)
		return err
	}

	err = resource2.GetAwsIps(accountContext, resourceChannel)
	if err != nil {
		log.Println("Error getting AWS IPs from network interfaces: "+accountContext.Name+"/"+accountContext.Region, err)
		return err
	}

	err = resource2.GetAwsConfigResources(accountContext, resourceChannel)
	if err != nil {
		log.Println("Error getting AWS Config resources: "+accountContext.Name+"/"+accountContext.Region, err)
		return err
	}

	return nil
}
