package loki

import (
	"bitbucket.org/fraktalcyber/cspm-core/src/core/integration/aws/util"
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	util2 "bitbucket.org/fraktalcyber/cspm-core/src/core/util"
	"crypto/sha1"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"regexp"
	"strconv"
	"strings"
	"time"
)

func ProcessFalcoLogLines(accountContext *model2.AccountContext, streamContext *StreamContext, lines []*Line, securityFindingChannel chan *model2.SecurityFinding) error {
	for _, l := range lines {
		timestamp := l.Timestamp
		line := l.Line

		if strings.HasPrefix(line, "{") {
			parseJsonFalcoLine(accountContext, streamContext, timestamp, line, securityFindingChannel)
		} else {
			parseTextFalcoLine(accountContext, streamContext, timestamp, line, securityFindingChannel)
		}

	}

	return nil
}

func parseJsonFalcoLine(accountContext *model2.AccountContext, streamContext *StreamContext, timestamp time.Time, line string, securityFindingChannel chan *model2.SecurityFinding) error {

	lineBytes := []byte(line)
	var lineMap map[string]interface{}

	err := json.Unmarshal(lineBytes, &lineMap)
	if err != nil {
		log.Printf("Error deserializing Falco JSON message: %v %v", err.Error(), line)
		return err
	}

	findingType := util2.GetStringWithJsonPointer(lineMap, "/rule")
	if findingType == nil {
		log.Printf("Error no rule in Falco JSON message: %v", line)
		return errors.New("no rule defined for Falco message")
	}

	output := util2.GetStringWithJsonPointer(lineMap, "/output")
	if output == nil {
		log.Printf("Error no output in Falco JSON message: %v", output)
		return errors.New("no output defined for Falco message")
	}

	priority := getNumericPriority(util2.GetStringWithJsonPointer(lineMap, "/priority"))
	containerId := util2.GetStringWithJsonPointer(lineMap, "/output_fields/container.id")
	file := util2.GetStringWithJsonPointer(lineMap, "/output_fields/fd.name")
	command := util2.GetStringWithJsonPointer(lineMap, "/output_fields/proc.cmdline")

	unixNanoTimeFloat64 := util2.GetFloat64WithJsonPointer(lineMap, "/output_fields/evt.time")
	var observed = time.Now()
	if unixNanoTimeFloat64 != nil {
		unixNanoTime := int64(*unixNanoTimeFloat64)
		observed = time.Unix(0, unixNanoTime)
		if err != nil {
			log.Printf("Error deserializing Falco JSON message time: %v %v", err.Error(), unixNanoTimeFloat64)
			return err
		}
	}

	var resourceType string
	var title string
	resourceId := "arn:aws:eks:" + streamContext.Region + ":" + streamContext.AccountId + ":cluster/" + streamContext.KubernetesCluster + "|node:" + streamContext.HostName
	if containerId != nil && *containerId != "host" {
		resourceType = "k8s:container"
		resourceId = resourceId + "|container:" + *containerId
		title = *findingType + " in " + streamContext.KubernetesCluster + " / " + streamContext.HostName + " / " + *containerId
	} else {
		resourceType = "k8s:node"
		title = *findingType + " in " + streamContext.KubernetesCluster + " / " + streamContext.HostName
	}
	if file != nil {
		resourceId = resourceId + "|file:" + *file
	}

	var lineHash string
	if command != nil {
		lineHash = fmt.Sprintf("%x", sha1.Sum([]byte(*findingType+"+"+resourceId+"+"+*command)))
	} else {
		lineHash = fmt.Sprintf("%x", sha1.Sum([]byte(*findingType+"+"+resourceId)))
	}
	key := "falco-" + lineHash

	var accountName = util.GetAwsAccountName(accountContext, streamContext.AccountId)
	var team = util.GetAwsAccountTeam(accountContext, streamContext.AccountId)
	var environmentType = util.GetAwsAccountEnvironmentType(accountContext, streamContext.AccountId)

	description := "Falco detected: " + title + "."

	var labels = map[string]interface{}{}

	bytes, _ := json.Marshal(streamContext)
	var metadata map[string]interface{}
	err = json.Unmarshal(bytes, &metadata)
	if err != nil {
		log.Println("Error serializing metadata", err)
		return err
	}
	metadata[timestamp.Format(time.RFC3339)] = lineMap
	if file != nil {
		metadata["file"] = file
	}
	if command != nil {
		metadata["command"] = command
	}
	if containerId != nil && *containerId == "host" {
		metadata["workloadType"] = "host"
	} else {
		metadata["workloadType"] = "container"
	}

	securityFindingChannel <- &model2.SecurityFinding{
		Id:               "",
		SpaceId:          accountContext.SpaceId,
		AccountId:        streamContext.AccountId,
		AccountName:      accountName,
		Region:           streamContext.Region,
		Team:             team,
		EnvironmentType:  environmentType,
		ResourceType:     &resourceType,
		ResourceId:       &resourceId,
		Category:         model2.CategoryAnomaly,
		Type:             *findingType,
		TypeLabel:        *findingType,
		Key:              key,
		SourceId:         streamContext.LogGroup + "-" + streamContext.ContainerName,
		SourceType:       "Falco",
		Title:            title,
		Description:      description,
		Metadata:         &metadata,
		Labels:           &labels,
		JiraIssueKey:     nil,
		Priority:         priority,
		OriginalPriority: priority,
		SourcePriority:   priority,
		RulePriority:     nil,
		ManualPriority:   nil,
		State:            model2.StateActive,
		AlertState:       model2.AlertStateInactive,
		Created:          time.Now(),
		Modified:         time.Now(),
		FirstObserved:    observed,
		LastObserved:     observed,
	}

	return nil
}

func parseTextFalcoLine(accountContext *model2.AccountContext, streamContext *StreamContext, timestamp time.Time, line string, securityFindingChannel chan *model2.SecurityFinding) {
	lineRegex := "^(?P<title>[^\\(]*)(\\((?P<description>.*)\\))*(?P<parameters>(\\s[a-zA-Z0-9\\.]*=[^\\s]*)*)*"
	lineParts := parseLogLine(lineRegex, line)

	message := model2.GetOptionalStringValue(lineParts, "title", nil)
	description := model2.GetOptionalStringValue(lineParts, "description", nil)
	parameters := model2.GetOptionalStringValue(lineParts, "parameters", nil)

	priority, findingType := parseMessage(*message)

	resourceType := "container"
	resourceId := "account:" + streamContext.AccountId + "|region:" + streamContext.Region + "|kubernetes-cluster:" + streamContext.KubernetesCluster + "|host:" + streamContext.HostName

	if parameters != nil {
		parameterMap := parseParameters(*parameters)
		logLineNamespace := model2.GetOptionalStringValue(parameterMap, "k8s.ns", nil)
		if logLineNamespace != nil {
			resourceId += "|namespace:" + *logLineNamespace
		}
		if description != nil {
			descriptionMap := parseParameters(*description)
			logLineImage := model2.GetOptionalStringValue(descriptionMap, "image", nil)
			if logLineImage != nil {
				resourceId += "|container:" + *logLineImage
			}
			logLineParent := model2.GetOptionalStringValue(descriptionMap, "parent", nil)
			if logLineParent != nil {
				resourceId += "|parent:" + *logLineParent
			}
			logLineFile := model2.GetOptionalStringValue(descriptionMap, "file", nil)
			if logLineFile != nil {
				resourceId += "|file:" + *logLineFile
			}
			logLineDirectory := model2.GetOptionalStringValue(descriptionMap, "directory", nil)
			if logLineDirectory != nil {
				resourceId += "|directory:" + *logLineDirectory
			}
			logLineCommand := model2.GetOptionalStringValue(descriptionMap, "command", nil)
			if logLineCommand != nil {
				resourceId += "|command:" + *logLineCommand
			}
			logLineUser := model2.GetOptionalStringValue(descriptionMap, "user", nil)
			if logLineUser != nil {
				resourceId += "|user:" + *logLineUser
			}
		}
	} else {
		resourceId += "|namespace:" + streamContext.KubernetesNamespace + "|container:" + streamContext.ContainerName
	}

	title := findingType + ":" + resourceId
	normalizedLine := strconv.Itoa(priority) + "/" + findingType + "/" + resourceType + "/" + resourceId

	lineHash := fmt.Sprintf("%x", sha1.Sum([]byte(normalizedLine)))
	key := "loki-" + lineHash

	var accountName = util.GetAwsAccountName(accountContext, streamContext.AccountId)
	var team = util.GetAwsAccountTeam(accountContext, streamContext.AccountId)
	var environmentType = util.GetAwsAccountEnvironmentType(accountContext, streamContext.AccountId)

	if description == nil || *description == "" {
		empty := "-"
		description = &empty
	}

	var labels = map[string]interface{}{}

	bytes, _ := json.Marshal(streamContext)
	var metadata map[string]interface{}
	err := json.Unmarshal(bytes, &metadata)
	if err != nil {
		log.Println("Error serializing metadata", err)
		return
	}
	metadata[timestamp.Format(time.RFC3339)] = line

	securityFindingChannel <- &model2.SecurityFinding{
		Id:               "",
		SpaceId:          accountContext.SpaceId,
		AccountId:        streamContext.AccountId,
		AccountName:      accountName,
		Region:           streamContext.Region,
		Team:             team,
		EnvironmentType:  environmentType,
		ResourceType:     &resourceType,
		ResourceId:       &resourceId,
		Category:         model2.CategoryAnomaly,
		Type:             findingType,
		TypeLabel:        findingType,
		Key:              key,
		SourceId:         streamContext.LogGroup + "-" + streamContext.ContainerName,
		SourceType:       "Falco",
		Title:            title,
		Description:      *description,
		Metadata:         &metadata,
		Labels:           &labels,
		JiraIssueKey:     nil,
		Priority:         priority,
		OriginalPriority: priority,
		SourcePriority:   priority,
		RulePriority:     nil,
		ManualPriority:   nil,
		State:            model2.StateActive,
		AlertState:       model2.AlertStateInactive,
		Created:          time.Now(),
		Modified:         time.Now(),
		FirstObserved:    timestamp,
		LastObserved:     timestamp,
	}
}

func parseMessage(message string) (int, string) {
	lowerMessage := strings.ToLower(message)

	priorityLabel := "emergency"
	if index := strings.Index(lowerMessage, priorityLabel); index != -1 {
		return 5, getTitle(message, index, priorityLabel)
	}

	priorityLabel = "alert"
	if index := strings.Index(lowerMessage, priorityLabel); index != -1 {
		return 5, getTitle(message, index, priorityLabel)
	}

	priorityLabel = "critical"
	if index := strings.Index(lowerMessage, priorityLabel); index != -1 {
		return 5, getTitle(message, index, priorityLabel)
	}

	priorityLabel = "error"
	if index := strings.Index(lowerMessage, priorityLabel); index != -1 {
		return 4, getTitle(message, index, priorityLabel)
	}

	priorityLabel = "warning"
	if index := strings.Index(lowerMessage, priorityLabel); index != -1 {
		return 3, getTitle(message, index, priorityLabel)
	}

	priorityLabel = "notice"
	if index := strings.Index(lowerMessage, priorityLabel); index != -1 {
		return 2, getTitle(message, index, priorityLabel)
	}

	priorityLabel = "informational"
	if index := strings.Index(lowerMessage, priorityLabel); index != -1 {
		return 1, getTitle(message, index, priorityLabel)
	}

	return 9, getTitle(lowerMessage, 0, priorityLabel)
}

func getNumericPriority(priority *string) int {
	if priority == nil {
		return 9
	}

	priorityLowerCase := strings.ToLower(*priority)
	switch priorityLowerCase {
	case "emergency":
	case "alert":
	case "critical":
	case "error":
		return 5
	case "warning":
		return 4
	case "notice":
		return 3
	case "informational":
		return 1
	default:
		return 9
	}

	return 9

}

func getTitle(message string, index int, priorityLabel string) string {
	title := message[index+len(priorityLabel):]

	startIndex := strings.LastIndex(title, ":")
	if startIndex != -1 {
		title = title[startIndex+1:]
	}

	findingType := title
	if index := strings.Index(title, "."); index != -1 {
		findingType = strings.TrimSpace(title[0:index])
	}
	findingType = strings.ToLower(findingType)

	return strings.TrimSpace(findingType)
}

func parseLogLine(regEx string, line string) *map[string]interface{} {
	var compRegEx = regexp.MustCompile(regEx)
	match := compRegEx.FindStringSubmatch(line)
	var valueMap map[string]interface{} = map[string]interface{}{}
	for i, name := range compRegEx.SubexpNames() {
		if name == "" {
			continue
		}
		valueMap[name] = match[i]
	}
	return &valueMap
}

func parseParameters(parametersString string) *map[string]interface{} {
	var valueMap map[string]interface{} = map[string]interface{}{}
	parameters := strings.Split(parametersString, " ")
	for _, parameter := range parameters {
		parameterParts := strings.Split(parameter, "=")
		if len(parameterParts) < 2 {
			continue
		}
		valueMap[strings.TrimSpace(parameterParts[0])] = strings.TrimSpace(parameterParts[1])
	}
	return &valueMap
}
