package loki

import (
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	"time"
)

type StreamContext struct {
	AccountId           string
	Region              string
	LogGroup            string
	KubernetesCluster   string
	HostName            string
	KubernetesNamespace string
	ContainerName       string
	IoStream            string
}

type Line struct {
	Timestamp time.Time
	Line      string
}

type LineProcessor func(accountContext *model2.AccountContext, streamContext *StreamContext, lines []*Line, securityFindingChannel chan *model2.SecurityFinding) error
