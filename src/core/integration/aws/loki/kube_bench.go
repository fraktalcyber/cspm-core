package loki

import (
	"bitbucket.org/fraktalcyber/cspm-core/src/core/integration/aws/util"
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	"encoding/json"
	"log"
	"strings"
	"time"
)

func ProcessKubeBenchLogLines(accountContext *model2.AccountContext, streamContext *StreamContext, lines []*Line, securityFindingChannel chan *model2.SecurityFinding) error {
	var lineStartIndexes []int
	var timestampsByLineStartIndexMap = map[int]time.Time{}

	var allLines = ""
	for _, l := range lines {
		lineStartIndex := len(allLines)
		lineStartIndexes = append(lineStartIndexes, lineStartIndex)
		timestampsByLineStartIndexMap[lineStartIndex] = l.Timestamp
		allLines = allLines + l.Line
		//log.Println(l.Line)
	}

	log.Printf("Kube Bench Processing - parsing %+v lines. Start Indexes: %+v\n", len(lines), lineStartIndexes)

	var startIndex = 0
	for {
		log.Printf("  decoding at index: %+v\n", startIndex)
		linesToParse := allLines[startIndex:]
		charactersDecoded := decodeLines(accountContext, streamContext, securityFindingChannel, timestampsByLineStartIndexMap, linesToParse)

		lineNumberAfterDecoder := indexOf(&lineStartIndexes, startIndex+charactersDecoded)
		if lineNumberAfterDecoder == -1 {
			// End
			log.Println("  Decoder read to end of line batch or read line partially.")
			lineNumberAfterDecoder = len(lineStartIndexes)
		}

		nextLineNumber := lineNumberAfterDecoder
		if charactersDecoded == 0 {
			nextLineNumber = lineNumberAfterDecoder + 1
		}
		if nextLineNumber > len(lineStartIndexes)-1 {
			// End
			log.Println("  All line decoded. Exiting decode loop.")
			break
		}
		startIndex = lineStartIndexes[nextLineNumber]
	}

	return nil
}

func indexOf(array *[]int, element int) int {
	for k, v := range *array {
		if element == v {
			return k
		}
	}
	return -1 //not found.
}

func decodeLines(accountContext *model2.AccountContext, streamContext *StreamContext, securityFindingChannel chan *model2.SecurityFinding, timestampsByLineStartIndexMap map[int]time.Time, lineBatch string) int {
	dec := json.NewDecoder(strings.NewReader(lineBatch))
	if !dec.More() {
		log.Println("    Kube Bench unmarshal warning. Line does not start JSON.")
		return 0
	}

	startIndex := int(dec.InputOffset())
	timestamp := timestampsByLineStartIndexMap[startIndex]
	var reports []interface{}
	err := dec.Decode(&reports)
	if err != nil {
		log.Println("    Kube Bench unmarshalling warning. Line JSON decode failed.", err)
		return startIndex
	}

	log.Printf("    characters decoded: %+v\n", dec.InputOffset())

	processReports(accountContext, streamContext, reports, securityFindingChannel, timestamp)

	log.Printf("    report processed.")

	return int(dec.InputOffset())
}

func processReports(accountContext *model2.AccountContext, streamContext *StreamContext, reports []interface{}, securityFindingChannel chan *model2.SecurityFinding, timestamp time.Time) {
	for _, r := range reports {
		var report = r.(map[string]interface{})
		//var reportId = report["id"].(string)
		//var reportTitle = report["text"].(string)
		//log.Printf("%+v %+v", reportId, reportTitle)

		if ss, ok := report["tests"]; ok {
			var sections = ss.([]interface{})
			for _, s := range sections {
				var section = s.(map[string]interface{})
				//var sectionId = section["section"].(string)
				//var sectionTitle = section["desc"].(string)
				//log.Printf("  %+v %+v", sectionId, sectionTitle)

				if ts, ok := section["results"]; ok {
					var tests = ts.([]interface{})
					for _, t := range tests {
						var test = t.(map[string]interface{})
						var testId = test["test_number"].(string)
						var testTitle = test["test_desc"].(string)
						var testDescription = test["remediation"].(string)
						testTitle = strings.Replace(testTitle, " (Automated)", "", 1)
						testTitle = strings.Replace(testTitle, " (Manual)", "", 1)
						var testStatus = test["status"].(string)
						var testType = test["type"].(string)
						if testType == "manual" {
							continue
						}
						var testPassed = testStatus == "PASS"
						var scored = test["scored"].(bool)

						//log.Printf("    %+v %+v - %+v type:'%+v' status:'%+v'", testId, testTitle, testPassed, testType, testStatus)

						var accountName = util.GetAwsAccountName(accountContext, streamContext.AccountId)
						var team = util.GetAwsAccountTeam(accountContext, streamContext.AccountId)
						var environmentType = util.GetAwsAccountEnvironmentType(accountContext, streamContext.AccountId)

						var resourceId = streamContext.KubernetesCluster + "/" + streamContext.HostName
						var resourceType = "host"
						var category = model2.CategoryCompliance
						var findingType = "CIS Kubernetes Benchmark/" + testId
						var findingKey = "kube-bench/" + streamContext.KubernetesCluster + "/" + streamContext.HostName + "/" + findingType
						var sourceId = "kube-bench/" + streamContext.KubernetesCluster + "/" + streamContext.HostName
						var sourceType = "Kube Bench"

						var priority = 3
						if scored {
							priority = 4
						}
						if testPassed {
							priority = 1
						}

						var labels = map[string]interface{}{}
						labels["aws_account_id"] = &streamContext.AccountId
						labels["aws_region"] = &streamContext.Region
						labels["resource_type"] = &resourceType
						labels["resource_id"] = &resourceId

						bytes, _ := json.Marshal(streamContext)
						var metadata map[string]interface{}
						err := json.Unmarshal(bytes, &metadata)
						if err != nil {
							log.Println("Error serializing metadata", err)
							continue
						}
						metadata["test"] = test

						securityFindingChannel <- &model2.SecurityFinding{
							Id:               "",
							SpaceId:          accountContext.SpaceId,
							AccountId:        streamContext.AccountId,
							AccountName:      accountName,
							Region:           streamContext.Region,
							Team:             team,
							EnvironmentType:  environmentType,
							ResourceType:     &resourceType,
							ResourceId:       &resourceId,
							Category:         category,
							Type:             findingType,
							TypeLabel:        testTitle,
							Key:              findingKey,
							SourceId:         sourceId,
							SourceType:       sourceType,
							Title:            testTitle,
							Description:      testDescription,
							Metadata:         &test,
							Labels:           &labels,
							JiraIssueKey:     nil,
							Priority:         priority,
							OriginalPriority: priority,
							SourcePriority:   priority,
							RulePriority:     nil,
							ManualPriority:   nil,
							State:            model2.StateActive,
							AlertState:       model2.AlertStateInactive,
							Created:          time.Now(),
							Modified:         time.Now(),
							FirstObserved:    timestamp,
							LastObserved:     timestamp,
						}
					}
				}
			}
		}
	}
}
