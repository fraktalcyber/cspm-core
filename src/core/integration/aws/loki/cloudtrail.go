package loki

import (
	"bitbucket.org/fraktalcyber/cspm-core/src/core/integration/aws/util"
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	util3 "bitbucket.org/fraktalcyber/cspm-core/src/core/util"
	"encoding/json"
	"log"
	"time"
)

func ProcessCloudTrailLogLines(accountContext *model2.AccountContext, streamContext *StreamContext, findingCategory model2.SecurityFindingCategory, findingType string, findingDescription string, priority int, lines []*Line, securityFindingChannel chan *model2.SecurityFinding) error {
	for _, l := range lines {
		timestamp := l.Timestamp
		line := l.Line

		accountName := util.GetAwsAccountName(accountContext, streamContext.AccountId)
		team := util.GetAwsAccountTeam(accountContext, streamContext.AccountId)
		environmentType := util.GetAwsAccountEnvironmentType(accountContext, streamContext.AccountId)

		var jsonLine map[string]interface{}
		err := json.Unmarshal([]byte(line), &jsonLine)
		if err != nil {
			log.Println("Error serializing metadata", err)
			continue
		}

		var resourceType *string
		resourceId := util3.GetStringWithJsonPointer(jsonLine, "/userIdentity/sessionContext/sessionIssuer/arn")
		if resourceId == nil {
			resourceId = util3.GetStringWithJsonPointer(jsonLine, "/userIdentity/arn")
		}
		if resourceId == nil {
			resourceId = util3.GetStringWithJsonPointer(jsonLine, "/userIdentity/invokedBy")
		}
		var findingKey string
		var title string
		if resourceId != nil {
			findingKey = findingType + "/" + streamContext.AccountId + "/" + streamContext.Region + "/" + *resourceId
			title = findingType + " " + *resourceId
			resourceTypeString := "AWS::IAM::User"
			resourceType = &resourceTypeString
		} else {
			findingKey = findingType + "/" + streamContext.AccountId + "/" + streamContext.Region
			title = findingType
		}

		description := findingDescription

		var labels = map[string]interface{}{}
		labels["aws_account_id"] = &streamContext.AccountId
		labels["aws_region"] = &streamContext.Region
		labels["resource_type"] = &resourceType
		labels["resource_id"] = &resourceId

		bytes, _ := json.Marshal(streamContext)
		var metadata map[string]interface{}
		err = json.Unmarshal(bytes, &metadata)
		if err != nil {
			log.Println("Error serializing metadata", err)
			continue
		}
		metadata[timestamp.Format(time.RFC3339)] = line

		securityFindingChannel <- &model2.SecurityFinding{
			Id:               "",
			SpaceId:          accountContext.SpaceId,
			AccountId:        streamContext.AccountId,
			AccountName:      accountName,
			Region:           streamContext.Region,
			Team:             team,
			EnvironmentType:  environmentType,
			ResourceType:     resourceType,
			ResourceId:       resourceId,
			Category:         findingCategory,
			Type:             findingType,
			TypeLabel:        findingType,
			Key:              findingKey,
			SourceId:         streamContext.LogGroup + "-" + streamContext.ContainerName,
			SourceType:       "CloudTrail",
			Title:            title,
			Description:      description,
			Metadata:         &metadata,
			Labels:           &labels,
			JiraIssueKey:     nil,
			Priority:         priority,
			OriginalPriority: priority,
			SourcePriority:   priority,
			RulePriority:     nil,
			ManualPriority:   nil,
			State:            model2.StateActive,
			AlertState:       model2.AlertStateInactive,
			Created:          time.Now(),
			Modified:         time.Now(),
			FirstObserved:    timestamp,
			LastObserved:     timestamp,
		}
	}

	return nil
}
