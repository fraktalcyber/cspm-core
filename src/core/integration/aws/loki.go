package aws

import (
	loki2 "bitbucket.org/fraktalcyber/cspm-core/src/core/integration/aws/loki"
	"bitbucket.org/fraktalcyber/cspm-core/src/core/integration/aws/util"
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	util3 "bitbucket.org/fraktalcyber/cspm-core/src/core/util"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/url"
	"sort"
	"strconv"
	"strings"
	"time"
)

func GetLokiFindings(logAccountContext *model2.AccountContext, accountContext *model2.AccountContext, securityFindingChannel chan *model2.SecurityFinding) error {

	err := QueryAndProcessLokiLogLines(logAccountContext, accountContext, "{log_group=~\".*host.*\",log_stream=~\".*falco.*\",aws_account_id=\""+accountContext.Id+"\",aws_region=\""+accountContext.Region+"\"}", securityFindingChannel, loki2.ProcessFalcoLogLines, false)
	if err == nil {
		err = QueryAndProcessLokiLogLines(logAccountContext, accountContext, "{log_group=~\".*kube-bench.*\",aws_account_id=\""+accountContext.Id+"\",aws_region=\""+accountContext.Region+"\"} |= \"[{\"", securityFindingChannel, loki2.ProcessKubeBenchLogLines, false)
	}
	if err == nil {
		err = GetCloudTrailLogLines(logAccountContext, accountContext, securityFindingChannel)
	}

	recordLokiAPIAvailabilityFinding(logAccountContext, securityFindingChannel, err)

	return err
}

func GetCloudTrailLogLines(logAccountContext *model2.AccountContext, accountContext *model2.AccountContext, securityFindingChannel chan *model2.SecurityFinding) error {
	var queryPrefix = "{log_group=~\"(.*CloudTrail.*)|(.*cloudtrail.*)\",aws_account_id=\"" + accountContext.Id + "\",aws_region=\"" + accountContext.Region + "\"} "
	var err error

	err = QueryAndProcessLokiLogLines(logAccountContext, accountContext, queryPrefix+" |= \"ConsoleLogin\" | json | eventName = \"ConsoleLogin\" and (additionalEventData_MFAUsed != \"Yes\" and userIdentity_sessionContext_attributes_mfaAuthenticated != \"true\") and responseElements_ConsoleLogin != \"Failure\"",
		securityFindingChannel, func(accountContext *model2.AccountContext, streamContext *loki2.StreamContext, lines []*loki2.Line, securityFindingChannel chan *model2.SecurityFinding) error {
			return loki2.ProcessCloudTrailLogLines(accountContext, streamContext, model2.CategoryAnomaly, model2.SecurityFindingTypeConsoleSigninNoMfa, "CIS AWS Foundations Benchmark / Console signin without MFA detected from CloudTrail.", 4, lines, securityFindingChannel)
		}, true)
	if err != nil {
		return err
	}

	err = QueryAndProcessLokiLogLines(logAccountContext, accountContext, queryPrefix+" |= \"Root\" != \"invokedBy\" | json | userIdentity_type = \"Root\" and eventType != \"AwsServiceEvent\"",
		securityFindingChannel, func(accountContext *model2.AccountContext, streamContext *loki2.StreamContext, lines []*loki2.Line, securityFindingChannel chan *model2.SecurityFinding) error {
			return loki2.ProcessCloudTrailLogLines(accountContext, streamContext, model2.CategoryAnomaly, model2.SecurityFindingTypeRootLogin, "CIS AWS Foundations Benchmark / Root login detected from CloudTrail.", 4, lines, securityFindingChannel)
		}, true)
	if err != nil {
		return err
	}

	err = QueryAndProcessLokiLogLines(logAccountContext, accountContext, queryPrefix+" |= \"SecurityGroup\" | json | eventName = \"AuthorizeSecurityGroupIngress\" or eventName = \"AuthorizeSecurityGroupEgress\" or eventName = \"RevokeSecurityGroupIngress\" or eventName = \"RevokeSecurityGroupEgress\" or eventName = \"CreateSecurityGroup\" or eventName = \"DeleteSecurityGroup\"",
		securityFindingChannel, func(accountContext *model2.AccountContext, streamContext *loki2.StreamContext, lines []*loki2.Line, securityFindingChannel chan *model2.SecurityFinding) error {
			return loki2.ProcessCloudTrailLogLines(accountContext, streamContext, model2.CategoryNotification, model2.SecurityFindingSecurityGroupChanges, "CIS AWS Foundations Benchmark / Security Group changes detected from CloudTrail.", 4, lines, securityFindingChannel)
		}, true)
	if err != nil {
		return err
	}

	err = QueryAndProcessLokiLogLines(logAccountContext, accountContext, queryPrefix+" |= \"iam.amazonaws.com\" | json | eventName = \"DeleteGroupPolicy\" or eventName = \"DeleteRolePolicy\" or eventName = \"DeleteUserPolicy\" or eventName = \"PutGroupPolicy\" or eventName = \"PutRolePolicy\" or eventName = \"PutUserPolicy\" or eventName = \"CreatePolicy\" or eventName = \"DeletePolicy\" or eventName = \"CreatePolicyVersion\" or eventName = \"DeletePolicyVersion\" or eventName = \"AttachRolePolicy\" or eventName = \"DetachRolePolicy\" or eventName = \"AttachUserPolicy\" or eventName = \"DetachUserPolicy\" or eventName = \"AttachGroupPolicy\" or eventName = \"DetachGroupPolicy\"",
		securityFindingChannel, func(accountContext *model2.AccountContext, streamContext *loki2.StreamContext, lines []*loki2.Line, securityFindingChannel chan *model2.SecurityFinding) error {
			return loki2.ProcessCloudTrailLogLines(accountContext, streamContext, model2.CategoryNotification, model2.SecurityFindingIAMChanges, "CIS AWS Foundations Benchmark / IAM Changes changes detected from CloudTrail.", 4, lines, securityFindingChannel)
		}, true)
	if err != nil {
		return err
	}

	err = QueryAndProcessLokiLogLines(logAccountContext, accountContext, queryPrefix+"| json | errorCode = \"UnauthorizedOperation\" or errorCode = \"AccessDenied\"",
		securityFindingChannel, func(accountContext *model2.AccountContext, streamContext *loki2.StreamContext, lines []*loki2.Line, securityFindingChannel chan *model2.SecurityFinding) error {
			return loki2.ProcessCloudTrailLogLines(accountContext, streamContext, model2.CategoryAnomaly, model2.SecurityFindingTypeUnauthorizedAPICalls, "CIS AWS Foundations Benchmark / Unauthorized API call detected from CloudTrail.", 3, lines, securityFindingChannel)
		}, true)
	if err != nil {
		return err
	}

	err = QueryAndProcessLokiLogLines(logAccountContext, accountContext, queryPrefix+" | json | eventName = \"CreateTrail\" or eventName = \"UpdateTrail\" or eventName = \"DeleteTrail\" or eventName = \"StartLogging\" or eventName = \"StopLogging\"",
		securityFindingChannel, func(accountContext *model2.AccountContext, streamContext *loki2.StreamContext, lines []*loki2.Line, securityFindingChannel chan *model2.SecurityFinding) error {
			return loki2.ProcessCloudTrailLogLines(accountContext, streamContext, model2.CategoryNotification, model2.SecurityFindingTypeCloudTrailConfigurationChanges, "CIS AWS Foundations Benchmark / Cloud Trail configuration changes detected from CloudTrail.", 4, lines, securityFindingChannel)
		}, true)
	if err != nil {
		return err
	}

	err = QueryAndProcessLokiLogLines(logAccountContext, accountContext, queryPrefix+" |= \"Failed authentication\" | json | eventName = \"ConsoleLogin\" and errorMessage =  \"Failed authentication\"",
		securityFindingChannel, func(accountContext *model2.AccountContext, streamContext *loki2.StreamContext, lines []*loki2.Line, securityFindingChannel chan *model2.SecurityFinding) error {
			return loki2.ProcessCloudTrailLogLines(accountContext, streamContext, model2.CategoryNotification, model2.SecurityFindingFailedAuthentication, "CIS AWS Foundations Benchmark / Failed authentication detected from CloudTrail.", 4, lines, securityFindingChannel)
		}, true)
	if err != nil {
		return err
	}

	err = QueryAndProcessLokiLogLines(logAccountContext, accountContext, queryPrefix+" |= \"ConsoleLogin\" | json | eventName = \"ConsoleLogin\" and responseElements_ConsoleLogin != \"Failure\"",
		securityFindingChannel, func(accountContext *model2.AccountContext, streamContext *loki2.StreamContext, lines []*loki2.Line, securityFindingChannel chan *model2.SecurityFinding) error {
			return loki2.ProcessCloudTrailLogLines(accountContext, streamContext, model2.CategoryNotification, model2.SecurityFindingTypeConsoleSignin, "CIS AWS Foundations Benchmark / Console signin detected from CloudTrail.", 4, lines, securityFindingChannel)
		}, true)
	if err != nil {
		return err
	}

	err = QueryAndProcessLokiLogLines(logAccountContext, accountContext, queryPrefix+" |= \"s3.amazonaws.com\" | json | eventName = \"PutBucketAcl\" or eventName = \"PutBucketPolicy\" or eventName = \"PutBucketCors\" or eventName = \"PutBucketLifecycle\" or eventName = \"PutBucketReplication\" or eventName = \"DeleteBucketPolicy\" or eventName = \"DeleteBucketCors\" or eventName = \"DeleteBucketLifecycle\" or eventName = \"DeleteBucketReplication\"",
		securityFindingChannel, func(accountContext *model2.AccountContext, streamContext *loki2.StreamContext, lines []*loki2.Line, securityFindingChannel chan *model2.SecurityFinding) error {
			return loki2.ProcessCloudTrailLogLines(accountContext, streamContext, model2.CategoryNotification, model2.SecurityFindingS3BucketPolicyChanges, "CIS AWS Foundations Benchmark / S3 bucket policy changes detected from CloudTrail.", 4, lines, securityFindingChannel)
		}, true)
	if err != nil {
		return err
	}

	err = QueryAndProcessLokiLogLines(logAccountContext, accountContext, queryPrefix+" |= \"kms.amazonaws.com\" | json | eventName = \"DisableKey\" or eventName = \"ScheduleKeyDeletion\"",
		securityFindingChannel, func(accountContext *model2.AccountContext, streamContext *loki2.StreamContext, lines []*loki2.Line, securityFindingChannel chan *model2.SecurityFinding) error {
			return loki2.ProcessCloudTrailLogLines(accountContext, streamContext, model2.CategoryNotification, model2.SecurityFindingDisableOrDeleteCMK, "CIS AWS Foundations Benchmark / Disable or delete CMK detected from CloudTrail.", 4, lines, securityFindingChannel)
		}, true)
	if err != nil {
		return err
	}

	err = QueryAndProcessLokiLogLines(logAccountContext, accountContext, queryPrefix+" |= \"config.amazonaws.com\" | json | eventName = \"StopConfigurationRecorder\" or eventName = \"DeleteDeliveryChannel\" or eventName = \"PutDeliveryChannel\" or eventName = \"PutConfigurationRecorder\"",
		securityFindingChannel, func(accountContext *model2.AccountContext, streamContext *loki2.StreamContext, lines []*loki2.Line, securityFindingChannel chan *model2.SecurityFinding) error {
			return loki2.ProcessCloudTrailLogLines(accountContext, streamContext, model2.CategoryNotification, model2.SecurityFindingAWSConfigChanges, "CIS AWS Foundations Benchmark / AWS Config changes detected from CloudTrail.", 4, lines, securityFindingChannel)
		}, true)
	if err != nil {
		return err
	}

	err = QueryAndProcessLokiLogLines(logAccountContext, accountContext, queryPrefix+" | json | eventName = \"CreateNetworkAcl\" or eventName = \"CreateNetworkAclEntry\" or eventName = \"DeleteNetworkAcl\" or eventName = \"DeleteNetworkAclEntry\" or eventName = \"ReplaceNetworkAclEntry\"  or eventName = \"ReplaceNetworkAclAssociation\"",
		securityFindingChannel, func(accountContext *model2.AccountContext, streamContext *loki2.StreamContext, lines []*loki2.Line, securityFindingChannel chan *model2.SecurityFinding) error {
			return loki2.ProcessCloudTrailLogLines(accountContext, streamContext, model2.CategoryNotification, model2.SecurityFindingNACLChanges, "CIS AWS Foundations Benchmark / NACL changes detected from CloudTrail.", 4, lines, securityFindingChannel)
		}, true)
	if err != nil {
		return err
	}

	err = QueryAndProcessLokiLogLines(logAccountContext, accountContext, queryPrefix+" | json | eventName = \"CreateCustomerGateway\" or eventName = \"DeleteCustomerGateway\" or eventName = \"AttachInternetGateway\" or eventName = \"CreateInternetGateway\" or eventName = \"DeleteInternetGateway\"  or eventName = \"DetachInternetGateway\"",
		securityFindingChannel, func(accountContext *model2.AccountContext, streamContext *loki2.StreamContext, lines []*loki2.Line, securityFindingChannel chan *model2.SecurityFinding) error {
			return loki2.ProcessCloudTrailLogLines(accountContext, streamContext, model2.CategoryNotification, model2.SecurityFindingNetworkGWChanges, "CIS AWS Foundations Benchmark / Network gateway changes detected from CloudTrail.", 4, lines, securityFindingChannel)
		}, true)
	if err != nil {
		return err
	}

	err = QueryAndProcessLokiLogLines(logAccountContext, accountContext, queryPrefix+" | json | eventName = \"CreateRoute\" or eventName = \"CreateRouteTable\" or eventName = \"ReplaceRoute\" or eventName = \"ReplaceRouteTableAssociation\" or eventName = \"DeleteRouteTable\" or eventName = \"DeleteRoute\" or eventName = \"DisassociateRouteTable\"",
		securityFindingChannel, func(accountContext *model2.AccountContext, streamContext *loki2.StreamContext, lines []*loki2.Line, securityFindingChannel chan *model2.SecurityFinding) error {
			return loki2.ProcessCloudTrailLogLines(accountContext, streamContext, model2.CategoryNotification, model2.SecurityFindingRouteTableChanges, "CIS AWS Foundations Benchmark / Route table changes detected from CloudTrail.", 4, lines, securityFindingChannel)
		}, true)
	if err != nil {
		return err
	}

	err = QueryAndProcessLokiLogLines(logAccountContext, accountContext, queryPrefix+" | json | eventName = \"CreateVpc\" or eventName = \"DeleteVpc\" or eventName = \"ModifyVpcAttribute\" or eventName = \"AcceptVpcPeeringConnection\" or eventName = \"CreateVpcPeeringConnection\" or eventName = \"DeleteVpcPeeringConnection\" or eventName = \"RejectVpcPeeringConnection\" or eventName = \"AttachClassicLinkVpc\" or eventName = \"DetachClassicLinkVpc\" or eventName = \"DisableVpcClassicLink\" or eventName = \"EnableVpcClassicLink\"",
		securityFindingChannel, func(accountContext *model2.AccountContext, streamContext *loki2.StreamContext, lines []*loki2.Line, securityFindingChannel chan *model2.SecurityFinding) error {
			return loki2.ProcessCloudTrailLogLines(accountContext, streamContext, model2.CategoryNotification, model2.SecurityFindingVPCChanges, "CIS AWS Foundations Benchmark / VPC changes detected from CloudTrail.", 4, lines, securityFindingChannel)
		}, true)
	if err != nil {
		return err
	}

	return nil
}

func QueryAndProcessLokiLogLines(logAccountContext *model2.AccountContext, accountContext *model2.AccountContext, query string, securityFindingChannel chan *model2.SecurityFinding, processLines loki2.LineProcessor, preferSecondaryLokiInstance bool) error {
	if preferSecondaryLokiInstance {
		lokiBaseUrl2 := util3.GetConfig("loki_base_url_2", "")
		if strings.TrimSpace(lokiBaseUrl2) != "" {
			err := queryAndProcessLokiLogLines(logAccountContext, accountContext, lokiBaseUrl2, query, securityFindingChannel, processLines)
			if err != nil {
				return err
			}
			return nil
		}
	}

	lokiBaseUrl := util3.GetConfig("loki_base_url", "")
	err := queryAndProcessLokiLogLines(logAccountContext, accountContext, lokiBaseUrl, query, securityFindingChannel, processLines)
	if err != nil {
		return err
	}

	return nil
}

func queryAndProcessLokiLogLines(logAccountContext *model2.AccountContext, accountContext *model2.AccountContext, lokiBaseUrl string, query string, securityFindingChannel chan *model2.SecurityFinding, processLines loki2.LineProcessor) error {
	data, err := queryLokiLogLines(logAccountContext, lokiBaseUrl, query)
	if err != nil {
		return err
	}

	results, err := model2.GetMandatoryArrayValue(data, "result")
	if err != nil {
		return err
	}

	err = processLokiResults(accountContext, results, processLines, securityFindingChannel)
	if err != nil {
		return err
	}
	return nil
}

func queryLokiLogLines(logAccountContext *model2.AccountContext, lokiBaseUrl string, query string) (*map[string]interface{}, error) {
	start := strconv.FormatInt(time.Now().Add(-20*time.Minute).UnixNano(), 10)
	url := lokiBaseUrl + "/loki/api/v1/query_range?query=" + url.QueryEscape(query) + "&start=" + start

	lokiOrganizationId := util3.GetConfig("loki_organization_id", "")
	basicAuthUserName := util3.GetConfig("loki_user", "")
	basicAuthPassword, err := util3.GetSecret("loki_password")
	if err != nil {
		return nil, err
	}

	log.Println("Loki query start: " + url)
	bodyBytes, err := util.HttpGet(logAccountContext, url, []util.Header{{
		Key:   "X-Scope-OrgID",
		Value: lokiOrganizationId,
	}}, basicAuthUserName, basicAuthPassword)
	if err != nil {
		return nil, err
	}

	var body map[string]interface{}
	err = json.Unmarshal(bodyBytes, &body)
	if err != nil {
		return nil, err
	}

	status, err := model2.GetMandatoryStringValue(&body, "status")

	if err != nil {
		return nil, err
	} else {
		log.Println("Loki query end. Status: " + status)
	}

	if status != "success" {
		errorMessage := fmt.Sprintf("Loki result status was not success: %v", status)
		log.Println(errorMessage)
		return nil, errors.New(errorMessage)
	}

	data, err := model2.GetMandatoryMapValue(&body, "data")
	if err != nil {
		return nil, err
	}

	resultType, err := model2.GetMandatoryStringValue(data, "resultType")
	if err != nil {
		return nil, err
	}
	if resultType != "streams" {
		errorMessage := fmt.Sprintf("Loki result type was not streams: %v", resultType)
		log.Println(errorMessage)
		return nil, errors.New(errorMessage)
	}
	return data, nil
}

func processLokiResults(accountContext *model2.AccountContext, results *[]interface{}, processLines loki2.LineProcessor, securityFindingChannel chan *model2.SecurityFinding) error {
	streamLineMap := map[string][]*loki2.Line{}
	streamContextMap := map[string]*loki2.StreamContext{}

	for _, result := range *results {
		var resultMap = result.(map[string]interface{})
		streamMap, err := model2.GetMandatoryMapValue(&resultMap, "stream")
		if err != nil {
			return err
		}

		streamKey := getStreamKey(streamMap)
		if _, ok := streamLineMap[streamKey]; ok {
		} else {
			streamContext, err := getStreamContext(streamMap)
			if err != nil {
				return err
			}
			if streamContext.IoStream == "stderr" {
				continue
			}

			streamLineMap[streamKey] = []*loki2.Line{}
			streamContextMap[streamKey] = &streamContext
		}

		if streamLines, ok := streamLineMap[streamKey]; ok {
			lines, err := model2.GetMandatoryArrayValue(&resultMap, "values")
			if err != nil {
				return err
			}

			for _, l := range *lines {

				var lineParts = l.([]interface{})
				line := (lineParts)[1].(string)
				timestamp, err := nanoSecondsStringToTime((lineParts)[0].(string))
				if err != nil {
					log.Println("Error processing loki log line timestamp.", err)
					continue
				}

				streamLines = append(streamLines, &loki2.Line{
					Timestamp: timestamp,
					Line:      line,
				})
				//log.Printf("%+v: %+v", streamKey, len(combinedStreamLines))
			}
			streamLineMap[streamKey] = streamLines
		}

	}

	for streamKey := range streamLineMap {
		streamLines := streamLineMap[streamKey]
		streamContext := streamContextMap[streamKey]
		util3.Reverse(streamLines)

		err := processLines(accountContext, streamContext, streamLines, securityFindingChannel)
		if err != nil {
			log.Printf("Error processing loki log lines %+v:\n%+v", streamLines, err)
		}
	}

	return nil
}

func getStreamKey(streamMap *map[string]interface{}) string {
	keys := make([]string, 0, len(*streamMap))
	for k := range *streamMap {
		if k == "log_stream" {
			continue
		}
		keys = append(keys, k)
	}
	sort.Strings(keys)

	streamKey := ""
	for _, k := range keys {
		streamKey += k + "=" + fmt.Sprintf("%+v", (*streamMap)[k]) + "&"
	}

	return streamKey
}

func getStreamContext(streamMap *map[string]interface{}) (loki2.StreamContext, error) {
	accountId, err := model2.GetMandatoryStringValue(streamMap, "aws_account_id")
	if err != nil {
		return loki2.StreamContext{}, err
	}

	region, err := model2.GetMandatoryStringValue(streamMap, "aws_region")
	if err != nil {
		return loki2.StreamContext{}, err
	}

	logGroup, err := model2.GetMandatoryStringValue(streamMap, "log_group")
	if err != nil {
		return loki2.StreamContext{}, err
	}

	kubernetesCluster, err := model2.GetMandatoryStringValue(streamMap, "kubernetes_cluster")
	if err != nil {
		err = nil
		// Ignore missing value.
		// return loki.StreamContext{}, err
	}

	hostName, err := model2.GetMandatoryStringValue(streamMap, "host")
	if err != nil {
		err = nil
		hostName, err = model2.GetMandatoryStringValue(streamMap, "host_id")
		if err != nil {
			err = nil
			hostName, err = model2.GetMandatoryStringValue(streamMap, "host_ip")
			if err != nil {
				err = nil
				// Ignore missing value.
				// return loki.StreamContext{}, err
			}
		}
	}

	kubernetesNamespace, err := model2.GetMandatoryStringValue(streamMap, "kubernetes_namespace")
	if err != nil {
		err = nil
		// Ignore missing value.
		// return loki.StreamContext{}, err
	}

	containerName, err := model2.GetMandatoryStringValue(streamMap, "container_name")
	if err != nil {
		err = nil
		// Ignore missing value.
		// return loki.StreamContext{}, err
	}

	ioStream, err := model2.GetMandatoryStringValue(streamMap, "io_stream")
	if err != nil {
		err = nil
		// Ignore missing value.
		// return loki.StreamContext{}, err
	}

	var lineContext = loki2.StreamContext{
		AccountId:           accountId,
		Region:              region,
		LogGroup:            logGroup,
		KubernetesCluster:   kubernetesCluster,
		HostName:            hostName,
		KubernetesNamespace: kubernetesNamespace,
		ContainerName:       containerName,
		IoStream:            ioStream,
	}
	return lineContext, nil
}

func nanoSecondsStringToTime(nanoSecondsString string) (time.Time, error) {
	nanoSeconds, err := strconv.ParseInt(nanoSecondsString, 10, 64)
	if err != nil {
		log.Printf("Error parsing loki timestamp %v.", err)
		return time.Time{}, err
	}

	return time.Unix(0, nanoSeconds*int64(time.Nanosecond)), nil
}

func recordLokiAPIAvailabilityFinding(accountContext *model2.AccountContext, securityFindingChannel chan *model2.SecurityFinding, err error) {
	findingType := "Fraktal/Compliance/CheckLokiAvailable"
	key := fmt.Sprintf("%v-%v/%v", findingType, accountContext.Name, accountContext.Region)
	priority := 0
	title := "FR1.4 Grafana Loki should be enabled."
	description := ""
	firstObservedAt := time.Now()
	lastObservedAt := time.Now()

	var warning bool
	if err == nil {
		description = "Loki API is available."
		priority = 1
		warning = false
	} else {
		log.Printf("Loki query error: %v\n", err)
		description = "Loki API is not available. Request caused error."
		priority = 5
		warning = true
	}

	util.AddCspmComplianceFinding(accountContext, priority, findingType, nil, nil, key, title, description, firstObservedAt, lastObservedAt, &securityFindingChannel, warning)
}
