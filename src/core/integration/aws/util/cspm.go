package util

import (
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	"time"
)

func AddCspmComplianceFinding(accountContext *model2.AccountContext, priority int, findingType string, resourceType *string, resourceId *string, key string, title string, description string, firstObservedAt time.Time, lastObservedAt time.Time, securityFindingChannel *chan *model2.SecurityFinding, warning bool) {
	var labels = map[string]interface{}{}
	labels["aws_account_id"] = &accountContext.Id
	labels["aws_region"] = &accountContext.Region
	labels["aws_resource_type"] = &resourceType
	labels["aws_resource_id"] = resourceId

	var metadata = map[string]interface{}{}

	var category = model2.CategoryCompliance
	if warning {
		category = model2.CategoryWarning
	}

	*securityFindingChannel <- &model2.SecurityFinding{
		Id:               "",
		SpaceId:          accountContext.SpaceId,
		AccountId:        accountContext.Id,
		AccountName:      accountContext.Name,
		Region:           accountContext.Region,
		Team:             accountContext.Team,
		EnvironmentType:  accountContext.EnvironmentType,
		ResourceType:     resourceType,
		ResourceId:       resourceId,
		Category:         category,
		Type:             findingType,
		TypeLabel:        findingType,
		Key:              key,
		SourceId:         "CSPM",
		SourceType:       "CSPM",
		Title:            title,
		Description:      description,
		Metadata:         &metadata,
		Labels:           &labels,
		JiraIssueKey:     nil,
		Priority:         priority,
		OriginalPriority: priority,
		SourcePriority:   priority,
		RulePriority:     nil,
		ManualPriority:   nil,
		State:            model2.StateActive,
		AlertState:       model2.AlertStateInactive,
		Created:          time.Now(),
		Modified:         time.Now(),
		FirstObserved:    firstObservedAt,
		LastObserved:     lastObservedAt,
	}
}
