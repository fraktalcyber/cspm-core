package util

import (
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	"context"
	"fmt"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/aws/signer/v4"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

type Header struct {
	Key   string
	Value string
}

func HttpGet(accountContext *model2.AccountContext, url string, headers []Header, basicAuthUser string, basicAuthPassword string) ([]byte, error) {
	cfg, err := GetAwsConfig(accountContext)
	if err != nil {
		return nil, err
	}

	ctx := context.Background()
	req, _ := http.NewRequest(http.MethodGet, url, nil)
	req = req.WithContext(ctx)

	for _, h := range headers {
		fmt.Printf("set header: (%v=%v)\n", h.Key, h.Value)
		req.Header.Set(h.Key, h.Value)
	}

	// Do either basic authentication or AWS authentication
	if strings.TrimSpace(basicAuthUser) != "" && strings.TrimSpace(basicAuthPassword) != "" {
		req.SetBasicAuth(basicAuthUser, basicAuthPassword)
	} else {
		err = SignRequest(cfg, ctx, req)
		if err != nil {
			return nil, err
		}
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Printf("failed to call remote service: (%v)\n", err)
		return nil, err
	}
	defer res.Body.Close()

	if res.StatusCode != 200 {
		fmt.Printf("service returned a status not 200: (%d)\n", res.StatusCode)
		return nil, fmt.Errorf("loki query returned a status not 200: (%d)\n", res.StatusCode)
	}

	bodyBytes, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Printf("failed to read body bytes: (%v)\n", err)
		return nil, err
	}
	return bodyBytes, nil
}

func SignRequest(cfg *aws.Config, ctx context.Context, req *http.Request) error {
	signer := v4.NewSigner(cfg.Credentials)
	err := signer.SignHTTP(ctx, req, "", "execute-api", cfg.Region, time.Now())
	if err != nil {
		fmt.Printf("failed to sign request: (%v)\n", err)
		return err
	}
	return nil
}
