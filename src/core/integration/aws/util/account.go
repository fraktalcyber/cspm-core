package util

import (
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	util2 "bitbucket.org/fraktalcyber/cspm-core/src/core/util"
	"fmt"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/aws/external"
	"github.com/aws/aws-sdk-go-v2/aws/stscreds"
	"github.com/aws/aws-sdk-go-v2/service/sts"
)

func GetAwsConfig(accountContext *model2.AccountContext) (*aws.Config, error) {
	var accountId = accountContext.Id
	var iamRole = util2.GetConfig("aws_iam_role", "")
	var roleArn = "arn:aws:iam::" + accountId + ":role/" + iamRole

	cfg, err := external.LoadDefaultAWSConfig()
	if err != nil {
		fmt.Println("unable to create an AWS session for the provided profile")
		return nil, err
	}

	stsSvc := sts.New(cfg)
	stsCredProvider := stscreds.NewAssumeRoleProvider(stsSvc, roleArn)
	cfg.Credentials = aws.NewChainProvider([]aws.CredentialsProvider{stsCredProvider})
	cfg.Region = accountContext.Region
	return &cfg, nil
}

func GetAwsAccountName(accountContext *model2.AccountContext, accountId string) string {
	var name = "?"
	if value, ok := accountContext.AccountNames[accountId]; ok {
		name = value
	} else {
		name = accountId
	}
	return name
}

func GetAwsAccountTeam(accountContext *model2.AccountContext, accountId string) string {
	var team = "?"
	if ac, ok := accountContext.AccountContexts[accountId]; ok {
		team = ac.Team
	}
	return team
}

func GetAwsAccountEnvironmentType(accountContext *model2.AccountContext, accountId string) string {
	var environmentType = "?"
	if ac, ok := accountContext.AccountContexts[accountId]; ok {
		environmentType = ac.EnvironmentType
	}
	return environmentType
}
