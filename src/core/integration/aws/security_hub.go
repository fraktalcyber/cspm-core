package aws

import (
	util2 "bitbucket.org/fraktalcyber/cspm-core/src/core/integration/aws/util"
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	"encoding/json"
	"log"
	"net/url"
	"strings"
	"time"
)
import "github.com/aws/aws-sdk-go/service/securityhub"

func GetSecurityHubFindings(accountContext *model2.AccountContext, securityFindingChannel *chan *model2.SecurityFinding) error {
	sh := securityhub.New(accountContext.Session)

	var nextToken *string
	for {
		var input = &securityhub.GetFindingsInput{
			NextToken: nextToken,
		}
		var output, err = sh.GetFindings(input)
		if err != nil {
			log.Println("Error getting findings from SecurityHub", err.Error())
			return err
		}

		for _, f := range output.Findings {
			var resourceType *string
			var resourceId *string
			var region *string

			if len(f.Resources) > 0 {
				resourceType = f.Resources[0].Type
				resourceId = f.Resources[0].Id
				region = f.Resources[0].Region
			}
			if region == nil {
				region = &accountContext.Region
			}

			firstObservedAt := time.Now()
			if f.FirstObservedAt != nil {
				firstObservedAt, err = time.Parse(time.RFC3339, *f.FirstObservedAt)
				if err != nil {
					log.Println("Error parsing security finding first observed timestamp", err)
					continue
				}
			}

			lastObserverAt := time.Now()
			if f.LastObservedAt != nil {
				lastObserverAt, err = time.Parse(time.RFC3339, *f.LastObservedAt)
				if err != nil {
					log.Println("Error parsing security finding last observed timestamp", err)
					continue
				}
			}

			bytes, _ := json.Marshal(f)
			var metadataString = string(bytes)
			var metadata map[string]interface{}
			err = json.Unmarshal(bytes, &metadata)
			if err != nil {
				log.Println("Error serializing security finding metadata", err)
				continue
			}

			var accountId = accountContext.Id
			var accountName = accountContext.Name
			var team = accountContext.Team
			var environmentType = accountContext.EnvironmentType
			if f.AwsAccountId != nil {
				accountId = *f.AwsAccountId
				accountName = util2.GetAwsAccountName(accountContext, accountId)
				team = util2.GetAwsAccountTeam(accountContext, accountId)
				environmentType = util2.GetAwsAccountEnvironmentType(accountContext, accountId)
			}

			var labels = map[string]interface{}{}
			labels["aws_account_id"] = &accountId
			labels["aws_region"] = region
			labels["aws_resource_type"] = f.Types[0]
			labels["aws_resource_id"] = f.Id

			priority := 9
			if f.Severity != nil && f.Severity.Label != nil {
				if *f.Severity.Label == "INFORMATIONAL" {
					priority = 1 // Informal
				} else if *f.Severity.Label == "LOW" {
					priority = 2 // Low
				} else if *f.Severity.Label == "MEDIUM" {
					priority = 3 // Medium
				} else if *f.Severity.Label == "HIGH" {
					priority = 4 // High
				} else if *f.Severity.Label == "CRITICAL" {
					priority = 5 // Critical
				} else {
					priority = 9 // Unknown
				}
			} else if f.Severity != nil && f.Severity.Normalized != nil {
				if *f.Severity.Normalized == 0 {
					priority = 1 // Informal
				} else if *f.Severity.Normalized == 1 {
					priority = 2 // Low
				} else if *f.Severity.Normalized <= 40 {
					priority = 3 // Medium
				} else if *f.Severity.Normalized <= 70 {
					priority = 4 // High
				} else {
					priority = 5 // Critical
				}
			}

			sourceType := *f.ProductFields["aws/securityhub/ProductName"]
			findingType := *f.Types[0]

			if sourceType == "Security Hub" && f.GeneratorId != nil {
				findingType = *f.GeneratorId
				var startIndex = strings.Index(findingType, "ruleset/")
				if startIndex != -1 {
					findingType = findingType[startIndex+len("ruleset/"):]
				}
			}

			findingTypeLabel := *f.Title
			if sourceType != "Security Hub" {
				findingTypeLabel = findingType
			}

			category := model2.CategoryAnomaly
			state := model2.SecurityFindingState(strings.ToLower(*f.RecordState))
			if f.Compliance != nil {
				if state == "active" && *f.Compliance.Status == "WARNING" {
					category = model2.CategoryWarning
				} else {
					category = model2.CategoryCompliance
				}
			}
			if strings.Contains(metadataString, "CVE") || strings.Contains(metadataString, "vulnerability") {
				category = model2.CategoryVulnerability
			}
			if strings.HasPrefix(findingType, "Software and Configuration Checks") ||
				strings.HasPrefix(findingType, "Software & Configuration Checks") {
				category = model2.CategoryCompliance
			}

			sourceUrl := f.SourceUrl
			if sourceUrl == nil {
				sourceUrlPrefix := "https://" + *region + ".console.aws.amazon.com/securityhub/home?region=" + *region + "#/findings?search="
				sourceUrlQuery := url.QueryEscape("Id=" + url.QueryEscape("\\operator\\:EQUALS\\:"+*f.Id))
				sourceUrlAggregate := sourceUrlPrefix + sourceUrlQuery
				sourceUrl = &sourceUrlAggregate
			}

			remediation := ""
			var remediationUrl *string = nil
			if f.Remediation != nil && f.Remediation.Recommendation != nil {
				if f.Remediation.Recommendation.Text != nil {
					remediation = *f.Remediation.Recommendation.Text
				}
				if f.Remediation.Recommendation.Url != nil {
					remediationUrl = f.Remediation.Recommendation.Url
				}
			}

			*securityFindingChannel <- &model2.SecurityFinding{
				Id:               "",
				SpaceId:          accountContext.SpaceId,
				AccountId:        accountId,
				AccountName:      accountName,
				Region:           *region,
				Team:             team,
				EnvironmentType:  environmentType,
				ResourceType:     resourceType,
				ResourceId:       resourceId,
				Category:         category,
				Type:             findingType,
				TypeLabel:        findingTypeLabel,
				Key:              *f.Id,
				SourceId:         *f.ProductArn,
				SourceType:       sourceType,
				SourceFindingUrl: sourceUrl,
				Title:            *f.Title,
				Description:      *f.Description,
				Remediation:      remediation,
				RemediationUrl:   remediationUrl,
				Metadata:         &metadata,
				Labels:           &labels,
				JiraIssueKey:     nil,
				Priority:         priority,
				OriginalPriority: priority,
				SourcePriority:   priority,
				RulePriority:     nil,
				ManualPriority:   nil,
				State:            state,
				AlertState:       model2.AlertStateInactive,
				Created:          time.Now(),
				Modified:         time.Now(),
				FirstObserved:    firstObservedAt,
				LastObserved:     lastObserverAt,
			}
		}

		nextToken = output.NextToken
		if nextToken == nil {
			break
		}
	}

	return nil
}
