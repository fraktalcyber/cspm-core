package aws

import (
	"bitbucket.org/fraktalcyber/cspm-core/src/core/integration/aws/util"
	"bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	"fmt"
	"github.com/aws/aws-sdk-go/service/ecr"
	"github.com/aws/aws-sdk-go/service/guardduty"
	"github.com/aws/aws-sdk-go/service/securityhub"
	"log"
	"strings"
	"time"
)

func GetCsmpFindings(accountContext *model.AccountContext, securityFindingChannel *chan *model.SecurityFinding) error {
	if err := CheckSecurityHubEnabled(accountContext, securityFindingChannel); err != nil {
		return err
	}
	if err := CheckGuardDutyEnabled(accountContext, securityFindingChannel); err != nil {
		return err
	}
	if err := CheckEcrEnabled(accountContext, securityFindingChannel); err != nil {
		return err
	}
	return nil
}

func CheckSecurityHubEnabled(accountContext *model.AccountContext, securityFindingChannel *chan *model.SecurityFinding) error {
	firstObservedAt := time.Now()
	lastObservedAt := time.Now()
	resourceType := "AWS::SecurityHub::Hub"
	findingType := "Fraktal/Compliance/CheckSecurityHubEnabled"
	key := fmt.Sprintf("%v-%v/%v", findingType, accountContext.Id, accountContext.Region)
	title := "FR1.1 SecurityHub should be enabled."

	var standardArns = []string{"aws-foundational-security-best-practices/v/1.0.0", "cis-aws-foundations-benchmark/v/1.2.0"}

	sh := securityhub.New(accountContext.Session)

	describeHubOutput, err := sh.DescribeHub(nil)
	if err != nil {
		log.Printf("Error describing security hub: %v\n", err)
		util.AddCspmComplianceFinding(accountContext, 5, findingType, &resourceType, nil, key, title, err.Error(), firstObservedAt, lastObservedAt, securityFindingChannel, true)
		return err
	}
	resourceId := describeHubOutput.HubArn

	getEnabledStandardsOutput, err := sh.GetEnabledStandards(nil)
	if err != nil {
		log.Printf("Error getting enabled SecurityHub hub standards: %v\n", err)
		util.AddCspmComplianceFinding(accountContext, 5, findingType, &resourceType, resourceId, key, title, err.Error(), firstObservedAt, lastObservedAt, securityFindingChannel, true)
		return err
	}

	var enabledStandardArns []*string
	for _, standardSubscription := range getEnabledStandardsOutput.StandardsSubscriptions {
		enabledStandardArns = append(enabledStandardArns, standardSubscription.StandardsArn)
	}

	description := ""
	passed := len(enabledStandardArns) > 0
	if !passed {
		description += fmt.Sprintf("SecurityHub standards not enabled: %v.", standardArns)
	} else {
		for _, standardArn := range standardArns {
			found := false
			for _, enabledStandardArn := range enabledStandardArns {
				if strings.Index(*enabledStandardArn, standardArn) != -1 {
					found = true
				}
			}
			if !found {
				description += fmt.Sprintf("SecurityHub standard not enabled: %v.", standardArn)
				passed = false
			}
		}
	}

	var priority int
	if passed {
		priority = 1
	} else {
		priority = 5
	}

	util.AddCspmComplianceFinding(accountContext, priority, findingType, &resourceType, resourceId, key, title, description, firstObservedAt, lastObservedAt, securityFindingChannel, false)

	return nil
}

func CheckGuardDutyEnabled(accountContext *model.AccountContext, securityFindingChannel *chan *model.SecurityFinding) error {
	firstObservedAt := time.Now()
	lastObservedAt := time.Now()
	resourceType := "AWS::GuardDuty::Detector"
	findingType := "Fraktal/Compliance/CheckGuardDutyEnabled"
	key := fmt.Sprintf("%v-%v/%v", findingType, accountContext.Id, accountContext.Region)
	title := "FR1.2 GuardDuty should be enabled."
	description := "Checks that GuardDuty is enabled."

	gd := guardduty.New(accountContext.Session)

	var listDetectorsOutput, err = gd.ListDetectors(nil)

	if err != nil {
		log.Printf("Error listing Guard Duty detectors: %v\n", err)
		util.AddCspmComplianceFinding(accountContext, 5, findingType, &resourceType, nil, key, title, err.Error(), firstObservedAt, lastObservedAt, securityFindingChannel, true)
		return err
	}

	passed := len(listDetectorsOutput.DetectorIds) > 0
	var resourceId *string

	if len(listDetectorsOutput.DetectorIds) > 0 {
		var detectorArn = fmt.Sprintf("arn:aws:guardduty:%v:%v:detector/%v", accountContext.Region, accountContext.Id, listDetectorsOutput.DetectorIds[0])
		resourceId = &detectorArn
	}

	var priority int
	if passed {
		priority = 1
	} else {
		priority = 5
	}

	util.AddCspmComplianceFinding(accountContext, priority, findingType, &resourceType, resourceId, key, title, description, firstObservedAt, lastObservedAt, securityFindingChannel, false)

	return nil
}

func CheckEcrEnabled(accountContext *model.AccountContext, securityFindingChannel *chan *model.SecurityFinding) error {
	firstObservedAt := time.Now()
	lastObservedAt := time.Now()
	resourceType := "AWS::ECR::Repository"
	findingType := "Fraktal/Compliance/CheckEcrImageScanningEnabled"
	title := "FR1.3 ECR scanning should be enabled."
	description := "Checks that ECR scanning is enabled for existing ECR repositories."

	cr := ecr.New(accountContext.Session)

	err := cr.DescribeRepositoriesPages(&ecr.DescribeRepositoriesInput{
		RegistryId: &accountContext.Id,
	}, func(output *ecr.DescribeRepositoriesOutput, lastPage bool) bool {
		for _, repository := range output.Repositories {
			resourceId := repository.RepositoryArn
			key := fmt.Sprintf("%v-%v/%v/%v", findingType, accountContext.Id, accountContext.Region, *repository.RepositoryName)
			var priority int
			if repository.ImageScanningConfiguration.ScanOnPush != nil && *repository.ImageScanningConfiguration.ScanOnPush {
				priority = 1
			} else {
				priority = 5
			}

			util.AddCspmComplianceFinding(accountContext, priority, findingType, &resourceType, resourceId, key, title, description, firstObservedAt, lastObservedAt, securityFindingChannel, false)

		}
		return !lastPage
	})

	if err != nil {
		log.Printf("Error listing ECR repositories : %v\n", err)
		key := fmt.Sprintf("%v-%v/%", findingType, accountContext.Name, accountContext.Region)
		util.AddCspmComplianceFinding(accountContext, 5, findingType, &resourceType, nil, key, title, err.Error(), firstObservedAt, lastObservedAt, securityFindingChannel, true)
		return err
	}

	return nil
}
