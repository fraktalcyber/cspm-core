package aws

import (
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	"encoding/json"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/support"
)

const name = "Trusted Advisor"

var (
	lang                  = aws.String("en")
	interestingCategories = map[string]model2.SecurityFindingCategory{
		"security": model2.CategoryCompliance,
	}
	priorityMapping = map[string]int{
		"not_available": 1,
		"ok":            1,
		"warning":       3,
		"error":         4,
	}
)

func GetTrustedAdvisorCheckResults(accountContext *model2.AccountContext, securityFindingChannel chan *model2.SecurityFinding) error {
	s := support.New(accountContext.Session)

	// First, list all available Trusted Advisor checks.

	checks, err := s.DescribeTrustedAdvisorChecks(&support.DescribeTrustedAdvisorChecksInput{
		Language: lang,
	})
	if err != nil {
		return fmt.Errorf("could not list Trusted Advisor checks: %w", err)
	}

	// Next, see if we need to refresh any checks (AWS does not refresh them automatically).
	// Also, we're only interested in security checks for now.

	var checkIds []*string
	for _, check := range checks.Checks {
		if aws.StringValue(check.Category) == "security" {
			checkIds = append(checkIds, check.Id)
		}
	}
	statuses, err := s.DescribeTrustedAdvisorCheckRefreshStatuses(&support.DescribeTrustedAdvisorCheckRefreshStatusesInput{CheckIds: checkIds})
	if err != nil {
		return fmt.Errorf("error listing Trusted Advisor refresh statuses: %w", err)
	}

	for _, refreshStatus := range statuses.Statuses {
		if aws.Int64Value(refreshStatus.MillisUntilNextRefreshable) > 0 {
			continue
		}

		_, err := s.RefreshTrustedAdvisorCheck(&support.RefreshTrustedAdvisorCheckInput{CheckId: refreshStatus.CheckId})
		if err != nil {
			if !strings.Contains(err.Error(), "is not refreshable") {
				log.Printf("error refreshing Trusted Advisor check %s: %v", aws.StringValue(refreshStatus.CheckId), err)
			}
			continue
		}
	}

	// Finally, list the current status of each check and send them to securityFindingChannel.

	for _, c := range checks.Checks {
		category, ok := interestingCategories[aws.StringValue(c.Category)]
		if !ok {
			continue
		}

		result, err := s.DescribeTrustedAdvisorCheckResult(&support.DescribeTrustedAdvisorCheckResultInput{Language: lang, CheckId: c.Id})
		if err != nil {
			log.Printf("error getting Trusted Advisor status of check (%s) %s: %v", aws.StringValue(c.Name), aws.StringValue(c.Id), err)
			continue
		}

		var labels = map[string]interface{}{}
		bytes, _ := json.Marshal(result.Result)
		var metadata map[string]interface{}
		err = json.Unmarshal(bytes, &metadata)
		if err != nil {
			log.Println("Error serializing trusted advisor check result to metadata", err)
			continue
		}
		var metadataString = string(bytes)

		if len(result.Result.FlaggedResources) == 0 {
			securityFinding := newSecurityFinding(accountContext, c, result, category, metadataString, labels, metadata, nil, nil)
			securityFindingChannel <- &securityFinding
			continue
		}

		for _, resource := range result.Result.FlaggedResources {
			securityFinding := newSecurityFinding(accountContext, c, result, category, metadataString, labels, metadata, resource.ResourceId, nil)
			securityFindingChannel <- &securityFinding
			continue
		}
	}

	return nil
}

func newSecurityFinding(accountContext *model2.AccountContext,
	c *support.TrustedAdvisorCheckDescription,
	result *support.DescribeTrustedAdvisorCheckResultOutput,
	category model2.SecurityFindingCategory, metadataString string,
	labels map[string]interface{},
	metadata map[string]interface{},
	resourceId *string,
	resourceType *string) model2.SecurityFinding {

	var findingType = aws.StringValue(c.Name)
	var findingTypeLabel = aws.StringValue(c.Name)
	var key = "trusted-advisor/" + accountContext.Id + "/" + aws.StringValue(c.Id)
	var priority = priorityMapping[aws.StringValue(result.Result.Status)]
	var title = aws.StringValue(c.Name)
	var description = aws.StringValue(c.Description)

	//log.Printf("Trusted advisor check category '%v' type '%v' key '%v' title '%v' priority '%v' result: %v", category, findingType, key, title, priority, metadataString)

	sourceUrl := "https://console.aws.amazon.com/trustedadvisor/home?region=" + accountContext.Region + "#/category/performance?checkId=" + *c.Id

	base := model2.SecurityFinding{
		Id:               "",
		SpaceId:          accountContext.SpaceId,
		Key:              key,
		AccountId:        accountContext.Id,
		AccountName:      accountContext.Name,
		Region:           accountContext.Region,
		Team:             accountContext.Team,
		EnvironmentType:  accountContext.EnvironmentType,
		SourceId:         name,
		SourceType:       name,
		SourceFindingUrl: &sourceUrl,
		Category:         category,
		Title:            title,
		Description:      description,
		Remediation:      "",
		RemediationUrl:   nil,
		Type:             findingType,
		TypeLabel:        findingTypeLabel,
		Labels:           &labels,
		Metadata:         &metadata,
		JiraIssueKey:     nil,
		Priority:         priority,
		OriginalPriority: priority,
		SourcePriority:   priority,
		RulePriority:     nil,
		ManualPriority:   nil,
		State:            model2.StateActive,
		AlertState:       model2.AlertStateInactive,
		ResourceId:       resourceId,
		ResourceType:     resourceType,
		Created:          time.Now(),
		Modified:         time.Now(),
		FirstObserved:    time.Now(),
		LastObserved:     time.Now(),
	}
	return base
}
