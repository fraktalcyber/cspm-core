package resource

import (
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	uuid "github.com/satori/go.uuid"
	"time"
)

func GetAwsAccountAndRegion(accountContext *model2.AccountContext, resourceChannel chan *model2.Resource) error {
	var labels = map[string]interface{}{}
	labels["awsAccountId"] = &accountContext.Id
	labels["team"] = &accountContext.Team
	labels["environmentType"] = &accountContext.EnvironmentType

	var metadata = map[string]interface{}{}
	metadata["awsAccountId"] = &accountContext.Id
	metadata["team"] = &accountContext.Team
	metadata["environmentType"] = &accountContext.EnvironmentType

	now := time.Now()
	accountKey := "AWS::::Account:" + accountContext.Id
	resourceChannel <- &model2.Resource{
		Id:              uuid.NewV4().String(),
		SpaceId:         accountContext.SpaceId,
		AccountId:       accountContext.Id,
		AccountName:     accountContext.Name,
		Region:          "global",
		Team:            accountContext.Team,
		EnvironmentType: accountContext.EnvironmentType,
		Category:        model2.ResourceCategoryAccount,
		Type:            "AWS::::Account",
		Key:             accountKey,
		Labels:          &labels,
		Metadata:        &metadata,
		State:           model2.ResourceStateActive,
		Created:         now,
		Modified:        now,
		FirstObserved:   now,
		LastObserved:    now,
	}

	labels["awsRegion"] = &accountContext.Region
	metadata["awsRegion"] = &accountContext.Region

	resourceChannel <- &model2.Resource{
		Id:              "",
		ParentKey:       &accountKey,
		SpaceId:         accountContext.SpaceId,
		AccountId:       accountContext.Id,
		AccountName:     accountContext.Name,
		Region:          accountContext.Region,
		Team:            accountContext.Team,
		EnvironmentType: accountContext.EnvironmentType,
		Category:        model2.ResourceCategoryRegion,
		Type:            "AWS::::Region",
		Key:             "AWS::::Region:" + accountContext.Id + "/" + accountContext.Region,
		Labels:          &labels,
		Metadata:        &metadata,
		State:           model2.ResourceStateActive,
		Created:         now,
		Modified:        now,
		FirstObserved:   now,
		LastObserved:    now,
	}

	return nil
}
