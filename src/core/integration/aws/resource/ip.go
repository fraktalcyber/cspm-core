package resource

import (
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	"encoding/json"
	"github.com/aws/aws-sdk-go/service/ec2"
	"log"
	"time"
)

func GetAwsIps(accountContext *model2.AccountContext, resourceChannel chan *model2.Resource) error {
	ec := ec2.New(accountContext.Session)

	var nextToken *string
	for {
		output, err := ec.DescribeNetworkInterfaces(&ec2.DescribeNetworkInterfacesInput{
			NextToken: nextToken,
		})
		if err != nil {
			log.Println("Error describing network interfaces", err)
			return err
		}

		for _, networkInterface := range output.NetworkInterfaces {
			//log.Println(networkInterface)

			if networkInterface.Association != nil {
				resourceType := "public-ip"

				var labels = map[string]interface{}{}
				labels["awsAccountId"] = &accountContext.Id
				labels["awsRegion"] = &accountContext.Region
				labels["team"] = &accountContext.Team
				labels["environmentType"] = &accountContext.EnvironmentType

				if *networkInterface.Association.IpOwnerId == "amazon" {
					// Dynamic public ip
				} else {
					// Elastic IP
					labels["awsElasticIp"] = networkInterface.Association.PublicIp
				}

				var metadata map[string]interface{}
				bytes, _ := json.Marshal(networkInterface)
				err = json.Unmarshal(bytes, &metadata)
				if err != nil {
					log.Println("Error serializing network interface metadata", err)
					continue
				}

				now := time.Now()
				accountKey := "AWS::::Account:" + accountContext.Id
				resourceChannel <- &model2.Resource{
					Id:              "",
					ParentKey:       &accountKey,
					SpaceId:         accountContext.SpaceId,
					AccountId:       accountContext.Id,
					AccountName:     accountContext.Name,
					Region:          accountContext.Region,
					Team:            accountContext.Team,
					EnvironmentType: accountContext.EnvironmentType,
					Category:        model2.ResourceCategoryIp,
					Type:            resourceType,
					Key:             *networkInterface.Association.PublicIp,
					Labels:          &labels,
					Metadata:        &metadata,
					State:           model2.ResourceStateActive,
					Created:         now,
					Modified:        now,
					FirstObserved:   now,
					LastObserved:    now,
				}
			}

		}

		time.Sleep(2000 * time.Millisecond)

		nextToken = output.NextToken
		if nextToken == nil {
			break
		}
	}

	return nil
}
