package resource

import (
	"bitbucket.org/fraktalcyber/cspm-core/src/core/integration/aws/util"
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	util3 "bitbucket.org/fraktalcyber/cspm-core/src/core/util"
	"context"
	"encoding/json"
	"fmt"
	"github.com/aws/aws-sdk-go-v2/service/configservice"
	"github.com/mattn/go-jsonpointer"
	uuid "github.com/satori/go.uuid"
	"log"
	"time"
)

func GetAwsConfigResources(accountContext *model2.AccountContext, resourceChannel chan *model2.Resource) error {

	for _, resourceType := range util.ResourceTypes {
		err := GetAwsConfigResource(accountContext, resourceType, resourceChannel)
		if err != nil {
			return err
		}
	}

	return nil
}

func GetAwsConfigResource(accountContext *model2.AccountContext, resourceType configservice.ResourceType, resourceChannel chan *model2.Resource) error {
	cfg, err := util.GetAwsConfig(accountContext)
	if err != nil {
		log.Println("Error getting AWS config from account context.")
		return err
	}
	//log.Printf("Getting AWS configuration resource type %+v for account and region: %+v / %+v\n", resourceType, accountContext.Key, accountContext.Region)

	var nextToken *string = nil
	for {
		cs := configservice.New(*cfg)

		listDiscoveredResourcesResponse, err := cs.ListDiscoveredResourcesRequest(&configservice.ListDiscoveredResourcesInput{
			NextToken:    nextToken,
			ResourceType: resourceType,
		}).Send(context.TODO())

		if err != nil {
			log.Println("Error getting discovered resources from config.", err)
			return err
		}

		var resourceKeys []configservice.ResourceKey
		for _, resourceIdentifier := range listDiscoveredResourcesResponse.ResourceIdentifiers {
			resourceKeys = append(resourceKeys, configservice.ResourceKey{
				ResourceId:   resourceIdentifier.ResourceId,
				ResourceType: resourceIdentifier.ResourceType,
			})
			//log.Printf("Config resource discovered: %+v (%+v/%+v)\n", util.FormatStringPointer(resourceIdentifier.ResourceName), resourceIdentifier.ResourceType, util.FormatStringPointer(resourceIdentifier.ResourceId))
		}

		if len(resourceKeys) > 0 {
			response, err := cs.BatchGetResourceConfigRequest(&configservice.BatchGetResourceConfigInput{
				ResourceKeys: resourceKeys,
			}).Send(context.TODO())

			if err != nil {
				log.Println("Error getting resource configs for discovered resources.", err)
				return err
			}

			for _, item := range response.BatchGetResourceConfigOutput.BaseConfigurationItems {
				//log.Printf("Resource ID: %+v\n", util.FormatStringPointer(item.ResourceId))
				//log.Printf("Resource configuration: %+v\n", util.FormatStringPointer(item.Configuration))

				var configuration map[string]interface{}
				err = json.Unmarshal([]byte(*item.Configuration), &configuration)
				if err != nil {
					log.Println("Error serializing network interface metadata", err)
					return err
				}

				sendConfigResource(accountContext, item, resourceChannel, configuration)

				//log.Printf("Resource suplementary configuration: %+v\n", item.SupplementaryConfiguration)

				getDomainName(accountContext, resourceType, &item, configuration, "/dNSName", resourceChannel)
				getDomainName(accountContext, resourceType, &item, configuration, "/endpoint/address", resourceChannel)

			}
		}

		time.Sleep(2000 * time.Millisecond)

		nextToken = listDiscoveredResourcesResponse.NextToken
		if nextToken == nil {
			break
		}
	}
	return nil
}

func getDomainName(accountContext *model2.AccountContext, resourceType configservice.ResourceType, item *configservice.BaseConfigurationItem, configuration map[string]interface{}, jsonPointer string, resourceChannel chan *model2.Resource) {
	domainName := GetStringWithJsonPointer(configuration, jsonPointer)
	if domainName != "" {
		//log.Printf("- Found domain name: %+v\n", domainName)
		accountId := *item.AccountId
		accountRegion := *item.AwsRegion
		accountName := util.GetAwsAccountName(accountContext, accountId)
		accountKey := "AWS::::Account:" + accountId
		accountTeam := util.GetAwsAccountTeam(accountContext, accountId)
		accountEnvironmentType := util.GetAwsAccountEnvironmentType(accountContext, accountId)

		var labels = map[string]interface{}{}
		labels["awsAccountId"] = &accountId
		labels["team"] = &accountTeam
		labels["environmentType"] = &accountEnvironmentType

		now := time.Now()
		resourceKey := domainName
		resourceChannel <- &model2.Resource{
			Id:              uuid.NewV4().String(),
			SpaceId:         accountContext.SpaceId,
			ParentKey:       &accountKey,
			AccountId:       accountId,
			AccountName:     accountName,
			Region:          accountRegion,
			Team:            accountTeam,
			EnvironmentType: accountEnvironmentType,
			Category:        model2.ResourceCategoryDomainName,
			Type:            "domain-name",
			Key:             resourceKey,
			Labels:          &labels,
			Metadata:        &labels,
			State:           model2.ResourceStateActive,
			Created:         now,
			Modified:        now,
			FirstObserved:   now,
			LastObserved:    now,
		}
	}
}

func GetStringWithJsonPointer(configuration map[string]interface{}, jsonPointer string) string {
	value, _ := jsonpointer.Get(configuration, jsonPointer)
	return util3.ValueToString(value)
}

func sendConfigResource(accountContext *model2.AccountContext, item configservice.BaseConfigurationItem, resourceChannel chan *model2.Resource, metadata map[string]interface{}) {
	accountId := *item.AccountId
	accountRegion := *item.AwsRegion
	accountName := util.GetAwsAccountName(accountContext, accountId)
	accountKey := "AWS::::Account:" + accountId
	accountTeam := util.GetAwsAccountTeam(accountContext, accountId)
	accountEnvironmentType := util.GetAwsAccountEnvironmentType(accountContext, accountId)

	var labels = map[string]interface{}{}
	labels["awsAccountId"] = &accountId
	labels["team"] = &accountTeam
	labels["environmentType"] = &accountEnvironmentType

	resourceTypeString := fmt.Sprintf("%v", item.ResourceType)
	now := time.Now()
	var resourceKey string
	if item.Arn != nil {
		resourceKey = *item.Arn
	} else {
		resourceKey = resourceTypeString + ":" + *item.ResourceId
	}
	resourceChannel <- &model2.Resource{
		Id:              uuid.NewV4().String(),
		SpaceId:         accountContext.SpaceId,
		ParentKey:       &accountKey,
		AccountId:       accountId,
		AccountName:     accountName,
		Region:          accountRegion,
		Team:            accountTeam,
		EnvironmentType: accountEnvironmentType,
		Category:        model2.ResourceCategoryAwsConfigResource,
		Type:            resourceTypeString,
		Key:             resourceKey,
		Labels:          &labels,
		Metadata:        &metadata,
		State:           model2.ResourceStateActive,
		Created:         now,
		Modified:        now,
		FirstObserved:   now,
		LastObserved:    now,
	}
}
