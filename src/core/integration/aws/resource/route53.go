package resource

import (
	"bitbucket.org/fraktalcyber/cspm-core/src/core/integration/aws/util"
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	"context"
	"github.com/aws/aws-sdk-go-v2/service/route53"
	uuid "github.com/satori/go.uuid"
	"log"
	"net"
	"time"
)

func GetRoute53Resources(accountContext *model2.AccountContext, resourceChannel chan *model2.Resource) error {
	if accountContext.Region != "us-east-1" {
		return nil // Global services only against us-east-1 to avoid duplicates.
	}

	cfg, err := util.GetAwsConfig(accountContext)
	if err != nil {
		log.Println("Error getting AWS config from account context.")
		return err
	}

	rc := route53.New(*cfg)

	var nextToken *string = nil
	for {
		listHostedZonesResponse, err := rc.ListHostedZonesRequest(&route53.ListHostedZonesInput{
			DelegationSetId: nil,
			Marker:          nextToken,
			MaxItems:        nil,
		}).Send(context.TODO())
		if err != nil {
			log.Println("Error listing hosted zones.", err)
			return err
		}

		//log.Printf("route53 hosted zone count for account %+v: %+v\n", accountContext.Key, len(listHostedZonesResponse.HostedZones))

		for _, hostedZone := range listHostedZonesResponse.HostedZones {
			//log.Printf("route53 hosted zone: %+v/%+v (%+v)\n", accountContext.Key, util.FormatStringPointer(hostedZone.Key), util.FormatStringPointer(hostedZone.Id))

			err = GetHostedZoneResources(accountContext, rc, hostedZone, resourceChannel)
			if err != nil {
				return err
			}
		}

		time.Sleep(2000 * time.Millisecond)

		nextToken = listHostedZonesResponse.NextMarker
		if nextToken == nil {
			break
		}
	}

	return nil
}

func GetHostedZoneResources(accountContext *model2.AccountContext, rc *route53.Client, hostedZone route53.HostedZone, resourceChannel chan *model2.Resource) error {
	var nextRecordToken *string = nil
	for {
		listResourceRecordSetsRequestResponse, err := rc.ListResourceRecordSetsRequest(&route53.ListResourceRecordSetsInput{
			HostedZoneId:          hostedZone.Id,
			MaxItems:              nil,
			StartRecordIdentifier: nextRecordToken,
			StartRecordName:       nil,
			StartRecordType:       "",
		}).Send(context.TODO())

		if err != nil {
			log.Println("Error listing resource record sets.", err)
			return err
		}

		for _, resourceRecordSet := range listResourceRecordSetsRequestResponse.ResourceRecordSets {
			//log.Printf(" route53 record set %+v/%+v: %+v %+v", accountContext.Key, util.FormatStringPointer(hostedZone.Key), resourceRecordSet.Type, util.FormatStringPointer(resourceRecordSet.Key))
			if resourceRecordSet.Type == route53.RRTypeA || resourceRecordSet.Type == route53.RRTypeAaaa || resourceRecordSet.Type == route53.RRTypeCname {
				domainName := *resourceRecordSet.Name
				ips, err := net.LookupHost(domainName)
				if err != nil {
					//log.Println("Error resolving domain name IP addresses.", err)
					continue
				}
				/*for _, ip := range ips {
					log.Printf("   ip %+v",ip)
				}*/

				accountId := accountContext.Id
				accountRegion := "global"
				accountName := util.GetAwsAccountName(accountContext, accountId)
				accountKey := "AWS::::Account:" + accountId
				accountTeam := util.GetAwsAccountTeam(accountContext, accountId)
				accountEnvironmentType := util.GetAwsAccountEnvironmentType(accountContext, accountId)

				var labels = map[string]interface{}{}
				labels["awsAccountId"] = &accountId
				labels["team"] = &accountTeam
				labels["environmentType"] = &accountEnvironmentType
				labels["ips"] = ips

				now := time.Now()
				resourceKey := domainName
				resourceChannel <- &model2.Resource{
					Id:              uuid.NewV4().String(),
					SpaceId:         accountContext.SpaceId,
					ParentKey:       &accountKey,
					AccountId:       accountId,
					AccountName:     accountName,
					Region:          accountRegion,
					Team:            accountTeam,
					EnvironmentType: accountEnvironmentType,
					Category:        model2.ResourceCategoryDomainName,
					Type:            "domain-name",
					Key:             resourceKey,
					Labels:          &labels,
					Metadata:        &labels,
					State:           model2.ResourceStateActive,
					Created:         now,
					Modified:        now,
					FirstObserved:   now,
					LastObserved:    now,
				}
			}

		}

		time.Sleep(2000 * time.Millisecond)

		nextRecordToken = listResourceRecordSetsRequestResponse.NextRecordIdentifier
		if nextRecordToken == nil {
			break
		}
	}
	return nil
}
