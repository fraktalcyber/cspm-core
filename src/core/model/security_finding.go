package model

import (
	"time"
)

type SecurityFinding struct {
	tableName        struct{}                `pg:"security_finding,alias:g"`
	Id               string                  `json:"id"`
	SpaceId          string                  `json:"spaceId"`
	AccountId        string                  `json:"accountId"`
	AccountName      string                  `json:"accountName"`
	Region           string                  `json:"region"`
	Team             string                  `json:"team"`
	EnvironmentType  string                  `json:"environmentType"`
	ResourceType     *string                 `json:"resourceType"`
	ResourceId       *string                 `json:"resourceId"`
	Category         SecurityFindingCategory `json:"category"`
	Type             string                  `json:"type"`
	TypeLabel        string                  `json:"typeLabel" pg:",use_zero"`
	Key              string                  `json:"key"`
	SourceType       string                  `json:"sourceType"`
	SourceId         string                  `json:"sourceId"`
	SourceFindingUrl *string                 `json:"sourceFindingUrl"`
	Title            string                  `json:"title"`
	Description      string                  `json:"description" pg:",use_zero"`
	Remediation      string                  `json:"remediation" pg:",use_zero"`
	RemediationUrl   *string                 `json:"remediationUrl"`

	Labels   *map[string]interface{} `json:"labels"`
	Metadata *map[string]interface{} `json:"metadata"`

	JiraIssueKey *string `json:"jiraIssueKey"`

	Priority         int  `pg:",use_zero" json:"priority"`
	OriginalPriority int  `pg:",use_zero" json:"originalPriority"`
	SourcePriority   int  `pg:",use_zero" json:"sourcePriority"`
	RulePriority     *int `pg:",use_zero" json:"rulePriority"`
	ManualPriority   *int `pg:",use_zero" json:"manualPriority"`

	State      SecurityFindingState      `json:"state"`
	AlertState SecurityFindingAlertState `json:"alertState"`

	LastComment string `json:"lastComment"`
	LastAuthor  string `json:"lastAuthor"`

	Created       time.Time `json:"created"`
	Modified      time.Time `json:"modified"`
	LastObserved  time.Time `json:"lastObserved"`
	FirstObserved time.Time `json:"firstObserved"`
}
