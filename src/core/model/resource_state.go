package model

type ResourceState string

const (
	ResourceStateActive   ResourceState = "active"
	ResourceStateArchived ResourceState = "archived"
)
