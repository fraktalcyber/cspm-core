package model

import (
	"time"
)

type ProcessingRun struct {
	tableName   struct{}  `pg:"processing_run,alias:g"`
	Id          string    `json:"id"`
	SpaceId     string    `json:"spaceId"`
	Key         string    `json:"key"`
	PeriodStart time.Time `json:"modified"`
	PeriodEnd   time.Time `json:"modified"`
	Created     time.Time `json:"created"`
	Modified    time.Time `json:"modified"`
}
