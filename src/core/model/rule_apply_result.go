package model

type RuleApplyResult struct {
	SecurityFinding  *SecurityFinding
	Author           string
	Comment          string
	Changed          bool
	Prioritized      bool
	Alerted          bool
	AlertActivated   bool
	AlertDeactivated bool
}
