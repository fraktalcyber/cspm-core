package model

import (
	"time"
)

type Version struct {
	tableName struct{}  `pg:"version,alias:g"`
	Type      string    `pg:"type,pk" json:"type"`
	Version   string    `pg:"version,pk" json:"version"`
	Created   time.Time `json:"created"`
}
