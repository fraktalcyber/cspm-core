package model

const (
	SecurityFindingTypeConsoleSignin                  string = "Console Signin"
	SecurityFindingTypeConsoleSigninNoMfa             string = "Console Signin No MFA"
	SecurityFindingTypeUnauthorizedAPICalls           string = "Unauthorized API Calls"
	SecurityFindingTypeRootLogin                      string = "Root Login"
	SecurityFindingTypeCloudTrailConfigurationChanges string = "Cloud Trail Configuration Changes"
	SecurityFindingFailedAuthentication               string = "Failed Authentication"
	SecurityFindingDisableOrDeleteCMK                 string = "Disable or Delete CMK"
	SecurityFindingS3BucketPolicyChanges              string = "S3 Bucket Policy Changes"
	SecurityFindingAWSConfigChanges                   string = "AWS Config Changes"
	SecurityFindingSecurityGroupChanges               string = "Security Group Changes"
	SecurityFindingIAMChanges                         string = "IAM Changes"

	SecurityFindingNACLChanges       string = "NACL Changes"
	SecurityFindingNetworkGWChanges  string = "Network GW Changes"
	SecurityFindingRouteTableChanges string = "Route Table Changes"
	SecurityFindingVPCChanges        string = "VPC Changes"
)
