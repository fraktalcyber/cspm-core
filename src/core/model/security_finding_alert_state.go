package model

type SecurityFindingAlertState string

const (
	AlertStateInactive SecurityFindingAlertState = "inactive"
	AlertStateActive   SecurityFindingAlertState = "active"
)
