package model

type SecurityFindingCategory string

const (
	CategoryCompliance    SecurityFindingCategory = "compliance"
	CategoryAnomaly       SecurityFindingCategory = "anomaly"
	CategoryVulnerability SecurityFindingCategory = "vulnerability"
	CategoryWarning       SecurityFindingCategory = "warning"
	CategoryNotification  SecurityFindingCategory = "notification"
)
