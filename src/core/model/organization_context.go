package model

type OrganizationContext struct {
	MasterAccountContext *AccountContext
	LogAccountContext    *AccountContext
	AccountContexts      []*AccountContext
}
