package model

type SecurityFindingState string

const (
	StateActive   SecurityFindingState = "active"
	StateArchived SecurityFindingState = "archived"
)
