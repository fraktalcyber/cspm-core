package model

type ResourceCategory string

const (
	ResourceCategoryAccount           ResourceCategory = "account"
	ResourceCategoryRegion            ResourceCategory = "region"
	ResourceCategoryDomainName        ResourceCategory = "domain-name"
	ResourceCategoryIp                ResourceCategory = "ip"
	ResourceCategoryPort              ResourceCategory = "port"
	ResourceCategoryAwsConfigResource ResourceCategory = "aws-config-resource"
)
