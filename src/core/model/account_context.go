package model

import (
	"github.com/aws/aws-sdk-go/aws/session"
)

type AccountContext struct {
	Id              string
	Name            string
	Region          string
	SpaceId         string
	Team            string
	EnvironmentType string
	Session         *session.Session
	AccountNames    map[string]string
	AccountContexts map[string]*AccountContext
}
