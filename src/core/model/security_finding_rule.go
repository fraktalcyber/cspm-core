package model

import (
	"time"
)

type SecurityFindingRule struct {
	tableName struct{} `pg:"security_finding_rule,alias:g"`

	Id         string       `json:"id"`
	SpaceId    string       `json:"spaceId"`
	Type       string       `json:"type"`
	Priority   int          `pg:",use_zero" json:"priority"`
	Key        string       `json:"key"`
	Criteria   CriteriaPart `json:"metadata"`
	Prioritize int          `pg:",use_zero" json:"prioritize"`
	Alert      bool         `pg:",use_zero" json:"alert"`
	Archive    bool         `pg:",use_zero" json:"archive"`
	Enabled    bool         `pg:",use_zero" json:"enabled"`

	Created  time.Time `json:"created"`
	Modified time.Time `json:"modified"`
}
