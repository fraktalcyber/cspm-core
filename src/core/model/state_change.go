package model

import (
	"time"
)

type StateChange struct {
	tableName         struct{} `pg:"security_finding_state_change,alias:g"`
	Id                string
	SpaceId           string `json:"spaceId"`
	SecurityFindingId string

	Metadata *map[string]interface{}
	State    SecurityFindingState
	Priority int `pg:",use_zero"`
	Comment  string
	Author   string

	Created  time.Time
	Modified time.Time
}
