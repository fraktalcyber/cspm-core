package model

type Rule struct {
	Id       string
	Category string
	Key      string
	Priority int
	Enabled  bool
	Criteria []*CriteriaPart
	Action   Action
}

type Action struct {
	Prioritize int
	Alert      bool
	Archive    bool
}

type CriteriaPart struct {
	Path     *string
	Operator *Operator
	Value    *string
	Criteria []*CriteriaPart
}

type Operator string

const (
	OperatorEquals    Operator = "=="
	OperatorNotEquals Operator = "!="
	OperatorAnd       Operator = "&&"
	OperatorOr        Operator = "||"
	OperatorUndefined Operator = "?"
)

var CombiningLogicalOperators = []Operator{OperatorAnd, OperatorOr}
