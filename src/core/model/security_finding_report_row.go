package model

import (
	"time"
)

type SecurityFindingReportRow struct {
	tableName       struct{}                `pg:"security_finding_report_row,alias:g"`
	Id              string                  `json:"id"`
	SpaceId         string                  `json:"spaceId"`
	Key             string                  `json:"key"`
	AccountId       string                  `json:"accountId"`
	AccountName     string                  `json:"accountName"`
	Region          string                  `json:"region"`
	Team            string                  `json:"team"`
	EnvironmentType string                  `json:"environmentType"`
	SourceType      string                  `json:"sourceType"`
	Category        SecurityFindingCategory `json:"category"`
	Type            string                  `json:"type"`
	Priority        int                     `pg:",use_zero" json:"priority"`
	FindingCount    int                     `pg:",use_zero" json:"findingCount"`
	RowDate         time.Time               `json:"rowDate"`
	Created         time.Time               `json:"created"`
	Modified        time.Time               `json:"modified"`
}
