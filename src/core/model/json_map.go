package model

import (
	"errors"
	"fmt"
	"log"
)

func GetMandatoryMapValue(jsonMap *map[string]interface{}, key string) (*map[string]interface{}, error) {
	if val, ok := (*jsonMap)[key]; ok && val != nil {
		valueMap := val.(map[string]interface{})
		return &valueMap, nil
	} else {
		errorMessage := fmt.Sprintf("JSON MAP did not contain %v: %v", key, jsonMap)
		log.Println(errorMessage)
		return nil, errors.New(errorMessage)
	}
}

func GetMandatoryArrayValue(jsonMap *map[string]interface{}, key string) (*[]interface{}, error) {
	if val, ok := (*jsonMap)[key]; ok && val != nil {
		valueArray := val.([]interface{})
		return &valueArray, nil
	} else {
		errorMessage := fmt.Sprintf("JSON MAP did not contain %v: %v", key, jsonMap)
		log.Println(errorMessage)
		return nil, errors.New(errorMessage)
	}
}

func GetMandatoryStringValue(jsonMap *map[string]interface{}, key string) (string, error) {
	if val, ok := (*jsonMap)[key]; ok && val != nil {
		valueString := val.(string)
		return valueString, nil
	} else {
		errorMessage := fmt.Sprintf("JSON MAP did not contain %v: %v", key, jsonMap)
		return "", errors.New(errorMessage)
	}
}

func GetOptionalStringValue(jsonMap *map[string]interface{}, key string, defaultValue *string) *string {
	if val, ok := (*jsonMap)[key]; ok {
		valueString := val.(string)
		return &valueString
	} else {
		return defaultValue
	}
}
