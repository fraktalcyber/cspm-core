package model

import (
	"time"
)

type Resource struct {
	tableName       struct{}         `pg:"resource,alias:g"`
	Id              string           `json:"id"`
	ParentId        *string          `json:"parent_id"`
	Key             string           `json:"key"`
	Category        ResourceCategory `json:"category"`
	Type            string           `json:"type"`
	SpaceId         string           `json:"spaceId"`
	AccountId       string           `json:"accountId"`
	AccountName     string           `json:"accountName"`
	Region          string           `json:"region"`
	Team            string           `json:"team"`
	EnvironmentType string           `json:"environmentType"`

	Labels            *map[string]interface{} `json:"labels"`
	Metadata          *map[string]interface{} `json:"metadata"`
	VulnerabilityKeys []string                `json:"vulnerabilityKeys" pg:",array"`

	State ResourceState `json:"state"`

	Created       time.Time `json:"created"`
	Modified      time.Time `json:"modified"`
	LastObserved  time.Time `json:"lastObserved"`
	FirstObserved time.Time `json:"firstObserved"`

	ParentKey *string `pg:"-"`
}
