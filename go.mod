module bitbucket.org/fraktalcyber/cspm-core

go 1.15

require (
	github.com/aws/aws-sdk-go v1.34.6
	github.com/aws/aws-sdk-go-v2 v0.24.0
	github.com/go-pg/pg/v9 v9.1.7
	github.com/mattn/go-jsonpointer v0.0.1
	github.com/satori/go.uuid v1.2.0
	gopkg.in/ini.v1 v1.62.0
)
