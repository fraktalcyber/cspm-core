CREATE INDEX idx_security_finding_space_id_account_id_region
    ON public.security_finding USING btree
    (space_id ASC NULLS LAST, account_id ASC NULLS LAST, region ASC NULLS LAST)
    TABLESPACE pg_default;
CREATE INDEX idx_security_finding_report_row_row_date_category_team_account_
    ON public.security_finding_report_row USING btree
    (row_date ASC NULLS LAST, category COLLATE pg_catalog."default" ASC NULLS LAST, team COLLATE pg_catalog."default" ASC NULLS LAST, account_name COLLATE pg_catalog."default" ASC NULLS LAST, region COLLATE pg_catalog."default" ASC NULLS LAST)
    WITH (FILLFACTOR=80)
    TABLESPACE pg_default;

ALTER TABLE public.security_finding_report_row
    CLUSTER ON idx_security_finding_report_row_row_date_category_team_account_;
