CREATE TABLE public.vulnerability
(
    id uuid NOT NULL,
    key character varying(20) COLLATE pg_catalog."default" NOT NULL,
    source character varying(80) COLLATE pg_catalog."default" NOT NULL,
    summary character varying(400) COLLATE pg_catalog."default" NOT NULL,
    severity numeric(2,0) NOT NULL,
    score numeric(6,2) NOT NULL,
    metadata jsonb NOT NULL,
    created timestamp with time zone NOT NULL,
    modified timestamp with time zone NOT NULL,
    "references" character varying(500)[] COLLATE pg_catalog."default",
    CONSTRAINT vulnerability_pkey PRIMARY KEY (id)
);

ALTER TABLE public.resource
    ADD COLUMN parent_id uuid;
ALTER TABLE public.resource
    ADD FOREIGN KEY (parent_id)
        REFERENCES public.resource (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION;

ALTER TABLE public.resource
    ADD COLUMN vulnerability_keys character varying(20)[];

ALTER TABLE public.security_finding
    ALTER COLUMN title TYPE character varying(400) COLLATE pg_catalog."default";