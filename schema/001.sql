CREATE TABLE public.space
(
    id uuid NOT NULL,
    name character varying(80) COLLATE pg_catalog."default" NOT NULL,
    created timestamp with time zone NOT NULL,
    modified timestamp with time zone NOT NULL,
    CONSTRAINT space_pkey PRIMARY KEY (id)
);

CREATE TABLE public.security_finding
(
    id uuid NOT NULL,
    category character varying(40) COLLATE pg_catalog."default" NOT NULL,
    type character varying(300) COLLATE pg_catalog."default" NOT NULL,
    space_id uuid NOT NULL,
    account_id character varying(100) COLLATE pg_catalog."default" NOT NULL,
    account_name character varying(40) COLLATE pg_catalog."default" NOT NULL,
    region character varying(40) COLLATE pg_catalog."default" NOT NULL,
    resource_id character varying(400) COLLATE pg_catalog."default",
    resource_type character varying(80) COLLATE pg_catalog."default",
    source_id character varying(100) COLLATE pg_catalog."default" NOT NULL,
    source_type character varying(100) COLLATE pg_catalog."default" NOT NULL,
    key character varying(200) COLLATE pg_catalog."default" NOT NULL,
    title character varying(300) COLLATE pg_catalog."default" NOT NULL,
    description character varying(1000) COLLATE pg_catalog."default" NOT NULL,
    last_comment character varying(1000) COLLATE pg_catalog."default" NOT NULL,
    last_author character varying(100) COLLATE pg_catalog."default" NOT NULL,
    labels jsonb,
    metadata jsonb,
    priority numeric(1,0) NOT NULL,
    original_priority numeric(1,0) NOT NULL,
    source_priority numeric(1,0) NOT NULL,
    rule_priority numeric(1,0),
    manual_priority numeric(1,0),
    state character varying(40) COLLATE pg_catalog."default" NOT NULL,
    alert_state character varying(40) COLLATE pg_catalog."default" NOT NULL,
    jira_issue_key character varying(256) COLLATE pg_catalog."default",
    created timestamp with time zone NOT NULL,
    modified timestamp with time zone NOT NULL,
    last_observed timestamp with time zone NOT NULL,
    first_observed timestamp with time zone NOT NULL,
    CONSTRAINT security_finding_pkey PRIMARY KEY (id),
    CONSTRAINT security_event_space_id_fkey FOREIGN KEY (space_id)
        REFERENCES public.space (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE public.security_finding_state_change
(
    id uuid NOT NULL,
    space_id uuid NOT NULL,
    security_finding_id uuid NOT NULL,
    metadata jsonb NOT NULL,
    priority numeric NOT NULL,
    state character varying(40) COLLATE pg_catalog."default" NOT NULL,
    comment character varying(1000) COLLATE pg_catalog."default" NOT NULL,
    author character varying(100) COLLATE pg_catalog."default" NOT NULL,
    created timestamp with time zone NOT NULL,
    modified timestamp with time zone NOT NULL,
    CONSTRAINT security_finding_state_change_pkey PRIMARY KEY (id),
    CONSTRAINT security_finding_state_change_security_finding_id_fkey FOREIGN KEY (security_finding_id)
        REFERENCES public.security_finding (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE,
    CONSTRAINT security_finding_state_change_space_id_fkey FOREIGN KEY (space_id)
        REFERENCES public.space (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

INSERT INTO public.space(
    id, name, created, modified)
VALUES ('00000000-0000-0000-0000-000000000000', 'default', now(), now());