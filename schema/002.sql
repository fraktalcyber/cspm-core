ALTER TABLE public.security_finding
    ALTER COLUMN key TYPE character varying(300) COLLATE pg_catalog."default";
CREATE INDEX security_finding_priority_category_source_type_title_idx
    ON public.security_finding USING btree
        (priority DESC NULLS LAST, category ASC NULLS LAST, source_type ASC NULLS LAST, title ASC NULLS LAST, account_id ASC NULLS LAST, region ASC NULLS LAST, last_observed ASC NULLS LAST);