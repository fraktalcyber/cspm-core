CREATE TABLE public.security_finding_rule
(
    id uuid NOT NULL,
    space_id uuid,
    type character varying(200) NOT NULL,
    priority numeric(1,0) NOT NULL,
    key character varying(200) NOT NULL,
    criteria jsonb NOT NULL,
    prioritize numeric(1, 0) NOT NULL,
    alert boolean NOT NULL,
    archive boolean NOT NULL,
    enabled boolean NOT NULL,
    created timestamp with time zone NOT NULL,
    modified timestamp with time zone NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (space_id)
        REFERENCES public.space (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
);
