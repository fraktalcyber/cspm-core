ALTER TABLE public.resource
    ADD CONSTRAINT resource_key_key UNIQUE (key);

CREATE INDEX resource_state_categoty_type_idx
    ON public.resource USING btree
        (state ASC NULLS LAST, category ASC NULLS LAST, type ASC NULLS LAST);

CREATE INDEX resource_last_observed_state_idx
    ON public.resource USING btree
        (last_observed ASC NULLS LAST, state ASC NULLS LAST);

ALTER TABLE public.security_finding
    ADD CONSTRAINT security_finding_key_key UNIQUE (key);

DROP INDEX security_finding_key_idx;

ALTER TABLE public.resource
    ADD COLUMN team character varying(80);

ALTER TABLE public.resource
    ADD COLUMN environment_type character varying(20);

ALTER TABLE public.security_finding
    ADD COLUMN team character varying(80);

ALTER TABLE public.security_finding
    ADD COLUMN environment_type character varying(20);
