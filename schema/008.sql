CREATE TABLE public.security_finding_report_row
(
    id uuid NOT NULL,
    space_id uuid NOT NULL,
    key character varying(400) NOT NULL,
    account_id character varying(100) NOT NULL,
    account_name character varying(40) NOT NULL,
    region character varying(40) NOT NULL,
    team character varying(80),
    environment_type character varying(20),
    source_type character varying(100) NOT NULL,
    category character varying(40) NOT NULL,
    type character varying(300) NOT NULL,
    priority numeric(1) NOT NULL,
    finding_count numeric(10) NOT NULL,
    row_date timestamp with time zone NOT NULL,
    created timestamp with time zone NOT NULL,
    modified timestamp with time zone NOT NULL,
    CONSTRAINT security_finding_report_row_pkey PRIMARY KEY (id),
    CONSTRAINT security_finding_report_row_key_key UNIQUE (key)
);

CREATE INDEX idx_security_finding_state_change_security_finding_id_created
    ON public.security_finding_state_change USING btree
    (security_finding_id ASC NULLS LAST, created ASC NULLS LAST);

CREATE TABLE processing_run
(
    id uuid NOT NULL,
    space_id uuid NOT NULL,
    key character varying(400) NOT NULL,
    period_start timestamp with time zone NOT NULL,
    period_end timestamp with time zone NOT NULL,
    created timestamp with time zone NOT NULL,
    modified timestamp with time zone NOT NULL,
    CONSTRAINT processing_run_pkey PRIMARY KEY (id)
);