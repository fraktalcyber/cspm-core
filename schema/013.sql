ALTER TABLE public.resource DROP CONSTRAINT resource_parent_id_fkey;

ALTER TABLE public.resource
    ADD FOREIGN KEY (parent_id)
    REFERENCES public.resource (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;
