ALTER TABLE public.security_finding
    ALTER COLUMN jira_issue_key TYPE character varying(300) COLLATE pg_catalog."default";

ALTER TABLE public.security_finding
    ADD COLUMN type_label character varying(200);

ALTER TABLE public.security_finding
    ADD COLUMN remediation character varying(1000);

ALTER TABLE public.security_finding
    ADD COLUMN remediation_url character varying(500);

ALTER TABLE public.security_finding
    ADD COLUMN source_finding_url character varying(500);
