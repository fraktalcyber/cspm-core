CREATE INDEX security_finding_state_alert_state_idx
    ON public.security_finding USING btree
        (state ASC NULLS LAST, alert_state ASC NULLS LAST);
CREATE INDEX security_finding_key_idx
    ON public.security_finding USING btree
        (key ASC NULLS LAST);