CREATE TABLE public.resource
(
    id uuid NOT NULL,
    key character varying(400) NOT NULL,
    category character varying(40) NOT NULL,
    type character varying(80) NOT NULL,
    space_id uuid NOT NULL,
    account_id character varying(100) COLLATE pg_catalog."default" NOT NULL,
    account_name character varying(40) COLLATE pg_catalog."default" NOT NULL,
    region character varying(40) COLLATE pg_catalog."default" NOT NULL,
    state character varying(40) NOT NULL,
    labels jsonb,
    metadata jsonb,
    first_observed timestamp with time zone NOT NULL,
    last_observed timestamp with time zone NOT NULL,
    created timestamp with time zone NOT NULL,
    modified timestamp with time zone NOT NULL,
    PRIMARY KEY (id)
);
