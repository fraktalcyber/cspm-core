#!/bin/sh
if [ "$(id -u)" != "1000" ]; then
   echo "Container must be run with UID 1000" 1>&2
   exit 1
fi
echo "USER: "
whoami
echo "WORKING DIRECTORY: "
pwd
ls -la
./cspm-core