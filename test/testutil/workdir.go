package testutil

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

func SetTestWorkingDirectory() {
	dir, _ := os.Getwd()
	for !strings.HasSuffix(dir, "/test") {
		dir = filepath.Dir(dir)
		if dir == "/" {
			fmt.Printf("Setting working directory failed. Application root directory not found.")
			os.Exit(1)
		}
	}

	workingDirectory := dir + "/.."
	err := os.Chdir(workingDirectory)
	if err != nil {
		fmt.Printf("Setting working directory failed: %v", err)
		os.Exit(1)
	} else {
		fmt.Printf("Workdir: %v\n", workingDirectory)
	}

}
