package testutil

import (
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	"encoding/json"
	"io/ioutil"
)

func LoadJson(itemPath string) (map[string]interface{}, error) {
	itemBytes, err := ioutil.ReadFile(itemPath)
	if err != nil {
		return nil, err
	}

	var item map[string]interface{}
	err = json.Unmarshal(itemBytes, &item)
	if err != nil {
		return nil, err
	}
	return item, nil
}

func LoadRule(itemPath string) (*model2.Rule, error) {
	itemBytes, err := ioutil.ReadFile(itemPath)
	if err != nil {
		return nil, err
	}

	var item model2.Rule
	err = json.Unmarshal(itemBytes, &item)
	if err != nil {
		return nil, err
	}
	return &item, nil
}
