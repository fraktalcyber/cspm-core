package testutil

import (
	"bitbucket.org/fraktalcyber/cspm-core/src/core/storage/db"
	util2 "bitbucket.org/fraktalcyber/cspm-core/src/core/util"
	"github.com/go-pg/pg/v9"
	"log"
	"os"
	"testing"
)

func Setup() {
	if err := os.Setenv("ENV", "test"); err != nil {
		panic(err)
	}
	SetTestWorkingDirectory()
	util2.LoadConfig()
}

func SetupTestApplicationDatabase(t *testing.T) *db.Database {
	database := db.NewApplicationDatabase()

	err := database.Start()
	if err != nil {
		t.Fatalf("could not start datatabase: %v", err)
	}

	err = flushDB(t, database.Db)
	if err != nil {
		t.Fatalf("could not flush database: %v", err)
	}

	database.Db.Exec("INSERT INTO public.space(\n    id, name, created, modified)\nVALUES ('00000000-0000-0000-0000-000000000000', 'default', now(), now())")

	return database
}

func TearDownTestApplicationDatabase(t *testing.T, database *db.Database) {
	err := database.Stop()
	if err != nil {
		t.Fatalf("could not stop database: %v", err)
	}
}

// FlushDB flushes all dbs without removing the tables
// the t*testing.T parameter is to ensure this is used in a testing function
func flushDB(t *testing.T, db *pg.DB) error {
	var tableInfo []struct {
		Table string
	}
	query := `SELECT table_name "table"
				FROM information_schema.tables WHERE table_schema='public'
					AND table_type='BASE TABLE' AND table_name != 'version';`
	_, err := db.Query(&tableInfo, query)
	if err != nil {
		return err
	}

	truncateQueries := make([]string, len(tableInfo))

	for i, info := range tableInfo {
		truncateQueries[i] = "TRUNCATE " + info.Table + " CASCADE;"
		log.Println(truncateQueries[i])
	}

	err = db.RunInTransaction(func(tx *pg.Tx) error {
		for _, query := range truncateQueries {
			_, err = tx.Exec(query)
			if err != nil {
				return err
			}
		}
		return nil
	})

	return err
}
