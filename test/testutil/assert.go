package testutil

import (
	"encoding/json"
	"reflect"
	"testing"
)

func AssertEqual(t *testing.T, actual interface{}, expected interface{}) bool {
	t.Helper()
	if actual == expected {
		return true
	} else {
		t.Errorf("\nActual:   %v (type %v)\nExpected: %v (type %v)", actual, reflect.TypeOf(actual), expected, reflect.TypeOf(expected))
		return false
	}
}

func AssertEqualsJson(t *testing.T, rule interface{}, expected string) bool {
	t.Helper()
	bytes, err := json.Marshal(rule)
	if err != nil {
		t.Error(err.Error())
		return false
	}
	return AssertEqual(t, string(bytes), expected)
}
