package logic

import (
	"bitbucket.org/fraktalcyber/cspm-core/src/core/logic"
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	file2 "bitbucket.org/fraktalcyber/cspm-core/src/core/storage/file"
	util2 "bitbucket.org/fraktalcyber/cspm-core/src/core/util"
	"bitbucket.org/fraktalcyber/cspm-core/test/testutil"
	"fmt"
	uuid "github.com/satori/go.uuid"
	"testing"
	"time"
)

func TestDefaultRules(t *testing.T) {
	var rules, err = file2.LoadRules("./data/rules")
	if err != nil {
		t.Error(err)
		return
	}
	if !testutil.AssertEqual(t, len(rules), 2) {
		return
	}

	spaceId := "00000000-0000-0000-0000-000000000000"
	emptyString := ""
	id := uuid.NewV4().String()
	propertyMap := &map[string]interface{}{}
	var finding = &model2.SecurityFinding{
		Id:               id,
		SpaceId:          spaceId,
		AccountId:        "test-id",
		AccountName:      "test-name",
		Region:           "test-region",
		ResourceType:     &emptyString,
		ResourceId:       &emptyString,
		Category:         "anomaly",
		Type:             "test-type",
		Key:              "test-keu",
		SourceType:       "test-source-type",
		SourceId:         "test-source-id",
		Title:            "test-title",
		Description:      "test-description",
		Labels:           nil,
		Metadata:         propertyMap,
		JiraIssueKey:     nil,
		Priority:         3,
		OriginalPriority: 3,
		SourcePriority:   3,
		RulePriority:     nil,
		ManualPriority:   nil,
		State:            model2.StateActive,
		AlertState:       model2.AlertStateInactive,
		LastComment:      "test-last-comment",
		LastAuthor:       "test-last-author",
		Created:          time.Now(),
		Modified:         time.Now(),
		LastObserved:     time.Now(),
		FirstObserved:    time.Now(),
	}

	changes := logic.ApplyRulesToFinding(rules, finding)

	testutil.AssertEqual(t, len(changes), 2)
	testutil.AssertEqual(t, changes[0].Changed, true)
	testutil.AssertEqual(t, changes[0].Alerted, true)
	testutil.AssertEqual(t, changes[0].Prioritized, false)
	testutil.AssertEqual(t, changes[0].AlertActivated, false)
	testutil.AssertEqual(t, changes[0].AlertDeactivated, false)
	testutil.AssertEqual(t, changes[1].Changed, false)
	testutil.AssertEqual(t, changes[1].Alerted, false)
	testutil.AssertEqual(t, changes[1].Prioritized, false)
	testutil.AssertEqual(t, changes[1].AlertActivated, true)
	testutil.AssertEqual(t, changes[0].AlertDeactivated, false)

	fmt.Println(util2.ToPrettyJson(changes))
}
