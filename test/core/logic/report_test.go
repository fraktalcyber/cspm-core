package logic

import (
	"bitbucket.org/fraktalcyber/cspm-core/src/core/logic"
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	db2 "bitbucket.org/fraktalcyber/cspm-core/src/core/storage/db"
	"bitbucket.org/fraktalcyber/cspm-core/test/testutil"
	"log"
	"testing"
	"time"

	uuid "github.com/satori/go.uuid"
)

func TestSecurityFindingReporting(t *testing.T) {
	location := time.Now().Location()
	database := testutil.SetupTestApplicationDatabase(t)

	findingType1 := "test-type-1"
	findingType2 := "test-type-2"
	spaceId := "00000000-0000-0000-0000-000000000000"
	accountId := "test-account-id"
	region := "test-region"

	err := addTestFinding(t, database, spaceId, accountId, region, findingType1,
		time.Date(2021, 3, 1, 1, 0, 0, 0, location),
		time.Date(2021, 3, 2, 2, 0, 0, 0, location))
	if err != nil {
		return
	}

	err = addTestFinding(t, database, spaceId, accountId, region, findingType1,
		time.Date(2021, 3, 1, 3, 0, 0, 0, location),
		time.Date(2021, 3, 3, 4, 0, 0, 0, location))
	if err != nil {
		return
	}

	err = addTestFinding(t, database, spaceId, accountId, region, findingType2,
		time.Date(2021, 3, 1, 5, 0, 0, 0, location),
		time.Date(2021, 3, 4, 6, 0, 0, 0, location))
	if err != nil {
		return
	}

	latestRow, err := db2.GetLatestProcessingRun(database, spaceId, logic.GetSecuringFindingReportProcessingRunKey(accountId, region))
	if latestRow != nil {
		t.Error("security finding report row already exists.")
		return
	}

	close := false
	//err = logic.UpdateSecurityFindingReportRows(database, 2021, 3, 1, 4, spaceId, accountId, region)
	err = logic.UpdateSecurityFindingReportRowHistory(database, 2021, 3, 13, 12, 4, spaceId, accountId, region, &close)
	if err != nil {
		t.Error(err)
		return
	}

	latestRow, err = db2.GetLatestProcessingRun(database, spaceId, logic.GetSecuringFindingReportProcessingRunKey(accountId, region))
	if !testutil.AssertEqual(t, latestRow.PeriodEnd.In(location).Format("2006-01-02"), "2021-03-04") {
		return
	}

	var rows []*model2.SecurityFindingReportRow
	err = database.Db.Model(&rows).OrderExpr("key ASC").Select()
	if err != nil {
		t.Error(err)
		return
	}

	for i, row := range rows {
		log.Printf("%v: %v", i, row)
	}

	if !testutil.AssertEqual(t, len(rows), 8) {
		return
	}

	if !testutil.AssertEqual(t, rows[0].Key, "2021-03-01/test-account-id/test-region/test-source-type/test-category/test-type-1/1") {
		return
	}
	if !testutil.AssertEqual(t, rows[0].FindingCount, 2) {
		return
	}

	if !testutil.AssertEqual(t, rows[1].Key, "2021-03-01/test-account-id/test-region/test-source-type/test-category/test-type-2/1") {
		return
	}
	if !testutil.AssertEqual(t, rows[1].FindingCount, 1) {
		return
	}

	if !testutil.AssertEqual(t, rows[2].Key, "2021-03-02/test-account-id/test-region/test-source-type/test-category/test-type-1/1") {
		return
	}
	if !testutil.AssertEqual(t, rows[2].FindingCount, 1) {
		return
	}

	if !testutil.AssertEqual(t, rows[3].Key, "2021-03-02/test-account-id/test-region/test-source-type/test-category/test-type-2/1") {
		return
	}
	if !testutil.AssertEqual(t, rows[3].FindingCount, 1) {
		return
	}

	if !testutil.AssertEqual(t, rows[4].Key, "2021-03-03/test-account-id/test-region/test-source-type/test-category/test-type-1/1") {
		return
	}
	if !testutil.AssertEqual(t, rows[4].FindingCount, 0) {
		return
	}

	if !testutil.AssertEqual(t, rows[5].Key, "2021-03-03/test-account-id/test-region/test-source-type/test-category/test-type-2/1") {
		return
	}
	if !testutil.AssertEqual(t, rows[5].FindingCount, 1) {
		return
	}

	if !testutil.AssertEqual(t, rows[6].Key, "2021-03-04/test-account-id/test-region/test-source-type/test-category/test-type-1/1") {
		return
	}
	if !testutil.AssertEqual(t, rows[6].FindingCount, 0) {
		return
	}

	if !testutil.AssertEqual(t, rows[7].Key, "2021-03-04/test-account-id/test-region/test-source-type/test-category/test-type-2/1") {
		return
	}
	if !testutil.AssertEqual(t, rows[7].FindingCount, 0) {
		return
	}

	err = logic.UpdateSecurityFindingReportRowHistory(database, 2021, 3, 13, 12, 4, spaceId, accountId, region, &close)
	if err != nil {
		t.Error(err)
		return
	}

	err = database.Db.Model(&rows).OrderExpr("key ASC").Select()
	if err != nil {
		t.Error(err)
		return
	}

	for i, row := range rows {
		log.Printf("%v: %v", i, row)
	}

	if !testutil.AssertEqual(t, len(rows), 16) {
		return
	}

	if !testutil.AssertEqual(t, rows[0].Key, "2021-03-01/test-account-id/test-region/test-source-type/test-category/test-type-1/1") {
		return
	}
	if !testutil.AssertEqual(t, rows[0].FindingCount, 2) {
		return
	}

	if !testutil.AssertEqual(t, rows[1].Key, "2021-03-01/test-account-id/test-region/test-source-type/test-category/test-type-2/1") {
		return
	}
	if !testutil.AssertEqual(t, rows[1].FindingCount, 1) {
		return
	}

	if !testutil.AssertEqual(t, rows[2].Key, "2021-03-02/test-account-id/test-region/test-source-type/test-category/test-type-1/1") {
		return
	}
	if !testutil.AssertEqual(t, rows[2].FindingCount, 1) {
		return
	}

	if !testutil.AssertEqual(t, rows[3].Key, "2021-03-02/test-account-id/test-region/test-source-type/test-category/test-type-2/1") {
		return
	}
	if !testutil.AssertEqual(t, rows[3].FindingCount, 1) {
		return
	}

	if !testutil.AssertEqual(t, rows[4].Key, "2021-03-03/test-account-id/test-region/test-source-type/test-category/test-type-1/1") {
		return
	}
	if !testutil.AssertEqual(t, rows[4].FindingCount, 0) {
		return
	}

	if !testutil.AssertEqual(t, rows[5].Key, "2021-03-03/test-account-id/test-region/test-source-type/test-category/test-type-2/1") {
		return
	}
	if !testutil.AssertEqual(t, rows[5].FindingCount, 1) {
		return
	}

	if !testutil.AssertEqual(t, rows[6].Key, "2021-03-04/test-account-id/test-region/test-source-type/test-category/test-type-1/1") {
		return
	}
	if !testutil.AssertEqual(t, rows[6].FindingCount, 0) {
		return
	}

	if !testutil.AssertEqual(t, rows[7].Key, "2021-03-04/test-account-id/test-region/test-source-type/test-category/test-type-2/1") {
		return
	}
	if !testutil.AssertEqual(t, rows[7].FindingCount, 0) {
		return
	}

	if !testutil.AssertEqual(t, rows[8].Key, "2021-03-05/test-account-id/test-region/test-source-type/test-category/test-type-1/1") {
		return
	}
	if !testutil.AssertEqual(t, rows[8].FindingCount, 0) {
		return
	}

	if !testutil.AssertEqual(t, rows[9].Key, "2021-03-05/test-account-id/test-region/test-source-type/test-category/test-type-2/1") {
		return
	}
	if !testutil.AssertEqual(t, rows[9].FindingCount, 0) {
		return
	}

	if !testutil.AssertEqual(t, rows[10].Key, "2021-03-06/test-account-id/test-region/test-source-type/test-category/test-type-1/1") {
		return
	}
	if !testutil.AssertEqual(t, rows[10].FindingCount, 0) {
		return
	}

	if !testutil.AssertEqual(t, rows[11].Key, "2021-03-06/test-account-id/test-region/test-source-type/test-category/test-type-2/1") {
		return
	}
	if !testutil.AssertEqual(t, rows[11].FindingCount, 0) {
		return
	}

	if !testutil.AssertEqual(t, rows[12].Key, "2021-03-07/test-account-id/test-region/test-source-type/test-category/test-type-1/1") {
		return
	}
	if !testutil.AssertEqual(t, rows[12].FindingCount, 0) {
		return
	}

	if !testutil.AssertEqual(t, rows[13].Key, "2021-03-07/test-account-id/test-region/test-source-type/test-category/test-type-2/1") {
		return
	}
	if !testutil.AssertEqual(t, rows[13].FindingCount, 0) {
		return
	}

	if !testutil.AssertEqual(t, rows[14].Key, "2021-03-08/test-account-id/test-region/test-source-type/test-category/test-type-1/1") {
		return
	}
	if !testutil.AssertEqual(t, rows[14].FindingCount, 0) {
		return
	}

	if !testutil.AssertEqual(t, rows[15].Key, "2021-03-08/test-account-id/test-region/test-source-type/test-category/test-type-2/1") {
		return
	}
	if !testutil.AssertEqual(t, rows[15].FindingCount, 0) {
		return
	}

	err = logic.UpdateSecurityFindingReportRowHistory(database, 2021, 3, 9, 12, 4, spaceId, accountId, region, &close)
	if err != nil {
		t.Error(err)
		return
	}

	err = database.Db.Model(&rows).OrderExpr("key ASC").Select()
	if err != nil {
		t.Error(err)
		return
	}

	if !testutil.AssertEqual(t, len(rows), 16) {
		return
	}

	testutil.TearDownTestApplicationDatabase(t, database)
}

func addTestFinding(t *testing.T, database *db2.Database, spaceId string, accountId, region, findingType string, created time.Time, archived time.Time) error {
	propertyMap := &map[string]interface{}{}
	emptyString := ""
	id := uuid.NewV4().String()
	finding := &model2.SecurityFinding{
		Id:               id,
		SpaceId:          spaceId,
		AccountId:        accountId,
		AccountName:      "test-name",
		Region:           region,
		ResourceType:     &emptyString,
		ResourceId:       &emptyString,
		Category:         "test-category",
		Type:             findingType,
		TypeLabel:        findingType,
		Key:              id,
		SourceType:       "test-source-type",
		SourceId:         "test-source-id",
		SourceFindingUrl: nil,
		Title:            "test-title",
		Description:      "test-description",
		Remediation:      "test-remediation",
		RemediationUrl:   nil,
		Labels:           propertyMap,
		Metadata:         propertyMap,
		JiraIssueKey:     nil,
		Priority:         1,
		OriginalPriority: 1,
		SourcePriority:   0,
		RulePriority:     nil,
		ManualPriority:   nil,
		State:            "test-state",
		AlertState:       "test-alert-state",
		LastComment:      "test-last-comment",
		LastAuthor:       "test-last-author",
		Created:          created,
		Modified:         archived,
		LastObserved:     archived,
		FirstObserved:    created,
	}
	err := database.Db.Insert(finding)
	if err != nil {
		t.Error(err)
		return err
	}

	createdState := &model2.StateChange{
		Id:                uuid.NewV4().String(),
		SpaceId:           spaceId,
		SecurityFindingId: finding.Id,
		Metadata:          propertyMap,
		State:             model2.StateActive,
		Priority:          1,
		Comment:           "activated",
		Author:            "test-author",
		Created:           created,
		Modified:          created,
	}
	err = database.Db.Insert(createdState)
	if err != nil {
		t.Error(err)
		return err
	}

	archivedState := &model2.StateChange{
		Id:                uuid.NewV4().String(),
		SpaceId:           spaceId,
		SecurityFindingId: finding.Id,
		Metadata:          propertyMap,
		State:             model2.StateArchived,
		Priority:          1,
		Comment:           "archived",
		Author:            "test-author",
		Created:           archived,
		Modified:          archived,
	}
	err = database.Db.Insert(archivedState)
	if err != nil {
		t.Error(err)
		return err
	}

	return nil
}
