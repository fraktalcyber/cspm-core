package logic

import (
	"bitbucket.org/fraktalcyber/cspm-core/src/core/logic"
	"bitbucket.org/fraktalcyber/cspm-core/test/testutil"
	"testing"
)

func TestCriteriaMatch(t *testing.T) {

	var rule, err = testutil.LoadRule("test-data/rules/example/rule_2.json")
	if err != nil {
		t.Error(err)
		return
	}

	item, err := testutil.LoadJson("test-data/items/item_2_1.json")
	if err != nil {
		t.Error(err)
		return
	}

	if !testutil.AssertEqualsJson(t, item, "{\"bar\":\"a\",\"foo\":\"x\"}") {
		return
	}

	result := logic.MatchCriteria(rule.Criteria, &item)
	if !testutil.AssertEqual(t, result, true) {
		return
	}

}
