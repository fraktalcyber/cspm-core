package logic

import (
	file2 "bitbucket.org/fraktalcyber/cspm-core/src/core/storage/file"
	"bitbucket.org/fraktalcyber/cspm-core/test/testutil"
	"testing"
)

func TestLoadRules(t *testing.T) {
	var rules, err = file2.LoadRules("./test-data/rules")
	if err != nil {
		t.Error(err)
		return
	}
	if !testutil.AssertEqual(t, len(rules), 4) {
		return
	}
	var rule = rules[2]
	if !testutil.AssertEqualsJson(t, rule, "{\"Id\":\"\",\"Category\":\"example\",\"Key\":\"rule_1\",\"Priority\":1,\"Enabled\":true,\"Criteria\":[{\"Path\":\"/foo\",\"Operator\":\"==\",\"Value\":\"x\",\"Criteria\":null},{\"Path\":null,\"Operator\":\"\\u0026\\u0026\",\"Value\":null,\"Criteria\":null},{\"Path\":null,\"Operator\":null,\"Value\":null,\"Criteria\":[{\"Path\":\"/bar\",\"Operator\":\"!=\",\"Value\":\"y\",\"Criteria\":null},{\"Path\":null,\"Operator\":\"||\",\"Value\":null,\"Criteria\":null},{\"Path\":\"/bar\",\"Operator\":\"==\",\"Value\":\"z\",\"Criteria\":null}]}],\"Action\":{\"Prioritize\":3,\"Alert\":false,\"Archive\":true}}") {
		return
	}

}
