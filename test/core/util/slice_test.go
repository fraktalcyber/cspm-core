package util

import (
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	util2 "bitbucket.org/fraktalcyber/cspm-core/src/core/util"
	"bitbucket.org/fraktalcyber/cspm-core/test/testutil"
	"testing"
)

func TestStringSliceIndexOf(t *testing.T) {
	var QuarterlyExpiringSecurityFindingTypes = []string{
		model2.SecurityFindingTypeConsoleSigninNoMfa,
	}

	testutil.AssertEqual(t, util2.StringSliceIndexOf(QuarterlyExpiringSecurityFindingTypes, model2.SecurityFindingTypeConsoleSigninNoMfa), 0)
	testutil.AssertEqual(t, util2.StringSliceIndexOf(QuarterlyExpiringSecurityFindingTypes, "Not Contained"), -1)

	testutil.AssertEqual(t, util2.StringSliceContains(QuarterlyExpiringSecurityFindingTypes, model2.SecurityFindingTypeConsoleSigninNoMfa), true)
	testutil.AssertEqual(t, util2.StringSliceContains(QuarterlyExpiringSecurityFindingTypes, "Not Contained"), false)

}
