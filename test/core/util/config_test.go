package util

import (
	util2 "bitbucket.org/fraktalcyber/cspm-core/src/core/util"
	"bitbucket.org/fraktalcyber/cspm-core/test/testutil"
	"testing"
)

func TestGetConfig(t *testing.T) {
	testConfigValue := util2.GetConfig("test", "d")
	testutil.AssertEqual(t, testConfigValue, "d")
}
