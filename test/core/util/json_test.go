package util

import (
	"bitbucket.org/fraktalcyber/cspm-core/test/testutil"
	"encoding/json"
	"testing"

	"github.com/mattn/go-jsonpointer"
)

func TestJsonPointer(t *testing.T) {
	jsonString := `
	{
		"test": true,
		"foo": [1,true,2]
	}
	`
	var obj map[string]interface{}
	var pointer = &obj
	err := json.Unmarshal([]byte(jsonString), &obj)
	if err != nil {
		t.Fatalf("could not unmarshal string %v as JSON: %v", jsonString, err)
	}
	result, err := jsonpointer.Get(*pointer, "/foo/1")
	if err != nil {
		t.Fatal(err)
	}
	testutil.AssertEqual(t, result, true)

	result, err = jsonpointer.Get(*pointer, "/test")
	if err != nil {
		t.Fatal(err)
	}
	testutil.AssertEqual(t, result, true)
}
