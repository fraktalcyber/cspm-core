package shodan

import (
	util2 "bitbucket.org/fraktalcyber/cspm-core/src/core/util"
	"context"
	"io/ioutil"
	"log"
	"net/http"
	"testing"
)

func TestShodan(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration test.")
	}

	shodanKey, err := util2.GetSecret("shodan_key")
	if err != nil {
		t.Error(err)
		return
	}

	ip := "1.1.1.1"
	url := "https://api.shodan.io/shodan/host/" + ip + "?key=" + shodanKey

	ctx := context.Background()
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		t.Error(err)
		return
	}
	req = req.WithContext(ctx)

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Error(err)
		return
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		t.Error(res.StatusCode)
		return
	}

	bodyBytes, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Error(err)
		return
	}

	log.Println(string(bodyBytes))
}
