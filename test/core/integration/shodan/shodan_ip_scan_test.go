package shodan

import (
	"bitbucket.org/fraktalcyber/cspm-core/src/core/integration/shodan"
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	util2 "bitbucket.org/fraktalcyber/cspm-core/src/core/util"
	"fmt"
	"testing"
	"time"
)

func TestGetShodanScanForIpResource(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration test.")
	}

	c := make(chan *model2.Resource, 100000)
	v := make(chan *model2.Vulnerability, 100000)

	timestamp := time.Now().Add(-14 * 24 * time.Hour)
	r := &model2.Resource{
		Id:                "",
		ParentId:          nil,
		Key:               "1.1.1.1",
		Category:          model2.ResourceCategoryIp,
		Type:              "public-ip",
		SpaceId:           "00000000-0000-0000-0000-000000000000",
		AccountId:         "00000000000",
		AccountName:       "unit-test",
		Region:            "eu-central-1",
		Team:              "it",
		EnvironmentType:   "unit-test",
		Labels:            nil,
		Metadata:          nil,
		VulnerabilityKeys: []string{},
		State:             model2.ResourceStateActive,
		Created:           timestamp,
		Modified:          timestamp,
		LastObserved:      timestamp,
		FirstObserved:     timestamp,
		ParentKey:         nil,
	}

	err := shodan.GetShodanScanForIpResource(r, c, v)
	if err != nil {
		t.Fatal(err)
	}

out:
	for {
		select {
		case resource := <-c:
			fmt.Println(resource.Type + "/" + resource.Key + ": " + util2.FormatInterface(resource.Metadata))
		case <-time.After(time.Second * 5):
			t.Log("Timeout")
			t.Fail()
			break out
		}
		if len(c) == 0 {
			break
		}
	}
	close(c)

}
