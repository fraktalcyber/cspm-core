package shodan

import (
	util2 "bitbucket.org/fraktalcyber/cspm-core/src/core/util"
	"encoding/json"
	"io/ioutil"
	"log"
	"testing"
)

func TestSecurityFindingTableWithProwlerData(t *testing.T) {

	bytes, err := ioutil.ReadFile("test-data/scans/shodan.json") // just pass the file name
	if err != nil {
		t.Error(err)
		return
	}

	var scanResult map[string]interface{}
	err = json.Unmarshal(bytes, &scanResult)
	if err != nil {
		t.Error(err)
		return
	}

	ip := scanResult["ip_str"].(string)
	log.Println(ip)

	log.Println(" hostnames:")
	if hs, ok := scanResult["hostnames"]; ok {
		var hostnames = hs.([]interface{})
		for _, h := range hostnames {
			var hostname = h.(string)
			log.Println("  " + hostname)
		}
	}

	log.Println(" ports:")
	if data, ok := scanResult["data"]; ok {
		var portScans = data.([]interface{})

		for _, p := range portScans {
			var portScan = p.(map[string]interface{})
			var transport = portScan["transport"].(string)
			var port = portScan["port"].(float64)
			var product = util2.ValueToStringPointer(portScan["product"])
			var version = util2.ValueToStringPointer(portScan["version"])
			log.Printf("  %v/%v %v %v", port, transport, util2.FormatStringPointer(product), util2.FormatStringPointer(version))

			if vs, ok := portScan["vulns"]; ok {
				var vulnerabilities = vs.(map[string]interface{})
				for vulnId, v := range vulnerabilities {
					var vulnerability = v.(map[string]interface{})
					var cvss = vulnerability["cvss"].(string)
					var summary = vulnerability["summary"].(string)
					log.Printf("   %v %v - %v", vulnId, cvss, summary)

					if rs, ok := vulnerability["references"]; ok {
						var references = rs.([]interface{})
						for _, r := range references {
							var r = r.(string)
							log.Printf("    %v", r)
						}
					}

				}
			}

		}

	}

}
