package aws

import (
	aws2 "bitbucket.org/fraktalcyber/cspm-core/src/core/integration/aws"
	"bitbucket.org/fraktalcyber/cspm-core/test/testutil"
	"testing"
)

func TestGetOrganizationContext(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration test.")
	}
	organizationContext, err := aws2.GetOrganizationContext()
	if err != nil {
		t.Error(err)
		return
	}
	testutil.AssertEqual(t, len(organizationContext.AccountContexts), 8)
}
