package loki

import (
	aws2 "bitbucket.org/fraktalcyber/cspm-core/src/core/integration/aws"
	loki2 "bitbucket.org/fraktalcyber/cspm-core/src/core/integration/aws/loki"
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	"fmt"
	"io/ioutil"
	"log"
	"testing"
	"time"
)

func TestKubeBenchReportProcess(t *testing.T) {
	bytes, err := ioutil.ReadFile("test-data/scans/kube_bench.json")
	if err != nil {
		t.Error(err)
		return
	}

	organizationContext, err := aws2.GetOrganizationContext()
	if err != nil {
		t.Error(err)
		return
	}

	c := make(chan *model2.SecurityFinding, 1000)
	accountContext := organizationContext.AccountContexts[0]
	streamContext := loki2.StreamContext{
		AccountId:           accountContext.Id,
		Region:              accountContext.Region,
		LogGroup:            "test-log-grpi",
		KubernetesCluster:   "test-cluster",
		HostName:            "test-host",
		KubernetesNamespace: "test-namespace",
		ContainerName:       "test-container",
	}
	var lines []*loki2.Line
	lines = append(lines, &loki2.Line{
		Timestamp: time.Now(),
		Line:      "}",
	})
	jsonString := string(bytes)
	jsonStringPartOne := jsonString[:10]
	jsonStringPartTwo := jsonString[10:]
	lines = append(lines, &loki2.Line{
		Timestamp: time.Now(),
		Line:      jsonStringPartOne,
	})
	lines = append(lines, &loki2.Line{
		Timestamp: time.Now(),
		Line:      jsonStringPartTwo,
	})
	lines = append(lines, &loki2.Line{
		Timestamp: time.Now(),
		Line:      "{",
	})
	lines = append(lines, &loki2.Line{
		Timestamp: time.Now(),
		Line:      jsonStringPartOne,
	})
	lines = append(lines, &loki2.Line{
		Timestamp: time.Now(),
		Line:      jsonStringPartTwo,
	})
	lines = append(lines, &loki2.Line{
		Timestamp: time.Now(),
		Line:      "{",
	})
	err = loki2.ProcessKubeBenchLogLines(accountContext, &streamContext, lines, c)
	if err != nil {
		t.Error(err)
		return
	}

	log.Println("Listening channel...")

	var findingCount = 0
	for {
		select {
		case _ = <-c:
			fmt.Print(".")
			findingCount++
			//fmt.Println(securityFinding)
		}
		if len(c) == 0 {
			break
		}
	}

	if findingCount != 2 {
		t.Errorf("Invalid number of findings returned: 182!=%+v", findingCount)
	}

	close(c)
}
