package loki

import (
	aws2 "bitbucket.org/fraktalcyber/cspm-core/src/core/integration/aws"
	loki2 "bitbucket.org/fraktalcyber/cspm-core/src/core/integration/aws/loki"
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	util2 "bitbucket.org/fraktalcyber/cspm-core/src/core/util"
	"fmt"
	"io/ioutil"
	"log"
	"testing"
	"time"
)

func TestFalcoJson1Process(t *testing.T) {
	bytes, err := ioutil.ReadFile("test-data/logs/falco_1.json")
	if err != nil {
		t.Error(err)
		return
	}

	organizationContext, err := aws2.GetOrganizationContext()
	if err != nil {
		t.Error(err)
		return
	}

	c := make(chan *model2.SecurityFinding, 1000)
	accountContext := organizationContext.AccountContexts[0]
	streamContext := loki2.StreamContext{
		AccountId:           accountContext.Id,
		Region:              accountContext.Region,
		LogGroup:            "test-log-group",
		KubernetesCluster:   "test-cluster",
		HostName:            "test-host",
		KubernetesNamespace: "test-namespace",
		ContainerName:       "test-container",
	}
	var lines []*loki2.Line
	lines = append(lines, &loki2.Line{
		Timestamp: time.Now(),
		Line:      string(bytes),
	})

	err = loki2.ProcessFalcoLogLines(accountContext, &streamContext, lines, c)
	if err != nil {
		t.Error(err)
		return
	}

	log.Println("Listening channel...")

	var findingCount = 0
	for {
		select {
		case securityFinding := <-c:
			fmt.Print(".")
			findingCount++
			securityFindingJson, _ := util2.ToPrettyJson(securityFinding)
			fmt.Println(securityFindingJson)
		}
		if len(c) == 0 {
			break
		}
	}

	if findingCount != 1 {
		t.Errorf("Invalid number of findings returned: 1!=%+v", findingCount)
	}

	close(c)
}

func TestFalcoJson2Process(t *testing.T) {
	bytes, err := ioutil.ReadFile("test-data/logs/falco_2.json")
	if err != nil {
		t.Error(err)
		return
	}

	organizationContext, err := aws2.GetOrganizationContext()
	if err != nil {
		t.Error(err)
		return
	}

	c := make(chan *model2.SecurityFinding, 1000)
	accountContext := organizationContext.AccountContexts[0]
	streamContext := loki2.StreamContext{
		AccountId:           accountContext.Id,
		Region:              accountContext.Region,
		LogGroup:            "test-log-group",
		KubernetesCluster:   "test-cluster",
		HostName:            "test-host",
		KubernetesNamespace: "test-namespace",
		ContainerName:       "test-container",
	}
	var lines []*loki2.Line
	lines = append(lines, &loki2.Line{
		Timestamp: time.Now(),
		Line:      string(bytes),
	})

	err = loki2.ProcessFalcoLogLines(accountContext, &streamContext, lines, c)
	if err != nil {
		t.Error(err)
		return
	}

	log.Println("Listening channel...")

	var findingCount = 0
	for {
		select {
		case securityFinding := <-c:
			fmt.Print(".")
			findingCount++
			securityFindingJson, _ := util2.ToPrettyJson(securityFinding)
			fmt.Println(securityFindingJson)
		}
		if len(c) == 0 {
			break
		}
	}

	if findingCount != 1 {
		t.Errorf("Invalid number of findings returned: 1!=%+v", findingCount)
	}

	close(c)
}

func TestFalcoJson3Process(t *testing.T) {
	bytes, err := ioutil.ReadFile("test-data/logs/falco_3.json")
	if err != nil {
		t.Error(err)
		return
	}

	organizationContext, err := aws2.GetOrganizationContext()
	if err != nil {
		t.Error(err)
		return
	}

	c := make(chan *model2.SecurityFinding, 1000)
	accountContext := organizationContext.AccountContexts[0]
	streamContext := loki2.StreamContext{
		AccountId:           accountContext.Id,
		Region:              accountContext.Region,
		LogGroup:            "test-log-group",
		KubernetesCluster:   "test-cluster",
		HostName:            "test-host",
		KubernetesNamespace: "test-namespace",
		ContainerName:       "test-container",
	}
	var lines []*loki2.Line
	lines = append(lines, &loki2.Line{
		Timestamp: time.Now(),
		Line:      string(bytes),
	})

	err = loki2.ProcessFalcoLogLines(accountContext, &streamContext, lines, c)
	if err != nil {
		t.Error(err)
		return
	}

	log.Println("Listening channel...")

	var findingCount = 0
	for {
		select {
		case securityFinding := <-c:
			fmt.Print(".")
			findingCount++
			securityFindingJson, _ := util2.ToPrettyJson(securityFinding)
			fmt.Println(securityFindingJson)
		}
		if len(c) == 0 {
			break
		}
	}

	if findingCount != 1 {
		t.Errorf("Invalid number of findings returned: 1!=%+v", findingCount)
	}

	close(c)
}
