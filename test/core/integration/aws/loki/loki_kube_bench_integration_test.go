package loki

import (
	aws2 "bitbucket.org/fraktalcyber/cspm-core/src/core/integration/aws"
	loki2 "bitbucket.org/fraktalcyber/cspm-core/src/core/integration/aws/loki"
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	util2 "bitbucket.org/fraktalcyber/cspm-core/src/core/util"
	"fmt"
	"testing"
)

func TestLokiKubeBenchIntegration(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration test.")
	}

	organizationContext, err := aws2.GetOrganizationContext()
	if err != nil {
		t.Fatal(err)
	}

	c := make(chan *model2.SecurityFinding, 1000)

	var accountContext *model2.AccountContext
	for _, accountContextCandidate := range organizationContext.AccountContexts {
		if accountContextCandidate.Id == util2.GetConfig("kube_bench_test_account", "") {
			accountContext = accountContextCandidate
		}
	}

	if accountContext == nil {
		t.Fatal("could not find kube-bench test account in account contexts")
	}

	err = aws2.QueryAndProcessLokiLogLines(organizationContext.LogAccountContext,
		accountContext,
		util2.GetConfig("loki_query_kube_bench", "{container_name=\"kube-bench\",aws_account_id=\""+accountContext.Id+"\",aws_region=\""+accountContext.Region+"\"}"),
		c, loki2.ProcessKubeBenchLogLines, false)

	if err != nil {
		t.Fatal(err)
	}

	for {
		select {
		case _ = <-c:
			fmt.Print(".")
		}
		if len(c) == 0 {
			break
		}
	}
	close(c)

}
