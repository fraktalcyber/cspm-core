package aws

import (
	aws2 "bitbucket.org/fraktalcyber/cspm-core/src/core/integration/aws"
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	"fmt"
	"testing"
)

func TestGetResources(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration test.")
	}

	organizationContext, err := aws2.GetOrganizationContext()
	if err != nil {
		t.Error(err)
		return
	}

	c := make(chan *model2.Resource, 1000)
	for _, a := range organizationContext.AccountContexts {
		err = aws2.GetResources(a, c)
		if err != nil {
			t.Error(err)
			return
		}
	}
	for {
		select {
		case resource := <-c:
			fmt.Println("Received resource.")
			fmt.Println(resource)
		}
		if len(c) == 0 {
			break
		}
	}
	close(c)
}
