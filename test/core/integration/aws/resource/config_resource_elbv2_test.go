package resource

import (
	aws2 "bitbucket.org/fraktalcyber/cspm-core/src/core/integration/aws"
	resource2 "bitbucket.org/fraktalcyber/cspm-core/src/core/integration/aws/resource"
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	util2 "bitbucket.org/fraktalcyber/cspm-core/src/core/util"
	"fmt"
	"github.com/aws/aws-sdk-go-v2/service/configservice"
	"testing"
)

func TestGetConfigELBV2Resources(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration test.")
	}

	organizationContext, err := aws2.GetOrganizationContext()
	if err != nil {
		t.Error(err)
		return
	}

	c := make(chan *model2.Resource, 100000)
	for _, a := range organizationContext.AccountContexts {
		err = resource2.GetAwsConfigResource(a, configservice.ResourceTypeAwsElasticLoadBalancingV2LoadBalancer, c)
		if err != nil {
			t.Error(err)
			return
		}
	}
	for {
		select {
		case resource := <-c:
			fmt.Println(resource.Type + "/" + resource.Key + ": " + util2.FormatInterface(resource.Metadata))
		}
		if len(c) == 0 {
			break
		}
	}
	close(c)
}
