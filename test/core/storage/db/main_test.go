package db

import (
	"bitbucket.org/fraktalcyber/cspm-core/test/testutil"
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	testutil.Setup()

	code := m.Run()

	os.Exit(code)
}
