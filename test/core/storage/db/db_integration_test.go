package db

import (
	model2 "bitbucket.org/fraktalcyber/cspm-core/src/core/model"
	db2 "bitbucket.org/fraktalcyber/cspm-core/src/core/storage/db"
	"encoding/json"
	"fmt"
	"log"
	"testing"
	"time"

	uuid "github.com/satori/go.uuid"
)

func TestDbSchemaUpdate(t *testing.T) {
	database := db2.NewApplicationDatabase()

	err := database.Start()
	if err != nil {
		log.Fatalf("could not start database: %v", err)
	}

	err = database.UpdateSchema()
	if err != nil {
		log.Fatalf("could not update database: %v", err)
	}

	err = database.Stop()
	if err != nil {
		log.Fatalf("could not stop database: %v", err)
	}
}

func TestSecurityFindingTable(t *testing.T) {
	database := db2.NewApplicationDatabase()

	err := database.Start()
	if err != nil {
		t.Fatalf("could not start datatabase: %v", err)
	}

	spaceId := "00000000-0000-0000-0000-000000000000"
	emptyString := ""
	id := uuid.NewV4().String()
	propertyMap := &map[string]interface{}{}
	var finding = &model2.SecurityFinding{
		Id:               id,
		SpaceId:          spaceId,
		AccountId:        "test-id",
		AccountName:      "test-name",
		Region:           "test-region",
		ResourceType:     &emptyString,
		ResourceId:       &emptyString,
		Category:         "test-category",
		Type:             "test-type",
		TypeLabel:        "test-type-label",
		Key:              "test-key",
		SourceType:       "test-source-type",
		SourceId:         "test-source-id",
		SourceFindingUrl: &emptyString,
		Title:            "test-title",
		Description:      "test-description",
		Remediation:      "test-remediation",
		RemediationUrl:   &emptyString,
		Labels:           nil,
		Metadata:         propertyMap,
		JiraIssueKey:     nil,
		Priority:         1,
		OriginalPriority: 1,
		SourcePriority:   0,
		RulePriority:     nil,
		ManualPriority:   nil,
		State:            "test-state",
		AlertState:       "test-alert-state",
		LastComment:      "test-last-comment",
		LastAuthor:       "test-last-author",
		Created:          time.Now(),
		Modified:         time.Now(),
		LastObserved:     time.Now(),
		FirstObserved:    time.Now(),
	}
	err = database.Db.Insert(finding)
	if err != nil {
		t.Error(err)
	}
	var findings []model2.SecurityFinding
	err = database.Db.Model(&findings).Select()
	if err != nil {
		t.Error(err)
	}

	for _, f := range findings {
		bytes, _ := json.Marshal(f.Metadata)
		fmt.Println(string(bytes))
	}

	err = database.Stop()
	if err != nil {
		t.Fatalf("could not stop database: %v", err)
	}
}

func TestSecurityFindingRuleTable(t *testing.T) {
	database := db2.NewApplicationDatabase()

	err := database.Start()
	if err != nil {
		t.Fatalf("could not start datatabase: %v", err)
	}

	spaceId := "00000000-0000-0000-0000-000000000000"
	id := uuid.NewV4().String()
	testPath := "/test/path"
	testValue := "test-value"
	var rule = &model2.SecurityFindingRule{
		Id:       id,
		SpaceId:  spaceId,
		Type:     "test-type",
		Priority: 1,
		Key:      "test-name",
		Criteria: model2.CriteriaPart{
			Path:     &testPath,
			Operator: nil,
			Value:    &testValue,
			Criteria: nil,
		},
		Prioritize: 0,
		Alert:      false,
		Archive:    false,
		Enabled:    false,
		Created:    time.Now(),
		Modified:   time.Now(),
	}
	err = database.Db.Insert(rule)
	if err != nil {
		t.Error(err)
	}
	var rules []model2.SecurityFindingRule
	err = database.Db.Model(&rules).Select()
	if err != nil {
		t.Error(err)
	}

	for _, r := range rules {
		bytes, _ := json.Marshal(r.Criteria)
		fmt.Println(string(bytes))
	}

	err = database.Stop()
	if err != nil {
		t.Fatalf("could not stop database: %v", err)
	}
}
