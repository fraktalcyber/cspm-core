FROM golang:1.15-alpine

# Golang setup
ENV USER=app
ENV UID=1000
ENV HOME /home/$USER
RUN adduser --disabled-password --uid "$UID" $USER

ENV GO111MODULE=on
ENV GO15VENDOREXPERIMENT 1

WORKDIR $HOME

COPY src src
COPY data data
COPY config/default.ini config/default.ini
RUN touch config/production.ini
COPY go.mod go.mod
COPY main.go main.go
COPY run.sh run.sh

RUN mkdir vendor
RUN go mod vendor
RUN go build

RUN rm -Rf main.go
RUN rm -Rf src
RUN rm -Rf vendor
RUN rm -Rf go.mod
RUN rm -Rf go.sum
RUN rm -Rf .cache

RUN chown -R root:$USER /home/$USER/*
RUN chmod g+x $HOME/run.sh
RUN chmod g+x $HOME/cspm-core

USER $USER

RUN ls -la

CMD ["./run.sh"]
