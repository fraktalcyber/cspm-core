# CSPM Core

Cloud security posture management core component collects findings to database,
manages priority and alerts according to defined rule set.

Alerts are sent via email. 

Collected findings stored in database can be visualized for example with:

 * [Grafana: The open observability platform](https://grafana.com/)

## Configure

Environment specific configuration files located in config directory.

### Development Configuration Example (config/dev.ini)

    # Space (Data Seggregation)
    space_id = 00000000-0000-0000-0000-000000000000 # Spaces are used to seggregate data from different organizations.
    
    # AWS
    aws_account_ids = 000000000000,111111111111 # Define static list of AWS account IDs. (Optional)
    aws_account_names = test-account-1,test-account-2 # Define static list of AWS account names. (Optional)

    aws_organization_master_account_id = #xxx # Define to read organization accounts dynamically. (Optional)
    aws_log_account_id = #yyy # Define if loki is used via AWS API Gateway and signing is needed. (Optional)

    aws_iam_role = SecurityAuditorRole # AWS IAM role which is assumed when accessing resources on each account.
    aws_regions = us-east-1,eu-west-1,eu-central-1 # AWS regions being inspected.
    aws_primary_region = eu-central-1 # Primary AWS region is used for defining organization master account context and loki signing account context.
    
    aws_account_tag_key_team = team # Used to define which AWS account tag contains team name owning the AWS account.
    aws_account_tag_key_environment_type = environment-type # Used to define which AWS account tag contains environment type of the AWS account.
    
    # Database
    database_host = 127.0.0.1
    database_port = 5432
    database_name = cspm
    database_user = cspm
    database_password = secrets/database_password
    database_ssl = false

    # SMTP (Notifications)    
    smtp_host = x.x.x.x
    smtp_port = 465
    smtp_username = xxx
    smtp_password = secrets/smtp_password
    smtp_secure = true
    smtp_from = x.x@x.x.x
    smtp_alert_to = x.x@x.x.x

## Build

    go build

## Test

    go test -short ./test/...

## Run

    export ENV=dev
    aws-vault exec <profile> -- ./cspm-core

## Docker

### Build Docker Image

    docker build . -t cspm-core:1.0

### Run Docker Image

    aws-vault exec <profile> -- docker run --rm --name=cspm-core -e AWS_REGION -e AWS_ACCESS_KEY_ID -e AWS_SECRET_ACCESS_KEY -e AWS_SESSION_TOKEN -e AWS_SECURITY_TOKEN -e ENV=docker --mount type=bind,src="$(pwd)"/config,dst=/home/app/config --mount type=bind,src="$(pwd)"/secrets,dst=/home/app/secrets cspm-core:1.0
    docker stop cspm-core
    docker rm cspm-core