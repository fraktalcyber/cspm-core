#!/usr/bin/env bash
docker rm -f postgres-cspm-test 2>/dev/null
echo "Test database starting..."
docker run --name postgres-cspm-test -p 15433:5432 --tmpfs /var/lib/postgresql/data:rw -e POSTGRES_PASSWORD=unit-test-db-password -d postgres:12-alpine
echo "Test database started."