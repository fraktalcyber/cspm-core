#!/usr/bin/env bash
echo "Running tests at: $(pwd)" && \
bin/start_test_db.sh && \
go clean -testcache && \
go test  -p 1 -short ./test/...  && \
bin/stop_test_db.sh