package main

import (
	"bitbucket.org/fraktalcyber/cspm-core/src/core"
	util2 "bitbucket.org/fraktalcyber/cspm-core/src/core/util"
	"fmt"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	util2.LoadConfig()

	var err error
	var scheduler = core.NewScheduler([]core.Extension{})

	err = scheduler.Start()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer func() {
		if err := scheduler.Stop(); err != nil {
			panic(err)
		}
	}()

	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	<-done
}
